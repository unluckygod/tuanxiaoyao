<?php 

namespace Common\Util;
/**  
 * 常用对称加密算法类  
 * 支持密钥：64/128/256 bit（字节长度8/16/32）  
 * 支持算法：DES/AES（根据密钥长度自动匹配使用：DES:64bit AES:128/256bit）  
 * 支持模式：CBC/ECB/OFB/CFB  
 * 密文编码：base64字符串/十六进制字符串/二进制字符串流  
 * 填充方式: PKCS5Padding（DES）  
 *  
 * @author: zero  
 * @version: 1.0.0  
 * @date: 2017/9/28  
 */ 

class CryptAES  
{  
    protected $cipher = MCRYPT_RIJNDAEL_128;  
    protected $mode = MCRYPT_MODE_ECB;  
    protected $pad_method = NULL;  
    protected $secret_key = '';  
    protected $iv = '';  
   
    public function set_cipher($cipher){  
        $this->cipher = $cipher;  
    }  
   
    public function set_mode($mode){  
        $this->mode = $mode;  
    }  
   
    public function set_iv($iv){  
        $this->iv = $iv;  
    }  
   
    public function set_key($key){  
        $this->secret_key = $key;  
    }  
   
    public function require_pkcs5(){  
        $this->pad_method = 'pkcs5';  
    }  
   
    protected function pad_or_unpad($str, $ext){  
        if ( is_null($this->pad_method) ) {  
            return $str;  
        }  
        else {  
            $func_name = __CLASS__ . '::' . $this->pad_method . '_' . $ext . 'pad';  
            if ( is_callable($func_name) ) {  
                $size = @mcrypt_get_block_size($this->cipher, $this->mode);  
                return call_user_func($func_name, $str, $size);  
            }  
        }  
        return $str;  
    }  
   
    protected function pad($str) {  
        return $this->pad_or_unpad($str, '');  
    }  
   
    protected function unpad($str)  {  
        return $this->pad_or_unpad($str, 'un');  
    }

    /**
     * 加密
     */  
   
    public function encrypt($str) {  
        $str = $this->pad($str);  
        $td = @mcrypt_module_open($this->cipher, '', $this->mode, '');  
   
        if ( empty($this->iv) ){  
            $iv = @mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);  
        }  
        else {  
            $iv = $this->iv;  
        }  
   
        mcrypt_generic_init($td, hex2bin($this->secret_key), $iv);  
        $cyper_text = mcrypt_generic($td, $str);  
        $rt = strtoupper(bin2hex($cyper_text));  
        mcrypt_generic_deinit($td);  
        mcrypt_module_close($td);  
   
        return $rt;  
    } 


   /**
    * 解密
    */
    public function decrypt($str){  
        $td = mcrypt_module_open($this->cipher, '', $this->mode, '');  
   
        if ( empty($this->iv) ){  
            $iv = @mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);  
        }  
        else{  
            $iv = $this->iv;  
        }  
   
        mcrypt_generic_init($td, $this->secret_key, $iv);  
        //$decrypted_text = mdecrypt_generic($td, self::hex2bin($str));  
        $decrypted_text = mdecrypt_generic($td, base64_decode($str));  
        $rt = $decrypted_text;  
        mcrypt_generic_deinit($td);  
        mcrypt_module_close($td);  
   
        return $this->unpad($rt);  
    }  
   	
   	/**
   	 * 转化成十六进制的
   	 */
    public static function hex2bin($hexdata) {  
        $bindata = '';  
        $length = strlen($hexdata);  
        for ($i=0; $i< $length; $i += 2) {  
            $bindata .= chr(hexdec(substr($hexdata, $i, 2)));  
        }  
        return $bindata;  
    }  
   
    public static function pkcs5_pad($text, $blocksize) {  
        $pad = $blocksize - (strlen($text) % $blocksize);  
        return $text . str_repeat(chr($pad), $pad);  
    }  
   
    public static function pkcs5_unpad($text) {  
        $pad = ord($text{strlen($text) - 1});  
        if ($pad > strlen($text)) return false;  
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) return false;  
        return substr($text, 0, -1 * $pad);  
    }

    /**
     * 获取当前时间精确到毫秒
     */
	public static function getMillisecond() { 
		list($s1, $s2) = explode(' ', microtime()); 
		return (float)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000); 
	} 
}  
?> 