﻿<?php

return array(
    'URL_MODEL' => 2,
	'DEFAULT_MODULE'     => 'Pig',
	'MODULE_ALLOW_LIST' => array('Api','Common','Pig'),

	/* 数据库配置 */
    'DB_TYPE'   => 'mysqli', // 数据库类型
    'DB_HOST'   => 'localhost', // 服务器地址
    'DB_NAME'   => 'teambuy', // 数据库名
    'DB_USER'   => 'root', // 用户名
    'DB_PWD'    => '',  // 密码
    'DB_PORT'   => '3306', // 端口
    'DB_PREFIX' => 'pig_', // 数据库表前缀

	//请根据项目自行更改参数
	'PATH_UPLOAD'   => '.'.DS.'/Uploads/',  //文件上传路径
	'LOG' => '.'.DS.'Log',  //日志保存目录
	'CERT' => '.'.DS.'pay_cert/',		  //支付证书路径
	'COOKIE_PREFIX' => 'teambuy_',     //cookie前缀
	
	'DEFAULT_LANG' 		    => 'zh-cn', // 默认语言
	'LANG_LIST'             => 'zh-cn,en-us', // 允许切换的语言列表 用逗号分隔
	'LANG_SWITCH_ON'        => true,   // 开启语言包功能
	'LANG_AUTO_DETECT'      => true, // 自动侦测语言 开启多语言功能后有效
	'VAR_LANGUAGE'    	 	=> 'l', // 默认语言切换变量
	'ERROR_PAGE' 			=> 'Public:error', // 错误定向页面
	'TMPL_ACTION_ERROR' => 'Public:exception', //默认错误跳转对应的模板文件
    'TMPL_ACTION_SUCCESS' => 'Public:success', //默认成功跳转对应的模板文件

	'URL_CASE_INSENSITIVE'  => true,   // 默认false 表示URL区分大小写 true则表示不区分大小写
	'TMPL_DETECT_THEME'     => true, // 自动侦测模板主题
	'SESSION_AUTO_START'    => false,  //session_start
	'SHOW_PAGE_TRACE'    	=> false,  //开启调试平台
	'HTML_CACHE_ON'			=> TRUE,   //开启静态缓存
	'DOMAIN_URL'			=> $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/',  //站点域名
	'PAGE' 					=> 1,	//默认页码
	'SHOW_NUM'				=> 10,  //默认显示条数
	//拓展配置
	'APP_SUB_DOMAIN_DEPLOY'   =>    1,

	'AUTHCODE_BACKEND' => 'WhJH%!dH7As9#MixDJ',//运营后台安全码
    'AUTHCODE' => 'MixDJ*&fTA8!fKKs', //商户后台安全码
    'API_SAFE_KEY' => 'MixDJ*&fTA8!fKKsDS#@!$G@#$!#SC!',   // API数据安全验证

    /* 消息队列配置 */
	'QUEUE' => array(
	    'type' => 'redis',
	    'host' => '127.0.0.1',
	    'port' =>  '6379',
	    'prefix' => 'queue',
	    'auth' =>  '',
	),
);
