<?php

namespace Common\Library\Job;

/**
 * 购团处理类
 */
class Group
{

    private $group_id,$model,$Obj;

    public function __construct($group_id){
        $this->group_id = $group_id;
        $this->model = M('Order');
        $this->Obj = M('Group');
    }

    public function handle(){
        $this->orderHandle();
    }

    /**
     * 团处理
     */
    protected function orderHandle(){
        $info = $this->getGroupInfo();
        //判断团类型及是否自动拼团,是否拼团成功
        if($info['complete_time'] == 0){
            //拼团类型为普通团且设置系统拼团,则修改团状态
            if(in_array($info['type'],[1,3]) && $info['auto_group'] == 1){
                //判断该团中是否存在未支付的订单
                $count = $this->model->where(array('is_pay' => 0,'group_id' => $this->group_id))->count();
                if($count){
                    //设置团失效
                    $data = [
                        'remark' => '拼团时间到,存在未支付的订单,拼团失败',
                        'state' => 3
                    ];
                    $flag = $this->Obj->where(array('id' => $this->group_id))->save($data);
                    if(!$flag){
                        debug($this->Obj->_sql());
                    }
                    //对已支付的用户进行退款操作
                    $order_ids = $this->model->where(array('group_id' => $this->$group_id,'is_pay' => 1,'status' => 1,'is_collect_bills' => 0))->Field('id')->select();
                    if($order_ids){
                        foreach ($order_ids as $key => $value) {
                            $flag = $this->wxrefund($value['id']);
                            if($flag){
                                $data = [
                                    'status' => 7,
                                    'tstamp' => time(),
                                    'remark' => '拼团失败,订单失效,支付金额原路返回'
                                ];
                                $num = $this->model->where(array('id' => $value['id']))->save($data);
                                if(!$num){
                                    debug($this->model->_sql());
                                }
                            }
                        }
                    }
                    //标识未订单状态为已失效
                    $ids = $this->model->where(array('group_id' => $this->$group_id,'is_pay' => 0,'status' => 1))->Field('id')->select();
                    if($ids){
                        $rs = [];
                        foreach ($ids as $key => $value) {
                            $rs[] = $value['id'];
                        }
                        $data = [
                            'status' => 7,
                            'tstamp' => time(),
                            'remark' => '拼团失败,订单失效'
                        ];
                        $num = $this->model->where(array('id' => array('IN',$rs)))->save($data);
                        if(!$num){
                            debug($this->model->_sql());
                        }
                    }
                }else{
                    $data = [
                        'remark' => '拼团成功,系统拼团',
                        'complete_time' => time()
                    ];
                    $flag = $this->Obj->where(array('id' => $this->group_id))->save($data);
                    if(!$flag){
                        debug($this->Obj->_sql());
                    }
                }
            }else{
                //对已支付的用户进行退款操作
                $order_ids = $this->model->where(array('group_id' => $this->$group_id,'is_pay' => 1,'status' => 1))->Field('id')->select();
                if($order_ids){
                    foreach ($order_ids as $key => $value) {
                        $flag = $this->wxrefund($value['id']);
                        if($flag){
                            $data = [
                                'status' => 7,
                                'tstamp' => time(),
                                'remark' => '拼团失败,订单失效,支付金额原路返回'
                            ];
                            $num = $this->model->where(array('id' => $value['id']))->save($data);
                            if(!$num){
                                debug($this->model->_sql());
                            }
                        }
                    }
                }
                //标识未订单状态为已失效
                $ids = $this->model->where(array('group_id' => $this->$group_id,'is_pay' => 0,'status' => 1))->Field('id')->select();
                if($ids){
                    $rs = [];
                    foreach ($ids as $key => $value) {
                        $rs[] = $value['id'];
                    }
                    $data = [
                        'status' => 7,
                        'tstamp' => time(),
                        'remark' => '拼团失败,订单失效'
                    ];
                    $num = $this->model->where(array('id' => array('IN',$rs)))->save($data);
                    if(!$num){
                        debug($this->model->_sql());
                    }
                }
            }
        }
    }

    private function getGroupInfo(){
        return $this->Obj->find($this->$group_id);
    }

        /**
     * 微信退款
     * @param   string  appid
     * @param   string  mchid
     * @param   string  key
     * @param   string  transaction_id   微信支付单号
     * @param   string  pay_price        订单金额
     */
    public function wxrefund($id){
        try{
            $Order = M('Order');
            //退款订单详情
            $info = $Order->where(array('id' => $id))->find();
            /*数据整理请求接口*/
            $params = array(
                'appid' => C('Config.appid'),
                'mch_id' => C('Config.mchid'),
                'nonce_str' => $this->CreateNonceStr(),
                'transaction_id' => $info['wx_num'],
                'out_refund_no' => date('YmdHis'),
                'total_fee' => $info['pay_price'] * 100,
                'refund_fee' => $info['pay_price'] * 100,
                'notify_url' => $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/api/refund/notify'
            );
            //证书
            $path = $_SERVER['DOCUMENT_ROOT'].'/'.C('CERT');
            //文件名
            $cert = 'apiclient_cert.pem';
            $key = 'apiclient_key.pem';
            $cert = $path . $cert;
            $key = $path . $key;
            //获取保存文件的物理路径
            $params = $this->getSignParams($params);
            $params = $this->createLinkString($params,C('Config.key'));
            $url = 'https://api.mch.weixin.qq.com/secapi/pay/refund';
            $res = $this->curlPostSLL($url,$params,$key,$cert);
            if(!$res){
                return false;
            }
            /*返回结果验证*/
            $res = (array)simplexml_load_string($res, 'SimpleXMLElement', LIBXML_NOCDATA);
            if($res['return_code'] == 'FAIL' || $res['result_code'] == 'FAIL'){
                debug($res);
                return false;
            }
            return true;
        }catch(\Exception $e){
            debug($e->getmessage());
            return false;
        }
    }

    /**
     * 生成随机字符串 
     */
    public function CreateNonceStr(){
        $inviteCode = '';
        $pattern='1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        for($i=0;$i<8;$i++)
        {
        $inviteCode .= $pattern{mt_rand(0,61)};
        }
        
        return $inviteCode;
    }

    /**
     * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
     * @param $params
     * @param $mkey
     * @return string
     */
    public function createLinkString($params,$mkey){
        $arg  = "";
        foreach($params as $key => $val){
            $arg.=$key."=".$val."&";
        }
        $params['sign'] = strtoupper(MD5($arg.'key='.$mkey));
        return $params;
    }

    /**
     * 获取参与签名的参数
     * @param $params
     * @return array
     */
    public function getSignParams($params){
        $tmp = array();
        foreach($params as $key => $val){
            if($key !== 'sign' && $val !== ''){
                $tmp[$key] = $val;
            }
        }

        ksort($tmp);
        reset($tmp);

        return $tmp;
    }

    //使用证书
    public function curlPostSLL($url,$data,$key,$cert){
        //初始化curl句柄，
        $ch = curl_init();
        /*设置不使用CA证书*/
        $opt[CURLOPT_SSL_VERIFYHOST] = 2;
        $opt[CURLOPT_SSL_VERIFYPEER] = FALSE;
        curl_setopt_array($ch, $opt);
        //设置服务器返回的数据不直接输出，而是保留在curl_exec()的返回值中
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

        /**--------微信退款证书---------**/
        //默认格式为PEM，可以注释
        curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
        curl_setopt($ch,CURLOPT_SSLCERT,$cert);
        //默认格式为PEM，可以注释
        curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
        curl_setopt($ch,CURLOPT_SSLKEY,$key);
        /**--------微信退款证书---------**/

        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POST,true);
        $xml = '<xml>';
        foreach($data as $k => $val){
            $xml .= '<'.$k.'>'.$val.'</'.$k.'>';
        }
        $xml .= '</xml>';
        $data = $xml;
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
        $return = curl_exec($ch);
        $error = curl_error($ch);
        if($error){
            debug($error);
        }
        curl_close($ch);
        return $return;
    }

}
