<?php

namespace Common\Library\Job;

/**
 * 订单处理类
 */
class Order
{

    private static $order_id,$model;

    public function __construct($order_id){
    	self::$order_id = $order_id;
    	self::$model = M('Order');
    }

    public static function handle(){
    	self::orderHandle();
    }

    /**
     * 订单处理
     */
    protected static function orderHandle(){
    	$info = self::getOrderInfo();
    	//判断订单未支付做失效处理
    	if($info['status'] == 1){
    		$flag = self::$model->where(array('id' => self::$order_id))->save(['status' => 7,'tstamp' => time(),'remark' => '订单超时未支付,订单失效']);
    		if(!$flag){
    			debug(self::$model->_sql());
    		}
            //设置团失效
            $data = [
                'remark' => '存在时效期内未支付的订单,拼团失败',
                'state' => 3
            ];
            $flag = M('group')->where(array('id' => $info['group_id']))->save($data);
            if(!$flag){
                debug($this->Obj->_sql());
            }
    	}
    }

    private static function getOrderInfo(){
    	return self::$model->find(self::$order_id);
    }
}
