<?php
namespace Api\Model;
use Think\Model\RelationModel;

class BaseModel extends RelationModel{
	/*设置分页*/
    protected function limits($Model,$page,$show_num){
    	return $this->_link[$Model]['mapping_limit'] = ($page - 1) * $show_num.','.$show_num;
    }

    /*设置查询条件*/
    protected function condition($Model,$where){
    	return $this->_link[$Model]['condition'] = $where;
    }

    /*设置查询字段*/
    protected function setFields($Model,$field='*'){
        return $this->_link[$Model]['mapping_fields'] = $field;
    }

    /*设置查询字段*/
    protected function setOrder($Model,$field='crdate desc'){
        return $this->_link[$Model]['mapping_order'] = $field;
    }

    /**
	  *	获取关联数据
	  * @param  int   	$id    		主键id
	  * @param  string 	$model      关联模型
	  * @param  string  $where      附加查询条件
	  * @param  string  $fields     查询字段
	  * @param  int 	$page       页码(列表查询用于分页)
	  * @param  int     $show_num   查询条数
	  * @return mixed
	  */
	public function relation($id,$model = CONTROLLER_NAME,$where = '',$fields = '',$page = 0,$show_num = 1,$order = ''){
		$this->find($id);
		if($where)
			$this->condition($model,$where);
		if($fields)
			$this->setFields($model,$fields);
		if($page)
			$this->limits($model,$page,$show_num);
		if($order)
			$this->setOrder($model,$order);
		return $this->relationGet($model);
	}


	public function unlimitCategory($parent_id=0,$table,&$result = array()){
        $lists = M($table)->where(array('parent_id' => $parent_id))->Field('name,id,parent_id')->select();
		if($lists){
			foreach($lists as $key=>$value){
				$result[] = $value['id'];
				$this->unlimitCategory($value['id'],$table,$result);
			}
		}
        return $result;
    }
	/*基础查询条件*/
	public $where_comm = array(
				'deleted' => 0,
				'hidden' => 0
			); 
	/**
	 * 用户名验证
	 * @author  yichuanxia    
	 * @date    2017/1/19  
	 * @phone   15972053215
	 * @param   $username    用户名
	 * @return  bool 
	 */
	public function vilidateUsername($username){
		/*查询条件*/
		$where = array(
			'username' => $username
			);
		$where = array_merge($this->where_comm,$where);
		$result = M('user')->where($where)->select();
		if($result){
			return true;
		}
		return false;
	}

	/**
	 * 手机号匹配
	 * @author  yichuanxia    
	 * @date    2017/1/19  
	 * @phone   15972053215
	 * @param   int    $phone    手机号
	 * @return  bool 
	 */
	public function matchPhone($phone){
		/*匹配规则*/
		$preg_phone = '/^1[34578][0-9]{9}$/';

        if(!preg_match($preg_phone,$phone)){
            return false;
        }
        return true;
	}

	/**
	 * 手机号归属地及白名单验证
	 * @author  yichuanxia    
	 * @date    2017/2/23  
	 * @phone   15972053215
	 * @param   int    $phone    手机号
	 * @return  bool 
	 */
	public function checkPhoneUrlAndWhile($phone){
		$config = C('Config');
		$phoneWhileList = explode(',',$config['phoneWhitelist']);
		if(checkPhoneUrl($phone) || in_array($phone,$phoneWhileList)){
			return ture;
		}
		return false;
	}


	/**
	 * 手机号验证
	 * @author  yichuanxia    
	 * @date    2017/1/19  
	 * @phone   15972053215
	 * @param   int    $phone    手机号
	 * @return  bool 
	 */
	public function vilidatePhone($phone){
		/*查询条件*/
		$where = array(
			'mobile' => $phone
			);

		$where = array_merge($this->where_comm,$where);
		$result = M('user')->where($where)->select();
		if($result){
			return true;
		}
		return false;
	}

	/**
	 * 邮箱验证
	 * @author  yichuanxia    
	 * @date    2017/1/19  
	 * @phone   15972053215
	 * @param   int    $email    手机号
	 * @return  bool 
	 */
	public function matchEmail($email){
		/*匹配规则*/
		$preg_email = "/([a-z0-9]*[-_.]?[a-z0-9]+)*@([a-z0-9]*[-_]?[a-z0-9]+)+[.][a-z]{2,3}([.][a-z]{2})?/i";

        if(!preg_match($preg_email,$email)){
            return false;
        }
        return true;
	}

	/**
	 * 银行卡个验证
	 * @author  yichuanxia    
	 * @date    2017/2/19  
	 * @phone   15972053215
	 * @param   int    $card_no    手机号
	 * @return  bool 
	 */
	public function matchCardno($card_no){
		/*匹配规则*/
		$preg_cardno = "/^\d{16}|\d{19}$/";

        if(!preg_match($preg_cardno,$card_no)){
            return false;
        }
        return true;
	}
	
	/**
	 * 发送验证码验证
	 * @author   yichuanxia
	 * @Date     2017/1/19
	 * @phone    15972053215
	 * @param    int   $ip    ip地址
	 * @return   bool  
	 */
	public function vilidateSendCode($phone){
		/*读取缓存数据*/
		$data = S($phone);
		/*验证缓存数据(1.缓存数据是否存在 2.重发时间是否在设置时间内)*/
		if($data && $data['time'] + C('SMS_GAP_TIME') > time()){
            return false;
        }
        return true;
	}

	/**
	 * 发送验证码ip验证
	 * @author   yichuanxia
	 * @Date     2017/2/13
	 * @phone    15972053215
	 * @param    int   $ip    ip地址
	 * @return   bool  
	 */
	public function vilidateIp($ip){
		$data = M('ip')->where(array('ip' => $ip))->find();
		if($data){
			return true;
		}
        return false;
	}
	/**
	 * 生成验证码
	 * @author   yichuanxia
	 * @Date     2017/1/19
	 * @phone    15972053215
	 * @return   int  
	 */
	public function createCode(){
		$str='0123456789012345678901234567890123456789';
        $data['code']=substr(str_shuffle($str), 0,6);
        $data['time'] = C('SMS_EXPIRED_TIME')?C('SMS_EXPIRED_TIME'):1800;
        $data['content']='您的短信验证码是'.$code.'有效期为'.$time.'秒，请妥善保管';
        //$data['code']=123456;
        return $data;
	}

	/**
	 * 生成验证码缓存
	 * @author   yichuanxia
	 * @Date     2017/1/19
	 * @phone    15972053215
	 * @param    int      $phone    手机号 
	 * @param    int      $code     验证码
	 */
	public function createCodeCache($phone,$code){
		/*缓存数据 status使用状态 0未使用 1已使用*/
		$data = array(
			'phone' => $phone,
			'code' => $code,
			'time' => time(),
			'status' => 0
			);
        S($phone,$data,C('SMS_EXPIRED_TIME'));
	}

	/**
	 * 验证码验证
	 * @author   yichuanxia
	 * @Date     2017/1/19
	 * @phone    15972053215
	 * @param    int      $phone    手机号
	 * @param    int      $code     验证码
	 * @return   bool     
	 */
	public function vilidateCode($phone,$code){
		//return true;
		/*取缓存数据*/
		$data = S($phone);
		/*数据验证(1.手机号 2.验证码 3.是否过期 4.是否使用)*/
        if($data && $data['phone'] == $phone && $data['code'] == $code && (time() - $data['time']) < C('SMS_EXPIRED_TIME') && $data['status'] == 0){
        	return true;
        }
        return false;
	}

	/**
	 * 验证码密码(验证格式)
	 * @author   yichuanxia
	 * @Date     2017/1/19
	 * @phone    15972053215
	 * @param    int      $phone    手机号
	 * @param    int      $code     验证码 
	 * @return   bool  
	 */
	public function vilidatePWD($password){
		/*匹配规则(6-16位数字字母组成的字符串)*/
		$preg_pwd = '/(?=.*[0-9])(?=.*[a-zA-Z]).{6,16}/';
        if(!preg_match($preg_pwd, $password)){
            return false;
        }
        return true;
	}


	/**
	 * 生成邀请码
	 * @author   yichuanxia
	 * @Date     2017/1/19
	 * @phone    15972053215
	 * @return   string  
	 */
	public function CreateInviteCode(){
		$inviteCode = '';
		$pattern='1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	
    	for($i=0;$i<8;$i++)
    	{
    	$inviteCode .= $pattern{mt_rand(0,61)};
    	}
    	
    	return $inviteCode;
    }

    /**
	 * 邀请码验证
	 * @author   yichuanxia
	 * @Date     2017/1/19
	 * @phone    15972053215
	 * @param    string   $inviteCode   邀请码
	 * @return   bool  
	 */
	public function vilidateInviteCode($inviteCode){
		$where = array(
			'register_code' => $inviteCode
			);
		$where = array_merge($this->where_comm,$where);
    	$result = M('user')->where($where)->select();
    	if($result){
    		return true;
    	}
    	return false;
    }

    /**
	 * 生成token杂质
	 * @author   yichuanxia
	 * @Date     2017/2/7
	 * @phone    15972053215
	 * @return   string  
	 */
	public function CreateSalt(){
		$salt = '';
		$pattern= 'O]dWJ,[*g)%k"?q~g6Co!`cQvV>>Ilvw';
    	
    	for($i=0;$i<4;$i++)
    	{
    	$salt .= $pattern{mt_rand(0,31)};
    	}
    	
    	return $salt;
    }

    /**
	 * 验证登录是否过期
	 * @author   yichuanxia
	 * @Date     2017/2/13
	 * @phone    15972053215
	 * @param    int           $uid   用户id
	 * @return   bool  
	 */
	public function vilidateExpire($uid){
		//return true;
		$res = $this->getInfo('last_login_time',array('id' => $uid),'user','');
		if($res){
			$config = C('Config');
			$token_expire_time = $config['token_expire_time'] * 24 * 60 * 60;
			if((time() - $res['last_login_time']) < $token_expire_time ){
				return true;
			}
		}
		return false;
    }

    /**
	 * 修改资料
	 * @author   yichuanxia
	 * @Date     2017/1/19
	 * @phone    15972053215
	 * @param    array   $data   修改数据的数组
	 * @param    array   $where  修改条件
	 * @param    string  $table  修改表名
	 * @return   bool  
	 */
	public function editData($data,$where,$table = 'user'){
		if(!is_array($data)){
			return false;
		}
		if(!$where){
			return false;
		}
		if(!$table){
			return false;
		}
		$db_prefix = C('DB_PREFIX');
    	$sql = "show columns from {$db_prefix}{$table}";
    	$result = M('user')->query($sql);
    	if(!$result){
    		return false;
    	}
    	$columns = array();
    	foreach ($result as $key => $value) {
    		$columns[] = $value['Field'];
    	}

    	/*验证字段*/
    	foreach($data as $key => $val){
    		if(!in_array($key, $columns)){
    			return false;
    		}
    	}

    	/*数据保存*/
    	$res = M($table)->where($where)->save($data);
    	if($res >= 0){
    		return true;
    	}
    	return false;
    }

    /**
	 * 数据查询(含分页,此方法只适用单表操作)
	 * @author   yichuanxia
	 * @Date     2017/1/19
	 * @phone    15972053215
	 * @param    string  $field    查询字段
	 * @param    array   $where    查询条件
	 * @param    string  $table    表名
	 * @param    string  $moduel   图片保存目录
	 * @param    int     $page     页码
	 * @param    int     $show_num 显示条数
	 * @return   mix  
	 */
	public function getData($field,$where,$table,$moduel = '',$page = 1,$show_num = 10,$order = 'crdate DESC'){
		$result = M($table)
				->where($where)
				->limit(($page-1)*$show_num,$show_num)
				->order($order)
				->Field($field)
				->select();
		if($result){
			foreach($result as $key => &$val){
				if($moduel){
					/*图片处理(含多图跟单图) 开始*/
					/*单图处理 开始*/
					if($val['image']){
						$val['image'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/Uploads/'.$moduel.'/'.date('Ym',$val['crdate']).'/'.$val['image'];// /thumb_0
					}
					/*单图处理 结束*/

					/*多图处理 开始*/
					$images = M('attach')->where(array('info_id' => $val['id'],'info_tab' => $moduel))->Field('crdate,path')->select();
					if($images){
						foreach($images as $k => $v){
							$val['images'][$k] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/Uploads/'.$moduel.'/attach/'.date('Ym',$v['crdate']).'/'.$v['path'];
						}
					}
					/*多图处理 结束*/
					/*图片处理(含多图跟单图) 结束*/
				}

				/*时间处理 开始*/
				if($val['crdate']){
					$val['crdate'] = date("Y-m-d H:i:s",$val['crdate']);
				}
				/*时间处理 结束*/
				/*处理链接 开始*/
				if($val['link']){
					$val['link'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$val['link'];
				}
				/*处理链接 结束*/
			}

		}
		return $result?$result:array();
    }

    /**
	 * 数据查询(此方法只适用单表操作)
	 * @author   yichuanxia
	 * @Date     2017/1/19
	 * @phone    15972053215
	 * @param    string  $field    查询字段
	 * @param    array   $where    查询条件
	 * @param    string  $table    表名
	 * @param    string  $moduel   图片保存目录
	 * @param    string  $order    排序规则
	 * @return   mix  
	 */
	public function getAllData($field,$where,$table,$moduel = '',$order = 'crdate DESC'){
		$result = M($table)
				->where($where)
				->order($order)
				->Field($field)
				->select();
		if($result){
			foreach($result as $key => &$val){
				if($moduel){
					/*图片处理(含多图跟单图) 开始*/
					/*单图处理 开始*/
					if($val['image']){
						$val['image'] =$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/Uploads/'.$moduel.'/'.date('Ym',$val['crdate']).'/'.$val['image'];// /thumb_0
					}
					/*单图处理 结束*/

					/*多图处理 开始*/
					$images = M('attach')->where(array('info_id' => $val['id'],'info_tab' => $moduel))->Field('crdate,path')->select();
					if($images){
						foreach($images as $k => $v){
							$val['images'][$k] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/Uploads/'.$moduel.'/attach/'.date('Ym',$v['crdate']).'/'.$v['path'];
						}
					}
					/*多图处理 结束*/
					/*图片处理(含多图跟单图) 结束*/
				}

				/*时间处理 开始*/
				if($val['crdate']){
					$val['crdate'] = date("Y-m-d H:i:s",$val['crdate']);
				}
				/*时间处理 结束*/
				/*处理链接 开始*/
				if($val['link']){
					$val['link'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$val['link'];
				}
				/*处理链接 结束*/
			}

		}
		return $result?$result:array();
    }

    /**
	 * 获取数据详情
	 * @author   yichuanxia
	 * @Date     2017/1/19
	 * @phone    15972053215
	 * @param    string  $field   查询的字段
	 * @param    array   $where  查询条件
	 * @param    string  $table  表名
	 * @param    string  $moduel 图片保存目录
	 * @return   mix  
	 */
	public function getInfo($field,$where,$table = 'user',$moduel = ''){
		$result = M($table)->where($where)->Field($field)->find();
		if($result){
			if($moduel){
				/*单图处理 开始*/
				if($result['image']){
					$result['image'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/Uploads/'.$moduel.'/'.date('Ym',$result['crdate']).'/'.$result['image'];///thumb_0
				}
				/*单图处理 结束*/

				/*多图处理 开始*/
				$images = M('attach')->where(array('info_id' => $result['id'],'info_tab' => $moduel))->Field('crdate,path')->select();
				if($images){
					foreach($images as $k => $v){
						$result['images'][$k] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/Uploads/'.$moduel.'/attach/'.date('Ym',$v['crdate']).'/'.$v['path'];
					}
				}
				/*多图处理 结束*/
			}
			/*时间处理 开始*/
			if($result['crdate']){
				$result['crdate'] = date("Y-m-d H:i:s",$result['crdate']);
			}
			/*时间处理 结束*/

			/*处理链接 开始*/
			if($result['link']){
				$result['link'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$result['link'];
			}
			/*处理链接 结束*/
		} 

		return $result?$result:array();
    }

    /**
	 * 数据查询(含分页,此方法适用多表操作)
	 * @author   yichuanxia
	 * @Date     2017/1/19
	 * @phone    15972053215
	 * @param    string  $field        查询字段
	 * @param    array   $where        查询条件
	 * @param    array   $table        表名(数组)
	 * @param    array   $union_where  关联条件(对应关联表的关联条件)
	 * @param    string  $moduel       图片保存目录
	 * @param    int     $page         页码
	 * @param    int     $show_num     显示条数
	 * @return   mix  
	 */
	public function getDataLists($field,$where,$table,$union_where,$moduel = '',$page = 1,$show_num = 10){
		$obj = '';
		foreach ($table as $key => $value) {
			if($key === 0){
				$obj = M($value);
			}else{
				$obj->join(C('DB_PREFIX').$value." on {$union_where[$key - 1]}");
			}
		}
		$result = $obj->where($where)->Field($field)->limit(($page-1)*$show_num,$show_num)->select();			
		if($result){
			foreach($result as $key => &$val){
				if($moduel){
					/*图片处理(含多图跟单图) 开始*/
					/*单图处理 开始*/
					if($val['image']){
						$val['image'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/Uploads/'.$moduel.'/'.date('Ym',$val['crdate']).'/'.$val['image'];
					}
					/*单图处理 结束*/

					/*多图处理 开始*/
					$images = M('attach')->where(array('info_id' => $val['id'],'info_tab' => $moduel))->Field('crdate,path')->select();
					if($images){
						foreach($images as $k => $v){
							$val['images'][$k] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/Uploads/'.$moduel.'/attach/'.date('Ym',$v['crdate']).'/'.$v['path'];
						}
					}
					/*多图处理 结束*/
					/*图片处理(含多图跟单图) 开始*/
				}

				/*时间处理 开始*/
				if($val['crdate']){
					$val['crdate'] = date("Y-m-d H:i:s",$val['crdate']);
				}
				/*时间处理 结束*/

				/*处理链接 开始*/
				if($val['link']){
					$val['link'] =$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$val['link'];
				}
				/*处理链接 结束*/
			}

		}
					

		return $result?$result:array();
    }
    /**
	 * 数据查询(此方法适用多表操作)
	 * @author   yichuanxia
	 * @Date     2017/1/19
	 * @phone    15972053215
	 * @param    string  $field        查询字段
	 * @param    array   $where        查询条件
	 * @param    array   $table        表名(数组)
	 * @param    array   $union_where  关联条件(对应关联表的关联条件)
	 * @param    string  $moduel       图片保存目录
	 * @return   mix  
	 */
	public function getAllDataLists($field,$where,$table,$union_where,$moduel = ''){
		$obj = '';
		foreach ($table as $key => $value) {
			if($key === 0){
				$obj = M($value);
			}else{
				$obj->join(C('DB_PREFIX').$value." on {$union_where[$key - 1]}");
			}
		}
		$result = $obj->where($where)->Field($field)->select();			
		if($result){
			foreach($result as $key => &$val){
				if($moduel){
					/*图片处理(含多图跟单图) 开始*/
					/*单图处理 开始*/
					if($val['image']){
						$val['image'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/Uploads/'.$moduel.'/'.date('Ym',$val['crdate']).'/'.$val['image'];
					}
					/*单图处理 结束*/

					/*多图处理 开始*/
					$images = M('attach')->where(array('info_id' => $val['id'],'info_tab' => $moduel))->Field('crdate,path')->select();
					if($images){
						foreach($images as $k => $v){
							$val['images'][$k] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/Uploads/'.$moduel.'/attach/'.date('Ym',$v['crdate']).'/'.$v['path'];
						}
					}
					/*多图处理 结束*/
					/*图片处理(含多图跟单图) 开始*/
				}

				/*时间处理 开始*/
				if($val['crdate']){
					$val['crdate'] = date("Y-m-d H:i:s",$val['crdate']);
				}
				/*时间处理 结束*/

				/*处理链接 开始*/
				if($val['link']){
					$val['link'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$val['link'];
				}
				/*处理链接 结束*/
			}

		}
					

		return $result?$result:array();
    }

    /**
	 * 信息验证
	 * @author   yichuanxia
	 * @Date     2017/2/7
	 * @phone    15972053215
	 * @param    array      $where         查询条件
	 * @param    string     $table         表名
	 * @return   bool  
	 */
    public function checkInfo($where,$table = 'user'){
    	$result = M($table)->where($where)->select();
    	if($result){
    		return true;
    	}
    	return false;
    }

    /**
	 * 数据空值验证
	 * @author   yichuanxia
	 * @Date     2017/2/9
	 * @phone    15972053215
	 * @param    array     $data         待验证数组
	 * @return   bool  
	 */
    public function validateData($data){
    	if(is_array($data) && $data){
    		foreach ($data as $key => $value) {
    			if($value === ''){
    				return false;
    			}
    		}
    	}
    	return true;
    }

    /**
	 * 数据统计
	 * @author   yichuanxia
	 * @Date     2017/2/10
	 * @phone    15972053215
	 * @param    string  $field    查询字段
	 * @param    array   $where    查询条件
	 * @param    string  $table    表名
	 * @return   mix  
	 */
	public function dataCount($field,$where,$table){
		$result = M($table)
				->where($where)
				->Field($field)
				->select();
		return $result?$result:array();
    }

    /**
	 * 金额验证
	 * @author   yichuanxia
	 * @Date     2017/3/6
	 * @phone    15972053215
	 * @param    string      $money         金额
	 * @return   bool  
	 */
    public function matchMoney($money){
    	$match = '/^[0-9]+(.[0-9]{1,2})?$/';
    	if(!preg_match($match, $money)){
    		return false;
    	}
    	if(preg_match('/\./', $money)){
    		if(preg_match('/^0[0-9]/', $money)){
    			return false;
    		}
    		return true;
    	}else{
    		if(preg_match('/^0/', $money)){
    			return false;
    		}
    		return ture;
    	}
    }

    /**
	 * 获取某个字段值
	 * @author   yichuanxia
	 * @Date     2017/2/7
	 * @param    array      $where         查询条件
	 * @param    string     $table         表名
	 * @param    string     $field         字段  
	 */
    public function getField($where,$table = 'user',$field = 'name'){
    	$result = M($table)->where($where)->getField($field);
    	return $result;
    }

   /** 
    * 图片地址替换 
    * @param string $content 内容 
    * @param string $suffix 拼接内容 
    */  
     public function get_img_thumb_url($content="",$suffix=''){  
        // by http://www.manongjc.com/article/1319.html  
        $pregRule = "/<[img|IMG].*?src=[\'|\"](.*?(?:[\.jpg|\.jpeg|\.png|\.gif|\.bmp]))[\'|\"].*?[\/]?>/";  
        $content = preg_replace($pregRule, '<img src="'.$suffix.'${1}" style="max-width:100%">', $content);  
        return $content;  
    }

    /**
	 * 数据统计
	 * @author   yichuanxia
	 * @Date     2017/6/9
	 * @phone    15972053215
	 * @param    array   $where    查询条件
	 * @param    string  $table    表名
	 * @return   int  
	 */
	public function totalCount($where,$table){
		$result = M($table)
				->where($where)
				->count();
		return $result;
    }
}