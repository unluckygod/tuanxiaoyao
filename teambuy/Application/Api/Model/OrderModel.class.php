<?php

namespace Api\Model;

class OrderModel extends BaseModel
{
    protected $_link = array(
        /*关联订单详情*/
        'Group'=>array(
            'mapping_type'      => self::BELONGS_TO,      //关联类型
            'class_name'        => 'Group',         //关联模型
            'foreign_key'       => 'group_id',          //关联外键名称
        ),

        /*关联商品*/
        'Goods' => array(
            'mapping_type'      => self::HAS_ONE,
            'class_name'        => 'Goods',
            'foreign_key'       => 'goods_id'
        ),

        /*关联商品详情*/
        'GoodsInfo' => array(
            'mapping_type'      => self::HAS_ONE,
            'class_name'        => 'goods_info',
            'foreign_key'       => 'goods_info_id'
        )
    );
    
    /**
     * 生成订单号
     *
     */
    public function createOrderNum(){
    	static $result = '';
    	$result = date('YmdHis',time());
    	while($this->checkInfo(array('order_num' => $result),'order')){
    		sleep(1);
    		$result = date('YmdHis',time());
    	}
    	return $result;
    }
    
}

?>