<?php

namespace Api\Model;

class MemberModel extends BaseModel
{
	protected $_link = array(
		/*关联订单*/
        'Order'=>array(
            'mapping_type'      => self::HAS_MANY,   	//关联类型
            'class_name'        => 'Order',         	//关联模型
            'foreign_key'		=> 'user_id',        	//关联外键名称
            'mapping_fields'    => 'id,pay_price,start,end,type,status',
            'mapping_order'     => 'crdate desc'
        ),
        /*关联优惠券*/
        'UserCoupon'=>array(
            'mapping_type'      => self::HAS_MANY,   	//关联类型
            'class_name'        => 'UserCoupon',        //关联模型
            'foreign_key'		=> 'user_id',        	//关联外键名称
            'mapping_fields'    => '*',
            'mapping_order'     => 'money desc'
        ),
        /*关联佣金*/
        'Commission'=>array(
            'mapping_type'      => self::HAS_MANY,  	//关联类型
            'class_name'        => 'Commission',        //关联模型
            'foreign_key'		=> 'user_id',			//关联外键名称
            'mapping_fields'	=> '*',					//待查询字段
            'mapping_order'     => 'crdate desc'
        ),
        /*关联资产*/
        'Asset'=>array(
            'mapping_type'      => self::HAS_MANY,      //关联类型
            'class_name'        => 'Asset',             //关联模型
            'foreign_key'       => 'user_id',           //关联外键名称
            'mapping_fields'    => '*',                 //待查询字段
            'mapping_order'     => 'crdate desc'
        ),

        /*关联提现*/
        'Cash'=>array(
            'mapping_type'      => self::HAS_MANY,      //关联类型
            'class_name'        => 'Cash',             //关联模型
            'foreign_key'       => 'user_id',              //关联外键名称
            'mapping_fields'    => '*',                 //待查询字段
            'mapping_order'     => 'crdate desc'
        ),

        /*关联收藏*/
        'Collect'=>array(
        	'mapping_type'		=> self::HAS_MANY, 	   //关联类型
        	'class_name'        => 'Goods',        		//关联模型
            'foreign_key'		=> 'user_id',			//关联外键名称
            'mapping_fields' 	=> '*', 				//待查询字段
        ),

        /*关联评论*/
        'Comment'=>array(
        	'mapping_type'		=> self::HAS_MANY, 		//关联类型
        	'class_name'        => 'Comment',        	//关联模型
            'foreign_key'		=> 'user_id',			//关联外键名称
            'mapping_fields' 	=> '*', 				//待查询字段
        ),

        /*关联地址*/
        'UserAddress'=>array(
            'mapping_type'      => self::HAS_MANY,
            'class_name'        => 'UserAddress',
            'foreign_key'       => 'user_id',
            'condition'         => 'deleted=0',
            'mapping_order'     => 'is_default desc'
        ),
    );
}