<?php 
namespace Api\Model;

class CommentModel extends BaseModel{
	protected $_link = array(
        /*关联附件*/
        'Attach'=>array(
            'mapping_type'      => self::HAS_MANY,   //关联类型
            'class_name'        => 'Attach',         //关联模型
            'foreign_key'		=> 'info_id',        //关联主键
            'condition'			=> "info_tab = 'Comment'",    //额外关联条件
            'mapping_fields' 	=> 'path,crdate'     //待查询字段
        ),
        /*关联用户*/
        'Member'=>array(
            'mapping_type'      => self::BELONGS_TO,
            'class_name'        => 'User',
            'foreign_key'       => 'user_id',
        ),

        /*关联订单*/
        'Order'=>array(
            'mapping_type'      => self::BELONGS_TO,
            'class_name'        => 'Order',
            'foreign_key'       => 'order_id',
        ),
    );
}