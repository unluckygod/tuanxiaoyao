<?php
return array(
    //'配置项'=>'配置值'
    'URL_MODEL' => 0,
    'URL_CASE_INSENSITIVE'=>true,
    //短信验证码间隔发送时间
    'SMS_GAP_TIME'=>60,
    //短信验证码过期时间
    'SMS_EXPIRED_TIME' => 120,
    'ERROR_PAGE' => '',
    'RESULT_CODE' => array(
        'fail' =>  array('code' => 0, 'msg' => '操作失败！'),
        'success' => array('code' => 1, 'msg' => '操作成功！'),
        'lawless' =>  array('code' => 10, 'msg' => '服务器开小差了'),
        'sys_busy' =>  array('code' => 20, 'msg' => '系统繁忙！'),
        'no_more_data' =>  array('code' => 30, 'msg' => '暂无更多数据'),
        'param_error'=>array('code'=>40,'msg' => '参数错误'),

        //优惠券
        'is_recevied' => array('code' => 200,'msg' => '已经领过了'),
        'phone_type_error' => array('code' => 201,'msg' => '手机号格式错误'),
        'phone_unsame' => array('code' => 202,'msg' => '手机号已被绑定'),
        'pwd_type_error' => array('code' => 203,'msg' => '密码为6-16的数字字母组合'),
        'pwd_unsame' => array('code' => 204,'msg' => '两次密码不一致'),
        'invate_code_error' => array('code' => 205,'msg' => '邀请码不存在'),
        'phone_missing' => array('code' => 207,'msg' => '首次请用手机号验证码登录'),
        

        //短信验证
        'sms_code_error' => array('code' => 100, 'msg' => '短信验证码错误！'),
        'sms_code_expired' => array('code' => 101, 'msg' => '短信验证码超时！'),
        'sms_code_wait' => array('code' => 102, 'msg' => '您刚才已经发送过短信了，请耐心等待几秒！'),
        'sms_verify_error' => array('code' => 103,'msg' => '图形验证码错误'),
        'sendSMS_error' => array('code' => 104,'msg' => '短信发送失败'),
        'openid_exists' => array('code' => 105,'msg' => '此微信已绑定其他手机号'),

        //登录验证
        'login_failed' => array('code' => 300, 'msg' => '账号或密码有误，请重新输入'),
        'phone_failed' => array('code' => 301, 'msg' => '手机号格式错误'),

        /*优惠券*/
        'coupon_not_exists' => array('code' => 601, 'msg' => '优惠券不存在'),
        'coupon_is_deleted' => array('code' => 602, 'msg' => '优惠券已作废'),
        'coupon_is_expired' => array('code' => 603, 'msg' => '优惠券已过期'),
        'coupon_is_finished' => array('code' => 604, 'msg' => '优惠券被领光了'),
        'coupon_is_overtop' => array('code' => 605, 'msg' => '领多了会长胖的...'),

        'ofen_order' => array('code' => 700,'msg' => '此vip订单尚未支付,请勿重复下单'),
        'not_expired' => array('code' => 701,'msg' => '此vip尚未过期,无需充值'),
        'partner_exists' => array('code' => 702,'msg' => '您已填写过加盟信息,请勿重复添加'),
        'companies_not_exists' => array('code' => 703,'msg' => '请先完善企业信息'),

        'activity_expired' => array('code' => 704,'msg' => '活动已过期'),
        'get_upper_limit' => array('code' => 705,'msg' => '当天报名已达上限'),

        /*订单模块*/
        'activity_over' => array('code' => 706,'msg' => '活动已结束'),
        'over_activity_limit' => array('code' => 707,'msg' => '已超过活动限制购买数量'),
        'coupon_not_exitst' => array('code' => 710,'msg' => '优惠券不存在'),
        'coupon_type_error' => array('code' => 711,'msg' => '优惠券类型错误'),
        'user_not_exists' => array('code' => 716,'msg' => '该团仅适用于新用户'),
        'is_not_colonel' => array('code' => 717,'msg' => '用户暂无建团权限'),
        'group_over' => array('code' => 718,'msg' => '此团已满'),
        'date_deny' => array('code' => 719,'msg' => '日期格式错误'),
        'money_type_deny' => array('code' => 720,'msg' => '团购价格式错误'),
        'money_too_lower' => array('code' => 721,'msg' => '团购价不能低于分销价'),
        'num_too_lower' => array('code' => 722,'msg' => '拼团人数应在2个及以上'),
        'just_one_time' => array('code' => 723,'msg' => '请勿重复参加'),
        'is_full_member' => array('code' => 724,'msg' => '该团已满员'),

        /*商品价格库存详情*/
        'goods_not_exit' => array('code' => 801,'msg' => '商品找不到了'),
        'goods_is_down' => array('code' => 802,'msg' => '商品已下架'),
        'stock_not_enough' => array('code' => 803,'msg' => '库存不足'),

        /*申请提现*/
        'asset_not_enough' => array('code' => 901,'msg' => '可提现佣金不足'),
        'account_missing' => array('code' => 902,'msg' => '请填写提现到账账号'),
        'name_missing' => array('code' => 903,'msg' => '请填写姓名'),
        'type_error' => array('code' => 904,'msg' => '请选择提现方式'),
        'money_error' => array('code' => 905,'msg' => '提现金额格式错误'),

        /*申请退款*/
        'reason_missing' => array('code' => 906,'msg' => '请选择退款原因'),
        'overtime' => array('code' => 907,'msg' => '已超过可申请退款时间'),
        'order_type_error' => array('code' => 908,'msg' => '商品订单不支持退款'),
        'room_type_error' => array('code' => 909,'msg' => '活动房不支持退款'),
        'application_exists' => array('code' => 910,'msg' => '请勿重复申请退款'),


        /*评价*/
        'content_missing' => array('code' => 1010,'msg' => '请填写评价内容'), 
        'order_commented' => array('code' => 1011,'msg' => '该订单已经评价过了'),
        'order_status_error' => array('code' => 1012,'msg' => '订单状态错误'),
        'order_expired' => array('code' => 1013,'msg' => '订单已失效'),
        'order_paied' => array('code' => 1014,'msg' => '请勿重复支付'),
    )
    

);