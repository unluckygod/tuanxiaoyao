<?php
namespace Api\Util;

trait ValidateTrait{
	public function  response(){
		$data = I('param.');
		foreach ($this->tca as $key => $value) {
			switch ($value['method']) {
				case 'exists':
					$result = $this->modelCheck($value['model'],$data[$key]);
					break;
				default:
					$result = $this->regex($value['model'],$data[$key]);
					break;
			}
			if(!$result){
				return ['return_msg' => $value['code_key'],'return_key' => $key];
				break;
			}
		}
		return ['retrun_code' => 0];
	}

	public function modelCheck($model,$value){
		if(!isset($value) || !$value){
			return false;
		}
		$model = $model?:CONTROLLER_NAME;
		return D($model)->find($value)?true:false;
	}

	public function regex($reg,$value){
		return preg_match($reg, $value);
	}
}