<?php

namespace Api\Util;
class Test {
	public $url;
    public $param;
    public $method;
    public $dataType;
    
    public function __construct($url,$param,$method,$dataType) {
        $this->url = $url;
        $this->param = $param;
        $this->method = $method?$method:'get';
        $this->dataType = $dataType?$dataType:'json';
    }
    public function getResult(){
    	return $this->curl($this->url,$this->param,$this->method,$this->dataType);
    }
    public function curl($url,$data,$method,$dataType='json'){

	    //初始化curl句柄，
	    $ch = curl_init();
        /*设置不使用CA证书*/
        $opt[CURLOPT_SSL_VERIFYHOST] = 1;
        $opt[CURLOPT_SSL_VERIFYPEER] = FALSE;
        curl_setopt_array($ch, $opt);
	    //设置服务器返回的数据不直接输出，而是保留在curl_exec()的返回值中
	    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	    if($method == 'get'){
	    	$param = '';
	    	foreach($data as $key => $val){
	    		$param .=  $key.'='.$val.'&';
	    	}
	    	$param = substr($param,0,-1);
            curl_setopt($ch,CURLOPT_URL,$url.'?'.$param);
	    }else if($method == 'post'){
	    	curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_POST,true);
            switch($dataType){
            	case 'json':
            		$data = json_encode($data);
            	break;
            	case 'xml':
	            	$xml = '<xml>';
	            	foreach($data as $key => $val){
	            		$xml .= '<'.$key.'>'.$val.'</'.$key.'>';
	            	}
                    $xml .= '</xml>';
	            	$data = $xml;
            	default :
            	break;
            }
            curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
	    }
	    $return = curl_exec($ch);
        curl_close($ch);
        return $return;

	}
}