<?php
namespace Api\Controller;

class GoodsController extends APIController {
    public $tbName = CONTROLLER_NAME;

    /**
     * 列表页面
     */
    public function index() {
        try{
            $this->safeChecked();
            $page = I('param.page',C('PAGE'));
            $show_num = I('param.show_num',C('SHOW_NUM'));
            $Obj = D($this->tbName); //实例化对象
            $where = [
                'deleted' => 0,
                'hidden' => 0
            ];
            $list = $Obj
                    ->where($where)
                    ->Field('id,name,suggest_start_price,suggest_end_price,image,crdate,type')
                    ->order('crdate desc')
                    ->limit(($page -1)*$show_num,$show_num)
                    ->select();
            $GoodsInfo = M('goods_info');
            foreach ($list as $key => &$value) {
                $value['image'] = C('DOMAIN_URL').C('PATH_UPLOAD').$this->tbName.'/'.date('Ym',$value['crdate']).'/'.$value['image'];
                $value['stock'] = $GoodsInfo->where(array('deleted' => 0,'goods_id' => $value['id']))->sum('stock')?:0;
                $goods_info = $GoodsInfo->where(array('deleted' => 0,'goods_id' => $value['id']))->Field('id,assistant,distribution_price,stock')->order('distribution_price asc')->select();
                if($value['type'] == 0){
                    $value['price'] = $goods_info[0]['distribution_price'];
                }else{
                    $value['price'] = $goods_info[0]['distribution_price'].'-'.$goods_info[count($goods_info)-1]['distribution_price'];
                }
                $value['goods_info'] = $goods_info;
            }
            $this->res_data['data'] = $list?:array();
        }catch(\Exception $e){
            debug($e->getmessage());
            $this->res_data['code_key'] = 'fail';
        }
        $this->apiReply();
    }
}

?>