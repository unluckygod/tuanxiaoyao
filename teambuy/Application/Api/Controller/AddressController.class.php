<?php
namespace Api\Controller;

class AddressController extends APIController {
    public $tbName = 'user_address';

    /*待验证的参数*/
    public $tca = [
        'user_id' => [
            'method' => 'exists',
            'model' => 'member',
            'code_key' => 'lawless'
        ]
    ];

    /*保存参数*/
    public $field = ['user_id','address','name','phone','area']; 
    /**
     * 列表页面
     */
    public function index() {
        try{
            $this->safeChecked();
            /*验证*/
            $check = D('Validate','Util');
            $result = $check->letParam($this->tca)->response();
            if(isset($result['return_msg'])){
                $this->res_data['code_key'] = $result['return_msg'];
                $this->apiReply();
            }
            $Obj = D($this->tbName);
            $user_id = I('param.user_id');
            $result = $Obj->where(array('user_id' => $user_id,'deleted' => 0))->order('is_default desc,crdate desc')->select();
            /*获取收藏酒店*/
            $this->res_data['data'] = $result?:array();
        }catch(\Exception $e){
            debug($e->getmessage());
            $this->res_data['code_key'] = 'fail';
        }
        $this->apiReply();
    }

    /**
     * 地址详情
     */
    public function info(){
        try{
            $this->safeChecked();
            /*验证*/
            $check = D('Validate','Util');
            $this->tca = array(
                'address_id' => array(
                    'method' => 'exists',
                    'model'  => 'user_address',
                    'code_key' => 'lawless',
                )
            );
            $result = $check->letParam($this->tca)->response();
            if(isset($result['return_msg'])){
                $this->res_data['code_key'] = $result['return_msg'];
                $this->apiReply();
            }
            $address_id = I('param.address_id');
            $Obj = D($this->tbName);
            $result = $Obj->find($address_id);
            $this->res_data['data'] = $result?:array();
        }catch(\Exception $e){
            debug($e->getmessage());
            $this->res_data['code_key'] = 'fail';
        }
        $this->apiReply();
    }

    /**
     * 删除或者设置默认地址
     */
    public function edit(){
        try{
            $this->safeChecked();
            /*验证*/
            $check = D('Validate','Util');
            $this->tca = array(
                'address_id' => array(
                    'method' => 'exists',
                    'model'  => 'user_address',
                    'code_key' => 'lawless',
                ),
                'action' => array(
                    'method' => 'regex',
                    'model'  => '/^(default|del)$/',
                    'code_key' => 'lawless',
                ),

            );
            $result = $check->letParam($this->tca)->response();
            if(isset($result['return_msg'])){
                $this->res_data['code_key'] = $result['return_msg'];
                $this->apiReply();
            }
            $address_id = I('param.address_id');
            $action = I('param.action');
            $Obj = D($this->tbName);
            if($action == 'default'){
                $data = array('is_default' => 1,'tstamp' => time());
                $Obj->where(array('user_id' => I('param.user_id'),'deleted' => 0,'is_default' => 1))->setDec('is_default');
            }else{
                $data = array('deleted' => 1,'tstamp' => time());
            }
            $result = $Obj->where(array('id' => $address_id))->save($data);
        }catch(\Exception $e){
            debug($e->getmessage());
            $this->res_data['code_key'] = 'fail';
        }
        $this->apiReply();
    }

    /**
     *	新增/编辑
     */
    public function save(){
    	try{
            $this->safeChecked();
           /*验证*/
            $check = D('Validate','Util');
            $this->tca = array(
                'user_id' => array(
                    'method' => 'exists',
                    'model' => 'member',
                    'code_key' => 'lawless'
                ),
                'name' => array(
                    'method' => 'regex',
                    'model'  => '/\S/',
                    'code_key' => 'lawless',
                ),
                'address' => array(
                    'method' => 'regex',
                    'model'  => '/\S/',
                    'code_key' => 'lawless',
                ),
                'area' => array(
                    'method' => 'regex',
                    'model'  => '/\S/',
                    'code_key' => 'lawless',
                ),
            );
            $id = I('param.id',null);
            if($id){
                $this->tca = array_merge(array('id' => array('method' => 'exists','model' => 'user_address','code_key' => 'lawless')),$this->tca);
            }
            $result = $check->letParam($this->tca)->response();
            if(isset($result['return_msg'])){
                $this->res_data['code_key'] = $result['return_msg'];
                $this->apiReply();
            }
            $data = I('param.');
            /*过滤*/
            $Obj = D($this->tbName);
            //数据保存
            $this->filterCoupon($data);
            $Obj->create($data);
            if($id){
                $Obj->tstamp = time();
                $Obj->where(array('id' => $id))->save();
            }else{
                $Obj->crdate = $Obj->tstamp = time();
                $Obj->add();
            }
    	}catch(\Exception $e){
    		debug($e->getmessage());
            $this->res_data['code_key'] = 'fail';
    	}
        $this->apiReply();
    }

    /*数据筛选*/
    public function filterCoupon(&$data){
        foreach ($data as $key => $value) {
            if(!in_array($key,$this->field)){
                unset($data[$key]);
            }
        }
    }
}

?>