<?php
namespace Api\Controller;

class BannerController extends APIController {
    public $tbName = CONTROLLER_NAME;

    /**
     * 列表页面
     */
    public function index() {
        try{
            $this->safeChecked();
            static $data = array();
            $Obj = D($this->tbName); //实例化对象
            $Where = "deleted=0";
            $list = $Obj->where($Where)->Field('crdate,image')->order('tstamp desc')->limit(0,5)->select();
            $data['banner'] = array();
            if($list){
                foreach ($list as $key => $value) {
                    $data['banner'][] = C('DOMAIN_URL').C('PATH_UPLOAD').CONTROLLER_NAME.'/thumb_0/'.date('Ym',$value['crdate']).'/'.$value['image'];
                }
            }
            $this->res_data['data'] = $data?:array();
        }catch(\Exception $e){
            debug($e->getmessage());
            $this->res_data['code_key'] = 'fail';
        }
        $this->apiReply();
    }
}

?>