<?php
namespace Api\Controller;

class CommissionController extends WXController
{	
    private $tbName = CONTROLLER_NAME;
    /*待验证的参数*/
    public $tca = [
        'user_id' => [
            'method' => 'exists',
            'model' => 'member',
            'code_key' => 'lawless'
        ],
    ];

    /**
     * 首页
     */
    public function index() {
        try{
            static $data = [];
            $this->safeChecked();
            /*验证*/
            $check = D('Validate','Util');
            $result = $check->letParam($this->tca)->response();
            if(isset($result['return_msg'])){
                $this->res_data['code_key'] = $result['return_msg'];
                $this->apiReply();
            }
            $user_id = I('param.user_id');
            $Cash = M('cash');
            //获取结算收益 已同意的提现佣金
            $cash_total = $Cash->where(array('user_id' => $user_id,'status' => 2))->sum('money')?:0;
            $cashing_total = $Cash->where(array('user_id' => $user_id,'status' => 1))->sum('money')?:0;
            $Commission = M($this->tbName);
            //获取可提现佣金   总佣金 - 审核中的佣金 - 已提现的佣金
            $commission_total = $Commission->where(array('user_id' => $user_id))->sum('money')?:0;
            $valid_total = $commission_total - $cash_total - $cashing_total;

            //获取最近两个超级团长拼团成功的团购收益订单
            $Group = M('group');
            $Order = M('order');
            $Goods = M('goods');
            $result = $Group->where(array('user_id' => $user_id,'complete_time' => array('gt',0),'type' => 3))->Field('id,group_num,num,activity_id,goods_id')->order('crdate desc')->limit('0,2')->select();
            $result = $result?:[];
            if($result){
                foreach ($result as $key => &$value) {
                    $value['order'] = $Order
                                    ->alias('a')
                                    ->join(C('DB_PREFIX').'memeber as b ON a.user_id = b.id')
                                    ->where(array('group_id' => $value['id']))
                                    ->Field('a.num,a.deal_money,a.crdate,b.wxlogo')
                                    ->select();
                    foreach ($value['order'] as &$val) {
                        $val['crdate'] = date('Y-m-d H:i:s',$val['crdate']);
                    }
                    //团购价
                    $value['group_price'] = $value['order'][0]['deal_money'];
                    //总成交金额
                    $value['sales'] = $Order->where(array('group_id' => $value['id']))->sum('total_price');
                    //总分销金额
                    $value['distribution'] = $Order->where(array('group_id' => $value['id'],'is_collect_bills' => 0))->sum('distribution_money');
                    //总佣金
                    $value['commission'] = $value['sales'] - $value['distribution'];
                    //商品信息
                    $goods_info = $Goods->Field('image,crdate')->find($value['goods_id']);
                    $value['image'] = C('DOMAIN_URL').C('PATH_UPLOAD').'Goods/'.date('Ym',$goods_info['crdate']).'/'.$goods_info['image'];
                }
            }
            $data = [
                'cash' => $cash_total,
                'valid' => $valid_total,
                'list' => $result
            ];

            $this->res_data['data'] = $data?:array();
        }catch(\Exception $e){
            debug($e->getmessage());
            $this->res_data['code_key'] = 'fail';
        }
        $this->apiReply();
    }

    /**
     * 列表页面
     */
    public function list() {
        try{
            static $data = [];
            $this->safeChecked();
            /*验证*/
            $check = D('Validate','Util');
            $result = $check->letParam($this->tca)->response();
            if(isset($result['return_msg'])){
                $this->res_data['code_key'] = $result['return_msg'];
                $this->apiReply();
            }
            $user_id = I('param.user_id');
            $page = I('param.page',C('PAGE'));
            $show_num = I('param.show_num',C('SHOW_NUM'));
            //获取最近两个超级团长拼团成功的团购收益订单
            $Group = M('group');
            $Order = M('order');
            $Goods = M('goods');
            $data = $Group
                    ->where(array('user_id' => $user_id,'complete_time' => array('gt',0),'type' => 3))
                    ->Field('id,group_num,num,activity_id,goods_id')
                    ->order('crdate desc')
                    ->limit(($page -1)*$show_num,$show_num)
                    ->select();
            if($data){
                foreach ($data as $key => &$value) {
                    $value['order'] = $Order
                                    ->alias('a')
                                    ->join(C('DB_PREFIX').'memeber as b ON a.user_id = b.id')
                                    ->where(array('group_id' => $value['id']))
                                    ->Field('a.num,a.deal_money,a.crdate,b.wxlogo')
                                    ->select();
                    foreach ($value['order'] as &$val) {
                        $val['crdate'] = date('Y-m-d H:i:s',$val['crdate']);
                    }
                    //团购价
                    $value['group_price'] = $value['order'][0]['deal_money'];
                    //总成交金额
                    $value['sales'] = $Order->where(array('group_id' => $value['id']))->sum('total_price');
                    //总分销金额
                    $value['distribution'] = $Order->where(array('group_id' => $value['id'],'is_collect_bills' => 0))->sum('distribution_money');
                    //总佣金
                    $value['commission'] = $value['sales'] - $value['distribution'];
                    //商品信息
                    $goods_info = $Goods->Field('image,crdate')->find($value['goods_id']);
                    $value['image'] = C('DOMAIN_URL').C('PATH_UPLOAD').'Goods/'.date('Ym',$goods_info['crdate']).'/'.$goods_info['image'];
                }
            }
            $this->res_data['data'] = $data?:array();
        }catch(\Exception $e){
            debug($e->getmessage());
            $this->res_data['code_key'] = 'fail';
        }
        $this->apiReply();
    }	
}