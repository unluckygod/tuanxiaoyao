<?php
namespace Api\Controller;

class CollectController extends APIController {
    public $tbName = CONTROLLER_NAME;

    /*待验证的参数*/
    public $tca = [
        'user_id' => [
            'method' => 'exists',
            'model' => 'member',
            'code_key' => 'lawless'
        ],
        'activity_id' => [
            'method' => 'exists',
            'model' => 'activity',
            'code_key' => 'lawless'
        ],
    ];

    /*保存参数*/
    public $field = ['user_id','activity_id']; 
    /**
     * 列表页面
     */
    public function index() {
        try{
            $this->safeChecked();
            $this->tca = [
                'user_id' => [
                    'method' => 'exists',
                    'model' => 'member',
                    'code_key' => 'lawless'
                ]
            ];
            /*验证*/
            $check = D('Validate','Util');
            $result = $check->letParam($this->tca)->response();
            if(isset($result['return_msg'])){
                $this->res_data['code_key'] = $result['return_msg'];
                $this->apiReply();
            }
            $Obj = D($this->tbName);
            $user_id = I('param.user_id');
            $page = I('param.page',C('PAGE'));
            $show_num = I('param.show_num',C('SHOW_NUM'));
            /*获取收藏商品*/
            $result = $Obj
                    ->alias('a')
                    ->join(C('DB_PREFIX').'activity as b ON a.activity_id = b.id')
                    ->join(C('DB_PREFIX').'goods as c ON b.goods_id = c.id')
                    ->where(array('a.user_id' => $user_id,'b.deleted' => 0,'b.hidden' => 0))
                    ->limit(($page -1)*$show_num,$show_num)
                    ->order('a.id desc')
                    ->Field('c.id,c.name,c.image,b.crdate,b.end,b.id as activity_id,b.num')
                    ->select();
            if($result){
                //$GoodsInfo = M('goods_info');
                $Plan = M('Plan');
                $Collect = M('collect');
                foreach ($result as $key => &$value) {
                    $value['image'] = C('DOMAIN_URL').C('PATH_UPLOAD').'Goods/thumb_0/'.date('Ym',$value['crdate']).'/'.$value['image'];
                    /*$info = $GoodsInfo->where(array('goods_id' => $value['id'],'deleted' => 0))->Field('distribution_price')->order('distribution_price asc')->select();
                    $value['price'] = $info[0]['distribution_price'];*/
                    $info = $Plan->where(array('activity_id' => $value['activity_id'],'deleted' => 0))->Field('group_price,leader_price')->order('group_price asc')->limit(0,1)->select();
                    $value['price'] = $info[0]['leader_price'] > 0 ?$info[0]['leader_price']:$info[0]['group_price'];
                    //判断活动是否过期 1为有效 2为过期
                    if($value['end'] > time()){
                        $value['status'] = 1;
                    }else{
                        $value['status'] = 2;
                    }
                    //统计活动收藏人数
                    $value['count'] = $Collect->where(array('activity_id' => $value['activity_id']))->count()?:0;
                }
            }
            $this->res_data['data'] = $result?:array();
        }catch(\Exception $e){
            debug($e->getmessage());
            $this->res_data['code_key'] = 'fail';
        }
        $this->apiReply();
    }

    /**
     *  收藏或取消
     */
    public function collectOrCancel(){
        try{
            $this->safeChecked();
            /*验证*/
            $check = D('Validate','Util');
            $result = $check->letParam($this->tca)->response();
            if(isset($result['return_msg'])){
                $this->res_data['code_key'] = $result['return_msg'];
                $this->apiReply();
            }
            $data = I('param.');
            /*过滤*/
            $Obj = D($this->tbName);
            $info = $Obj->where(array('user_id' => $data['user_id'],'activity_id' => $data['activity_id']))->find();
            if($info){
                $Obj->delete($info['id']);
            }else{
                //数据保存
                $this->filterCoupon($data);
                $Obj->add($data);
                debug($Obj->_sql());
            }
        }catch(\Exception $e){
            debug($e->getmessage());
            $this->res_data['code_key'] = 'fail';
        }
        $this->apiReply();
    }

    /**
     *  判断是否收藏
     */
    public function isCollect(){
        try{
            $this->safeChecked();
            /*验证*/
            $check = D('Validate','Util');
            $result = $check->letParam($this->tca)->response();
            if(isset($result['return_msg'])){
                $this->res_data['code_key'] = $result['return_msg'];
                $this->apiReply();
            }
            $data = I('param.');
            /*过滤*/
            $Obj = D($this->tbName);
            $info = $Obj->where(array('user_id' => $data['user_id'],'activity_id' => $data['activity_id']))->find();
            $this->res_data['data'] = ['flag' => $info?true:false];
        }catch(\Exception $e){
            debug($e->getmessage());
            $this->res_data['code_key'] = 'fail';
        }
        $this->apiReply();
    }

    /*数据筛选*/
    public function filterCoupon(&$data){
        foreach ($data as $key => $value) {
            if(!in_array($key,$this->field)){
                unset($data[$key]);
            }
        }
    }
}

?>