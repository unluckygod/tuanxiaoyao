<?php
namespace Api\Controller;

class ComputerController extends APIController {
    public $tbName = CONTROLLER_NAME; 
    
    /**
     * 列表页面
     */
    public function index() {
        try{
            static $data = [];
            $this->safeChecked();
            $Obj = D($this->tbName);
            //获取所有维修类目
            $data = $Obj->where(array('deleted' => 0))->select();
            $this->res_data['data'] = $data;
        }catch(\Exception $e){
            debug($e->getmessage());
            $this->res_data['code_key'] = 'fail';
        }
        $this->apiReply();
    }

}

?>