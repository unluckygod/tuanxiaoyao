<?php
namespace HQ\Util;
/**
 * 通用的树型类，可以生成任何树型结构
 */
class TestUtil
{
	private $user = '15337163911';
    private $pwd = '123456';
    private $mids = "10952";

    private $url = 'http://apis.renxinl.com:8080/smsgate/varsend.do';
	/*
	* 创建一个curl_init提交函数，模拟get提交，模拟post提交
	* $url 提交的服务器地址，必须
	* $data 提交的数据，必须，是关联数组，
	* $method 提交的方式，必须，默认get提交，选项值，只能填get、post,post提交可以上传文件，但是文件的地址必须这样设置,下标为‘file’，地址前加@
　　　　 array('file' => '@+文件完整的地址')
	* $dataType 提交的数据类型，默认string,选项值,目前支持(string,array,json,xml),get方式默认string,post方式无提交类型默认array
	* $options 其他选项，可有可以无，如果设置了，不能设置CURLOPT_URL,必须符合curl_setopt_array()的参数语法
	* 如果失败则返回false，提交数据成功，则返回服务器返回的结果
	*/
	public function curl($url='',$data=array(),$method='get',$dataType='string',$options=array()){
		$url = $this->url;
		$info = array(
            'user' => $this->user,
            'pwd' => md5($this->pwd),
            'mid' => $this->mids,
        );
        $data = array_merge($info,$data);
	    //初始化curl句柄，
	    $ch = curl_init();
	    //设置服务器返回的数据不直接输出，而是保留在curl_exec()的返回值中
	    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	    if(!empty($options) && is_array($options)){
	        if(curl_setopt_array($ch,$options) == false){
	            return false;
	        }
	    }
	    if($method == 'get'){
	    	$param = '';
	    	foreach($data as $key => $val){
	    		$param .=  $key.'='.$val.'&';
	    	}
	    	$param = substr($param,0,-1);
            curl_setopt($ch,CURLOPT_URL,$url.'?'.$param);
	    }else if($method == 'post'){
	    	curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_POST,true);
            switch($dataType){
            	case 'json':
            		$data = json_encode($data);
            	break;
            	case 'xml':
	            	$xml = '';
	            	foreach($data as $key => $val){
	            		$xml .= '<'.$key.'>'.$val.'<'.$key.'>';
	            	}
	            	$data = $xml;
            	default :
            	break;
            }
            curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
	    }
	    $return = curl_exec($ch);
        curl_close($ch);
        return $return;

	}
}

?>