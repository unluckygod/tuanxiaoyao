<?php
namespace Pig\Controller;

class SeckilltimeController extends BaseController {

    public $tbName = 'seckill_time';  

    /*
     * 钟点房时间段
     */

    public function index() {
        try{
            $Obj = M($this->tbName); //实例化对象
            if(IS_POST){
                foreach ($_POST['time'] as $k => $v) {
                    if(!$v){
                        $this->error('请选择时间段');
                    }
                }
                $Obj->startTrans();
                foreach ($_POST['time'] as $key => $value) {
                    if(isset($_POST['time_id'][$key])){
                        $hour_info = array(
                            'time' => $value
                        );
                        $Obj->where(array('id' => $_POST['time_id'][$key]))->save($hour_info);
                    }else{
                        $hour_info = array(
                            'time' => $value
                        );
                        $Obj->add($hour_info);
                    }
                }
                if($_POST['delIds']){
                    $hour_ids = explode(',', $_POST['delIds']);
                    $Obj->where(array('id' => array('IN',$hour_ids)))->delete();
                }
                $Obj->commit();
                $this->success('保存成功',U(CONTROLLER_NAME.'/index'));
            }
            $Where = "";
            //控制列表排序
            $sorting = I('get.sorting') ? I('get.sorting') : 'id';
            $order = I('get.order') ? I('get.order') : 'asc';
    		$list = $Obj->where($Where)
                        ->order($sorting.' '.$order)
                        ->select();
            $this->assign('list', $list); // 赋值数据集
            $this->display(); // 输出模板
        }catch(\Exception $e){
            $Obj->rollback();
            debug($e->getmessage());
            $message = IS_AJAX?'保存失败':'';
            $this->error($message);
        }
    }
}
