<?php
namespace Pig\Controller;
use \Pig\Util\Page;
class MemberController extends BaseController {
    public $tca = array();
    public $tbName = CONTROLLER_NAME;

    /**
     * 高级搜索
     + 数据说明
     + label    页面显示数据
     + name     数据库字段名
     + type     搜索类型(目前只支持input及select的搜索)
     + data     select框所有数据(在init函数中赋值,input类型,此项为空)
     + value    接收页面传值并在搜索之后在页面展示
     + category 针对input的类型(normal代表普通的input,time代表时间搜索的input),select类型无此数据
     */
    public $search = array(
    );

    /**
     * 数据操作(权限管理)
     + 数据说明(目前只支持添加,数据导入,数据导出----注意方法名需保持跟权限表的添加的数据一致)
     + label     页面显示操作权限
     + access    操作方法
     + class     页面显示样式
     */
    public $action = array(
        
    );

    /**
     * 管理操作(权限管理)
     + 数据说明(注意方法名需保持跟权限表的添加的数据一致)
     + label     页面显示操作权限
     + access    操作方法
     */
    public $edit_action = array(
    );

    /**
     * 操作管理(数据删除,发布,草稿,置顶,不顶,热门,不热等操作)
     + 数据说明(确保数据表中有相关参数,若需要添加特殊操作,应先在BaseController下添加相关操作方法)
     + label     页面显示操作内容
     + access    操作方法
     */
    public $handle = array(
    );

    /**
     * 列表页表头展示内容
     + 数据说明(确保数据表中有相关参数,若需要添加特殊操作,应先在BaseController下添加相关操作方法)
     + label     页面显示操作内容
     + field     字段名
     + status    编辑状态(true为列表页可编辑,false为不能编辑)
     */
    public $index_content = array(
        array('label' => 'ID编号', 'field' => 'id', 'status' => false),
        array('label' => '头像', 'field' => 'wxlogo', 'status' => false),
        array('label' => '昵称', 'field' => 'nickname', 'status' => false),
        array('label' => '地区', 'field' => 'area', 'status' => false),
        array('label' => '性别', 'field' => 'gender', 'status' => false),
        array('label' => '手机号','field' => 'mobile','status' => false),
        array('label' => '超级团长','field' => 'colonel','status' => false),
        array('label' => '注册时间','field' => 'crdate','status' => false)
    );

    protected function init() {
        //高级筛选
        $this->assign('status_all', $this->status_all);

        //数据操作(权限管理)
        $this->assign('action', $this->action);

        //管理操作
        $this->assign('edit_action', $this->edit_action);

        //操作管理
        $this->assign('handle', $this->handle);

        //列表页表头展示内容
        $this->assign('index_content', $this->index_content);
    }

    /**
     * 列表页面
     */
    public function index() {
        $this->init(); //加载初始化数据
        $Obj = D($this->tbName); //实例化对象

        $Where = "deleted=0";
        
        if(I('post.limit_num') != ''){
            session('page_limit_num', I('post.limit_num'));
        }
        $sorting = I('get.sorting') ? I('get.sorting') : 'id';
        $order = I('get.order') ? I('get.order') : 'desc';
        $limit_num = $_SESSION['page_limit_num'] ? $_SESSION['page_limit_num'] : 10;
        
        $Page = new Page($Obj->where($Where)->count(), $limit_num); // 实例化分页类 传入总记录数和每页显示的记录数
        $list = $Obj->where($Where)->order($sorting . ' ' . $order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        if($list){
            foreach ($list as $key => &$value) {
                $value['crdate'] = date('Y-m-d H:i',$value['crdate']);
                $value['colonel'] = $value['is_colonel']?'是':'否';
            }
        }
        $this->assign('list', $list); // 赋值数据集
        $this->assign('page', $Page->show()); // 分页显示输出 
        $this->display('Member/index'); // 输出模板
    }

    /**
     * 设置超级团长
     * @param null $id 需要设置的记录id
     */
    public function setColonel() {
        $id = I('request.id');
        $rs = D($this->tbName)->where(array('id' => $id))->save(array('is_colonel' => 1,'tstamp' => time()));
        if ($rs) {
            $this->success('设置成功!', U(CONTROLLER_NAME . '/index', 'kept=1'));
        } else {
            $this->error('设置失败，请稍后再试！');
        }
    }

    // Excel商品数据导出
    public function export(){
        try{
            import("Excel.PHPExcel");
            $Obj = D($this->tbName.' as a');
            $list = $Obj
                    ->order('a.crdate DESC')
                    ->select();
            //数据导出
            $Excel = new \PHPExcel();
            // 设置基本属性
            $Excel_pro = $Excel->getProperties();
            $Excel_pro->setCreator('admin') //设置创建者
                      ->setLastModifiedBy(date('Y-m-d H:i:s', time())) //设置时间
                      ->setTitle('用户数据') //设置标题
                      ->setSubject('导出') //备注
                      ->setDescription('用户数据导出'); //描述
                      //->setKeywords('商品') //关键字
                      //->setCategory('商品');//类别

            // sheet
            $Excel->setActiveSheetIndex(0);
            // sheet title
            $Excel->getActiveSheet(0)->setTitle('用户数据');

            $Excel->getActiveSheet()->setCellValue('A1', '用户id');
            $Excel->getActiveSheet()->setCellValue('B1', '昵称');
            $Excel->getActiveSheet()->setCellValue('C1', '地区');
            $Excel->getActiveSheet()->setCellValue('D1', '性别');
            $Excel->getActiveSheet()->setCellValue('E1', '手机号');
            $Excel->getActiveSheet()->setCellValue('F1', '注册时间');

            foreach($list as $k => $v){
                $k = $k + 2;
                $Excel->getActiveSheet()->setCellValue('A' . $k, $v['id']);
                $Excel->getActiveSheet()->setCellValue('B' . $k, $v['nickname']);
                $Excel->getActiveSheet()->setCellValue('C' . $k, $v['area']);
                $Excel->getActiveSheet()->setCellValue('D' . $k, $v['gender']);
                $Excel->getActiveSheet()->setCellValue('E' . $k, ' '.$v['mobile']);
                $Excel->getActiveSheet()->setCellValue('F' . $k, date('Y-m-d H:i',$v['crdate']));
            }
            //写入

            $Excel_write = \PHPExcel_IOFactory::createWriter($Excel, 'Excel5');

            // 输出
            ob_end_clean();
            header('Pragma: public');
            header('Expires: 0');
            header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
            header('Content-Type:application/force-download');
            header('Content-Type:application/vnd.ms-execl;');
            header('Content-Type:application/octet-stream');
            header('Content-Type:application/download');
            header("Content-Disposition:attachment;filename=用户数据-".date('Y-m-d').".xls");
            header('Content-Transfer-Encoding:binary');
            $Excel_write->save('php://output'); 
        }catch(\Exception $e){
            debug($e->getmessage());
            $this->error('导出失败');
        }
    }
}

?>