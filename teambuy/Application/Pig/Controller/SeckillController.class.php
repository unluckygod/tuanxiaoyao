<?php

namespace Pig\Controller;

class SeckillController extends BaseController {

    private $tca = array();
    public $tbName = CONTROLLER_NAME; 
    public $image_path = '';
    protected function init() {
        $info = '';
        if(I('request.id')){
            $info = M($this->tbName)->find(I('request.id'));
        }
        //神秘团
        $html = '';
        $html_activity = '';
        $Goods = M('goods');
        $activities = M('activity')->where(array('deleted' => 0,'hidden' => 0,'type' => 2,'end' => array('gt',time())))->Field('id as "0",goods_id as "1"')->select();
        foreach ($activities as $key => $value) {
            $select = $info?($info['activity_id'] == $value[0]?'selected="selected"':''):'';
            $html .= '<option value="'.$value[0].'"'.$select.'>'.$value[0].'.'.$Goods->where(array('id' => $value[1]))->getField('name').'</option>';
            $select = $value[0] == I('request.activity_id')?'selected="selected"':'';
            $html_activity .= '<option value="'.$value[0].'"'.$select.'>'.$value[0].'.'.$Goods->where(array('id' => $value[1]))->getField('name').'</option>';
        }
        $this->assign('html_activity',$html_activity);
        //时间段
        $html_times = '';
        $time_tree = '';
        $times = M('seckill_time')->Field('id as "0",time as "1"')->select();
        $this->assign('times',$times);
        foreach ($times as $key => $value) {
            $select = $info?($info['time_id'] == $value[0]?'selected="selected"':''):'';
            $html_times .= '<option value="'.$value[0].'"'.$select.'>'.$value[1].'</option>';
        }
        //配置字段
        $this->tca = array(
            'time_id' => array(
                'label' => '时间段',
                'type' => 'select_tree',
                'data' => $html_times,
                'reuqired' => true
            ),
            'activity_id' => array(
                'label' => '关联神秘团',
                'type' => 'select_tree',
                'data' => $html,
                'required' => true
            )
        );
    }

    /*
     * 列表页面
     */

    public function index() {
        $this->init();
        $Obj = D($this->tbName); //实例化对象
        $Where = "deleted=0";
         /*时间段查询*/   
        if (I('request.time_id')!='') {
            $I_keyword = trim(I('request.time_id'));
            $Where .= " and time_id = {$I_keyword}";
        }
        /*商品查询*/   
        if (I('request.activity_id')!='') {
            $I_keyword = trim(I('request.activity_id'));
            $Where .= " and activity_id = {$I_keyword}";
        }
        //控制分页显示条数
        if(I('post.limit_num')!=''){
            session('page_limit_num', I('post.limit_num'));
        }
        //控制列表排序
        $sorting = I('get.sorting') ? I('get.sorting') : 'id';
        $order = I('get.order') ? I('get.order') : 'desc';
        
        $limit_num = $_SESSION['page_limit_num'] ? $_SESSION['page_limit_num'] : 10;//客户列表默认10条
        
        $Page = new \Pig\Util\Page($list_sum = $Obj->where($Where)->count(), $offset = $limit_num); // 实例化分页类 传入总记录数和每页显示的记录数
        $list = $Obj
                ->where($Where)
                ->order($sorting.' '.$order)
                ->limit($Page->firstRow . ',' . $Page->listRows)
                ->select();
        $this->assign('list', $list); // 赋值数据集
        $this->assign('page', $Page->show()); // 分页显示输出
        $this->display(); // 输出模板
    }

    /*
     * 编辑数据
     */

    public function edit() {
        $this->save();
    }

    /*
     * 保存数据
     */

    public function save() {
        try{
            $this->init();
            //编辑数据
            $id = I('request.id') ? I('request.id') : null;
            $info = M($this->tbName)->where('id = ' . $id)->find();
            if (IS_POST) {
                /*页面原始必填项验证-start*/
                foreach ($_POST as $key => $val) {
                    if(@$this->tca[$key]['required'] && !preg_match('/\S/', $val)){
                        $this->error($this->tca[$key]['label'] . '*是必填项!');
                    }
                }
                //文件上传 end **********************************
                $Obj = D($this->tbName);
                $Obj->startTrans();
                $result = $Obj->create();
                if (!$result) {
                    $this->error($Obj->getError());
                } else { 
                    $Obj->tstamp = time();
                    if(!$id){
                        $Obj->crdate = time();
                        $id = $Obj->add();
                    }else{
                        $id = $Obj->where(array('id' => $id))->save();
                    }
                    if(!$id){
                        debug($Obj->_sql());
                        $this->error('保存失败');
                        exit;
                    }
                    $message = '添加成功!';   
                }
                $Obj->commit();
                $this->success($message,U(CONTROLLER_NAME.'/index'));
            }
            parent::autoFields($this->tca, $info);
            $this->display('info');
        }catch(\Exception $e){
            debug($e->getmessage());
            $this->error('保存失败');
        }
    }
}

?>