<?php
namespace Pig\Controller;

class CashController extends BaseController {
    public $tca = array();
    public $tbName = CONTROLLER_NAME;

    /**
     * 高级搜索
     + 数据说明
     + label    页面显示数据
     + name     数据库字段名
     + type     搜索类型(目前只支持input及select的搜索)
     + data     select框所有数据(在init函数中赋值,input类型,此项为空)
     + value    接收页面传值并在搜索之后在页面展示
     + category 针对input的类型(normal代表普通的input,time代表时间搜索的input),select类型无此数据
     */
    public $search = array(
    );

    /**
     * 数据操作(权限管理)
     + 数据说明(目前只支持添加,数据导入,数据导出----注意方法名需保持跟权限表的添加的数据一致)
     + label     页面显示操作权限
     + access    操作方法
     + class     页面显示样式
     */
    public $action = array(
        // array('label' => '添加', 'access' => 'save', 'class' => 'icon-plus')
    );

    /**
     * 管理操作(权限管理)
     + 数据说明(注意方法名需保持跟权限表的添加的数据一致)
     + label     页面显示操作权限
     + access    操作方法
     */
    public $edit_action = array(
        // array('label' => '编辑', 'access' => 'edit', 'class' => 'btn green-stripe mini','type' => 'link'),
        // array('label' => '删除', 'access' => 'del', 'class' => 'btn red-stripe mini del', 'type' => 'link')
    );

    /**
     * 操作管理(数据删除,发布,草稿,置顶,不顶,热门,不热等操作)
     + 数据说明(确保数据表中有相关参数,若需要添加特殊操作,应先在BaseController下添加相关操作方法)
     + label     页面显示操作内容
     + access    操作方法
     */
    public $handle = array(
    );

    /**
     * 列表页表头展示内容
     + 数据说明(确保数据表中有相关参数,若需要添加特殊操作,应先在BaseController下添加相关操作方法)
     + label     页面显示操作内容
     + field     字段名
     + status    编辑状态(true为列表页可编辑,false为不能编辑)
     */
    public $index_content = array(
        array('label' => 'ID编号', 'field' => 'id', 'status' => false),
        array('label' => '头像', 'field' => 'image', 'status' => false),
        array('label' => '昵称', 'field' => 'nickname', 'status' => false),
        array('label' => '提现金额', 'field' => 'money', 'status' => false),
        array('label' => '提现时间', 'field' => 'crdate', 'status' => false),
        array('label' => '提现状态', 'field' => 'state', 'status' => false),
        array('label' => '注册时间', 'field' => 'register', 'status' => false)
    );

    protected function init() {
        //高级筛选
        $this->assign('status_all', $this->status_all);

        //数据操作(权限管理)
        $this->assign('action', $this->action);

        //管理操作
        $this->assign('edit_action', $this->edit_action);

        //操作管理
        $this->assign('handle', $this->handle);

        //列表页表头展示内容
        $this->assign('index_content', $this->index_content);
    }

    /**
     * 列表页面
     */
    public function index() {
        $this->init(); //加载初始化数据
        $Obj = D($this->tbName); //实例化对象
        $Where = [];
        if(I('request.start') && I('request.end')){
            $start = strtotime(I('request.start').' 00:00:00');
            $end = strtotime(I('request.end'),' 23:59:59');
            $Where = ['a.crdate' => ['BETWEEN',[$start,$end]]];
        }
        if(I('request.start') && !I('request.end')){
            $start = strtotime(I('request.start').' 00:00:00');
            $Where = ['a.crdate' => ['egt',$start]];
        }
        if(!I('request.start') && I('request.end')){
            $end = strtotime(I('request.end').' 00:00:00');
            $Where = ['a.crdate' => ['elt',$end]];
        }
        $sorting = I('get.sorting') ? I('get.sorting') : 'a.id';
        $order = I('get.order') ? I('get.order') : 'desc';
        $limit_num = $_SESSION['page_limit_num'] ? $_SESSION['page_limit_num'] : 10;
        
        $Page = new \Pig\Util\Page($Obj->where($Where)->count(), $limit_num); // 实例化分页类 传入总记录数和每页显示的记录数
        $list = $Obj 
                ->alias('a')
                ->join(C('DB_PREFIX').'member as b ON a.user_id = b.id')   
                ->where($Where)->order($sorting . ' ' . $order)
                ->Field('a.*,b.wxlogo as image,b.nickname,b.mobile,b.area,b.gender,b.crdate as register')
                ->limit($Page->firstRow . ',' . $Page->listRows)
                ->select();
        if($list){
            foreach ($list as $key => &$value) {
                switch ($value['status']) {
                    case 1:
                        $value['state'] = '审核中';
                        break;
                    case 2:
                        $value['state'] = '提现成功';
                        break;
                    default:
                        $value['state'] = '提现失败';
                        break;
                }
                $value['crdate'] = date('Y-m-d H:i',$value['crdate']);
                $value['register'] = date('Y-m-d H:i',$value['register']);
            }
        }
        $this->assign('list', $list); // 赋值数据集
        $this->assign('page', $Page->show()); // 分页显示输出 
        $this->display(); // 输出模板
    }

    // /**
    //  * 同意提现
    //  */
    // public function agree(){
    //     try{
    //         if(IS_AJAX){
    //             $id = I('param.id');
    //             $Obj = D($this->tbName);
    //             $Obj->startTrans();
    //             $info = $Obj->find($id);
    //             $num = $Obj->where(array('id' => $id))->save(array('tstamp' => time(),'status' => 3));
    //             //新增资产提现记录
    //             $Asset = M('asset');
    //             $param = [
    //                 'user_id' => $info['user_id'],
    //                 'money' => $info['money'],
    //                 'type' => 2,
    //                 'remark' => '提现成功,扣除'.$info['money'],
    //                 'crdate' => time()
    //             ];
    //             $flag = $Asset->add($param);
    //             if($num && $flag){
    //                 $Obj->commit();
    //                 $this->success('操作成功',U(CONTROLLER_NAME.'/index'));
    //             }else{
    //                 $Obj->rollback();
    //                 debug([$Obj->_sql(),$Asset->_sql()]);
    //                 $this->error('操作失败');
    //             }
    //         }else{
    //            $this->error('非法操作'); 
    //         }
    //     }catch(\Exception $e){
    //         debug($e->getmessage());
    //         $this->error('操作失败');
    //     }
    // }

    // /**
    //  * 拒绝提现
    //  */
    // public function refuse(){
    //     try{
    //         if(IS_AJAX){
    //             $id = I('param.id');
    //             $Obj = D($this->tbName);
    //             $Obj->startTrans();
    //             $num = $Obj->where(array('id' => $id))->save(array('tstamp' => time(),'status' => 2));
    //             if($num){
    //                 $Obj->commit();
    //                 $this->success('操作成功',U(CONTROLLER_NAME.'/index'));
    //             }else{
    //                 $Obj->rollback();
    //                 debug($Obj->_sql());
    //                 $this->error('操作失败');
    //             }
    //         }else{
    //            $this->error('非法操作'); 
    //         }
    //     }catch(\Exception $e){
    //         debug($e->getmessage());
    //         $this->error('操作失败');
    //     }
    // }
}

?>