<?php
namespace Pig\Controller;

class BannerController extends BaseController {
    public $tca = array();
    public $tbName = CONTROLLER_NAME;
    public $image_path = '';
    public $thumb_height = '';
    public $thumb_width = '';

    /**
     * 高级搜索
     + 数据说明
     + label    页面显示数据
     + name     数据库字段名
     + type     搜索类型(目前只支持input及select的搜索)
     + data     select框所有数据(在init函数中赋值,input类型,此项为空)
     + value    接收页面传值并在搜索之后在页面展示
     + category 针对input的类型(normal代表普通的input,time代表时间搜索的input),select类型无此数据
     */
    public $search = array(
    );

    /**
     * 数据操作(权限管理)
     + 数据说明(目前只支持添加,数据导入,数据导出----注意方法名需保持跟权限表的添加的数据一致)
     + label     页面显示操作权限
     + access    操作方法
     + class     页面显示样式
     */
    public $action = array(
        array('label' => '添加', 'access' => 'save', 'class' => 'icon-plus')
    );

    /**
     * 管理操作(权限管理)
     + 数据说明(注意方法名需保持跟权限表的添加的数据一致)
     + label     页面显示操作权限
     + access    操作方法
     */
    public $edit_action = array(
        array('label' => '编辑', 'access' => 'edit', 'class' => 'btn green-stripe mini','type' => 'link'),
        array('label' => '删除', 'access' => 'del', 'class' => 'btn red-stripe mini del', 'type' => 'link'),
    );

    /**
     * 操作管理(数据删除,发布,草稿,置顶,不顶,热门,不热等操作)
     + 数据说明(确保数据表中有相关参数,若需要添加特殊操作,应先在BaseController下添加相关操作方法)
     + label     页面显示操作内容
     + access    操作方法
     */
    public $handle = array(
    );

    /**
     * 列表页表头展示内容
     + 数据说明(确保数据表中有相关参数,若需要添加特殊操作,应先在BaseController下添加相关操作方法)
     + label     页面显示操作内容
     + field     字段名
     + status    编辑状态(true为列表页可编辑,false为不能编辑)
     */
    public $index_content = array(
        array('label' => 'ID编号', 'field' => 'id', 'status' => false),
        array('label' => '图片', 'field' => 'image', 'status' => false),
    );

    protected function init() {
        //高级筛选
        $this->assign('status_all', $this->status_all);

        //数据操作(权限管理)
        $this->assign('action', $this->action);

        //管理操作
        $this->assign('edit_action', $this->edit_action);

        //操作管理
        $this->assign('handle', $this->handle);

        //列表页表头展示内容
        $this->assign('index_content', $this->index_content);
        
        if (ACTION_NAME == 'edit') {
            $id = I('param.id');
            $info = M($this->tbName)->where('id = ' . $id)->find();
            //图片路径
            $this->image_path = CONTROLLER_NAME . '/' . date('Ym', $info['crdate']) . '/';
        } 
        //配置字段
        $this->tca = array(
            'image' => array(
                'label' => '图片',
                'type' => 'image',
                'name' => 'image',
                'required' => true,
                'help' => '建议上传尺寸首页广告750*280,格式为png,jpg,gif，大小不超过300k'
            ),
        );
    }
}

?>