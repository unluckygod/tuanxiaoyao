<?php
/*
 * 此文件用于验证短信服务API接口，供开发时参考
 * 执行验证前请确保文件为utf-8编码，并替换相应参数为您自己的信息，并取消相关调用的注释
 * 建议验证前先执行Test.php验证PHP环境
 *
 * 2017/11/30
 */

namespace Aliyun\DySDKLite\Sms;

require_once dirname(__DIR__) . "/SignatureHelper.php";

use Aliyun\DySDKLite\SignatureHelper;

class sendSms{

    private $accessKeyId ='';
    private $accessKeySecret ='';
    private $params = [];

    public function __construct($accessKeyId,$accessKeySecret,$params){
        $this->accessKeyId = $accessKeyId;
        $this->accessKeySecret = $accessKeySecret;
        $this->params = $params;
    }

    public function send(){
        $security = false;
        // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
        $helper = new SignatureHelper();
        // *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
        if(!empty($this->params["TemplateParam"]) && is_array($this->params["TemplateParam"])) {
            $this->params["TemplateParam"] = json_encode($this->params["TemplateParam"], JSON_UNESCAPED_UNICODE);
        }
        // 此处可能会抛出异常，注意catch
        $content = $helper->request(
            $this->accessKeyId,
            $this->accessKeySecret,
            "dysmsapi.aliyuncs.com",
            array_merge($this->params, array(
                "RegionId" => "cn-hangzhou",
                "Action" => "SendSms",
                "Version" => "2017-05-25",
            )),
            $security
        );

        return $content;
    }
}