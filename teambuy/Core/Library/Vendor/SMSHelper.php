<?php
vendor('CCPRestSDK');

class SMSHelper {
    const TPL_WEAL = 110436;
    const TPL_AUDIT = 97606;
    const TPL_FEEDBACK = 97088;
    const TPL_FEEDBACK_STAFF = 99085;
    const TPL_PROPERTY_FEEDBACK = 97240;
    const TPL_PROPERTY_FEEDBACK_STAFF = 97241;


    protected static $_instance = NULL;
    protected $_client = NULL;

    protected function __construct() {
        $serverIP = 'app.cloopen.com';
        $serverPort='8883';
        $softVersion='2013-12-26';
        $accountSid = 'aaf98f894b353559014b5c89ea34150a';
        $accountToken = 'bd0c1c31b2754593a54300c4d622bb42';
        $appId = 'aaf98f894b353559014b5c9cf47e1528';


        $this->_client = new REST($serverIP,$serverPort,$softVersion);
        $this->_client->setAccount($accountSid,$accountToken);
        $this->_client->setAppId($appId);
    }

    public static function getInstance() {
        if(is_null(self::$_instance)) {
            self::$_instance = new SMSHelper();
        }

        return self::$_instance;
    }

    public function sendSMS($tempId, $to, $datas = []) {
        $ret = [
            'success' => false,
            'message' => '',
        ];

        $result = $this->_client->sendTemplateSMS($to,$datas,$tempId);

        if($result == NULL ) {
            $ret['message'] = 'Fatal Error!';
        } else if($result->statusCode!=0) {
            $ret['message'] = $result->statusMsg . '(' .$result->statusCode. ')';
        } else {
            $ret['success'] = true;
        }

        return $ret;
    }
}