/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : teambuy

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2019-01-30 14:51:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pig_access
-- ----------------------------
DROP TABLE IF EXISTS `pig_access`;
CREATE TABLE `pig_access` (
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户组',
  `menu_id` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  KEY `group_id` (`group_id`) USING BTREE,
  KEY `menu_id` (`menu_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='权限表\r\n';

-- ----------------------------
-- Records of pig_access
-- ----------------------------
INSERT INTO `pig_access` VALUES ('1', '6');
INSERT INTO `pig_access` VALUES ('1', '5');
INSERT INTO `pig_access` VALUES ('1', '4');
INSERT INTO `pig_access` VALUES ('1', '7');
INSERT INTO `pig_access` VALUES ('1', '8');
INSERT INTO `pig_access` VALUES ('1', '9');
INSERT INTO `pig_access` VALUES ('1', '10');
INSERT INTO `pig_access` VALUES ('1', '11');
INSERT INTO `pig_access` VALUES ('1', '12');
INSERT INTO `pig_access` VALUES ('1', '13');
INSERT INTO `pig_access` VALUES ('1', '14');
INSERT INTO `pig_access` VALUES ('1', '15');
INSERT INTO `pig_access` VALUES ('1', '16');
INSERT INTO `pig_access` VALUES ('1', '17');
INSERT INTO `pig_access` VALUES ('1', '18');
INSERT INTO `pig_access` VALUES ('1', '19');
INSERT INTO `pig_access` VALUES ('1', '20');
INSERT INTO `pig_access` VALUES ('1', '21');
INSERT INTO `pig_access` VALUES ('1', '22');
INSERT INTO `pig_access` VALUES ('1', '23');
INSERT INTO `pig_access` VALUES ('1', '24');
INSERT INTO `pig_access` VALUES ('1', '26');
INSERT INTO `pig_access` VALUES ('1', '25');
INSERT INTO `pig_access` VALUES ('1', '27');
INSERT INTO `pig_access` VALUES ('1', '28');
INSERT INTO `pig_access` VALUES ('1', '32');
INSERT INTO `pig_access` VALUES ('1', '29');
INSERT INTO `pig_access` VALUES ('1', '30');
INSERT INTO `pig_access` VALUES ('1', '31');
INSERT INTO `pig_access` VALUES ('2', '29');
INSERT INTO `pig_access` VALUES ('2', '30');
INSERT INTO `pig_access` VALUES ('2', '31');

-- ----------------------------
-- Table structure for pig_account
-- ----------------------------
DROP TABLE IF EXISTS `pig_account`;
CREATE TABLE `pig_account` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(225) NOT NULL COMMENT '微信用户openid',
  PRIMARY KEY (`id`),
  KEY `openid` (`openid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信openid记录表';

-- ----------------------------
-- Records of pig_account
-- ----------------------------

-- ----------------------------
-- Table structure for pig_activity
-- ----------------------------
DROP TABLE IF EXISTS `pig_activity`;
CREATE TABLE `pig_activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '活动(团)类型 1普通团 2神秘团',
  `hidden` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否关闭 0否1是',
  `start` int(11) NOT NULL DEFAULT '0' COMMENT '活动开始时间',
  `end` int(11) NOT NULL DEFAULT '0' COMMENT '活动截止时间',
  `goods_id` int(11) NOT NULL COMMENT '关联商品',
  `num` int(11) NOT NULL DEFAULT '2' COMMENT '团购人数',
  `limit` int(11) NOT NULL DEFAULT '0' COMMENT '限购数量 0表示不限购',
  `cut_money` decimal(20,0) NOT NULL DEFAULT '0' COMMENT '团长立减金(针对普通团)',
  `is_support_coupon` tinyint(1) NOT NULL DEFAULT '0' COMMENT '否是支持优惠券 0否1是',
  `coupon_id` int(11) DEFAULT '0' COMMENT '关联优惠券 0表示不发放优惠券',
  `group_coupon_id` int(11) DEFAULT '0' COMMENT '团长优惠券(针对神秘团)',
  `crdate` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `tstamp` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `auto_group` tinyint(1) NOT NULL DEFAULT '0' COMMENT '自动拼团 0否1是',
  `total` int(11) NOT NULL DEFAULT '1' COMMENT '最大开团数',
  `expired` int(11) NOT NULL DEFAULT '1' COMMENT '成团有效期(单位：小时)',
  PRIMARY KEY (`id`),
  KEY `start` (`start`) USING BTREE,
  KEY `end` (`end`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='活动表';

-- ----------------------------
-- Records of pig_activity
-- ----------------------------
INSERT INTO `pig_activity` VALUES ('2', '2', '0', '1546790400', '1548863999', '6', '2', '2', '0', '0', '1', '1', '1546849248', '1547546738', '0', '0', '15', '1');
INSERT INTO `pig_activity` VALUES ('3', '1', '0', '1546790400', '1548950399', '6', '3', '5', '10', '1', '1', '0', '1546850864', '1546850864', '0', '1', '0', '1');
INSERT INTO `pig_activity` VALUES ('4', '1', '0', '1546876800', '1546963199', '6', '2', '3', '15', '0', '0', '0', '1546916398', '1547546735', '0', '0', '0', '1');
INSERT INTO `pig_activity` VALUES ('6', '1', '0', '0', '0', '11', '2', '3', '5', '1', '1', '0', '1547088324', '1547088324', '0', '1', '1', '12');
INSERT INTO `pig_activity` VALUES ('7', '2', '0', '0', '0', '11', '3', '3', '0', '1', '1', '1', '1547088653', '1547549643', '0', '0', '15', '15');
INSERT INTO `pig_activity` VALUES ('8', '2', '0', '0', '0', '11', '3', '2', '0', '1', '1', '1', '1547088993', '1547549488', '0', '0', '15', '30');
INSERT INTO `pig_activity` VALUES ('9', '1', '0', '1547172000', '1547258400', '11', '3', '2', '15', '1', '1', '0', '1547204230', '1547549729', '0', '1', '1', '3');

-- ----------------------------
-- Table structure for pig_asset
-- ----------------------------
DROP TABLE IF EXISTS `pig_asset`;
CREATE TABLE `pig_asset` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `money` decimal(20,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '变化值',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '变化类型 1增2减',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '关联用户id',
  `crdate` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='资产表';

-- ----------------------------
-- Records of pig_asset
-- ----------------------------
INSERT INTO `pig_asset` VALUES ('2', '200.00', '2', '提现成功,扣除200.00', '1', '1546593171');

-- ----------------------------
-- Table structure for pig_attach
-- ----------------------------
DROP TABLE IF EXISTS `pig_attach`;
CREATE TABLE `pig_attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tstamp` int(11) DEFAULT '0' COMMENT '修改时间',
  `crdate` int(11) DEFAULT '0' COMMENT '创建时间',
  `cruser_id` int(11) DEFAULT '0' COMMENT '创建人',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '删除',
  `hidden` tinyint(1) DEFAULT '0' COMMENT '隐藏',
  `sorting` int(11) DEFAULT '0' COMMENT '排序',
  `hit` int(11) DEFAULT '0' COMMENT '访问次数',
  `hot` tinyint(1) DEFAULT '0' COMMENT '推荐',
  `top` tinyint(1) DEFAULT '0' COMMENT '置顶',
  `title` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '标题',
  `info_id` int(11) DEFAULT '0',
  `info_tab` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='附件';

-- ----------------------------
-- Records of pig_attach
-- ----------------------------
INSERT INTO `pig_attach` VALUES ('1', '1546506735', '1546506735', '0', '0', '0', '0', '0', '0', '0', 'veer-133872101', '6', 'Goods', null, '5c2dd1ee73046.jpg');
INSERT INTO `pig_attach` VALUES ('2', '1546506735', '1546506735', '0', '0', '0', '0', '0', '0', '0', 'veer-141113085', '6', 'Goods', null, '5c2dd1ee7e3f9.jpg');
INSERT INTO `pig_attach` VALUES ('3', '1546506735', '1546506735', '0', '0', '0', '0', '0', '0', '0', 'veer-152757616', '6', 'Goods', null, '5c2dd1ee8786b.jpg');
INSERT INTO `pig_attach` VALUES ('4', '1547087951', '1547087951', '0', '0', '0', '0', '0', '0', '0', 'veer-135955783', '11', 'Goods', null, '5c36af508b0be.jpg');
INSERT INTO `pig_attach` VALUES ('5', '1547087951', '1547087951', '0', '0', '0', '0', '0', '0', '0', 'veer-141113085', '11', 'Goods', null, '5c36af50b21c7.jpg');
INSERT INTO `pig_attach` VALUES ('6', '1547087951', '1547087951', '0', '0', '0', '0', '0', '0', '0', 'veer-152757616', '11', 'Goods', null, '5c36af50a0c6b.jpg');

-- ----------------------------
-- Table structure for pig_banner
-- ----------------------------
DROP TABLE IF EXISTS `pig_banner`;
CREATE TABLE `pig_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) DEFAULT '0' COMMENT '删除',
  `image` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '广告图片',
  `crdate` int(11) DEFAULT NULL,
  `tstamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='广告';

-- ----------------------------
-- Records of pig_banner
-- ----------------------------
INSERT INTO `pig_banner` VALUES ('1', '0', '5c3e9d6e0f236.jpg', '1544515624', '1547607406');

-- ----------------------------
-- Table structure for pig_be_groups
-- ----------------------------
DROP TABLE IF EXISTS `pig_be_groups`;
CREATE TABLE `pig_be_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tstamp` int(11) DEFAULT '0' COMMENT '修改时间',
  `crdate` int(11) DEFAULT '0' COMMENT '创建时间',
  `cruser_id` int(11) DEFAULT '0' COMMENT '创建人',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '删除',
  `hidden` tinyint(1) DEFAULT '0' COMMENT '隐藏',
  `title` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '组名',
  `remark` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户组';

-- ----------------------------
-- Records of pig_be_groups
-- ----------------------------
INSERT INTO `pig_be_groups` VALUES ('1', '1546747034', '1546747034', '0', '0', '0', '超级管理员', '拥有所有权限');
INSERT INTO `pig_be_groups` VALUES ('2', '1546747052', '1546747052', '0', '0', '0', '财务组', '');

-- ----------------------------
-- Table structure for pig_be_users
-- ----------------------------
DROP TABLE IF EXISTS `pig_be_users`;
CREATE TABLE `pig_be_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tstamp` int(11) DEFAULT '0' COMMENT '修改时间',
  `crdate` int(11) DEFAULT '0' COMMENT '创建时间',
  `cruser_id` int(11) DEFAULT '0' COMMENT '创建人',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '删除',
  `hidden` tinyint(1) DEFAULT '0' COMMENT '隐藏',
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '密码',
  `usergroup` int(11) DEFAULT NULL COMMENT '用户组',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名字',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `remark` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '备注',
  `lastlogin` int(11) DEFAULT '0' COMMENT '上次登录时间',
  `lastloginip` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '上次登录IP',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='后台管理员';

-- ----------------------------
-- Records of pig_be_users
-- ----------------------------
INSERT INTO `pig_be_users` VALUES ('1', '0', '0', '0', '0', '0', 'admin', 'b3d9d7ab6da5cef7e2504ab9cf37a25d', '1', '超级管理员', 'admin123@qq.com', '', '1548388490', '127.0.0.1');
INSERT INTO `pig_be_users` VALUES ('2', '1546753530', '1546753530', '0', '0', '0', 'zero', '9fbd1a4caf26eee4c4712656834ad6ec', '2', 'zero', null, null, '1546753608', '127.0.0.1');

-- ----------------------------
-- Table structure for pig_cash
-- ----------------------------
DROP TABLE IF EXISTS `pig_cash`;
CREATE TABLE `pig_cash` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '关联用户id',
  `money` decimal(20,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '提现金额',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '提现方式 1微信 2支付宝',
  `account` varchar(255) NOT NULL DEFAULT '' COMMENT '账号',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '联系人',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '审核状态 1审核中 2提现成功 3提现失败',
  `refuse` varchar(255) DEFAULT '' COMMENT '拒绝理由',
  `crdate` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `tstamp` int(11) NOT NULL DEFAULT '0' COMMENT '编辑时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='提现表';

-- ----------------------------
-- Records of pig_cash
-- ----------------------------
INSERT INTO `pig_cash` VALUES ('1', '1', '200.00', '2', '15972053215', '伊川夏', '1', '', '1546593171', '1546593215');

-- ----------------------------
-- Table structure for pig_category
-- ----------------------------
DROP TABLE IF EXISTS `pig_category`;
CREATE TABLE `pig_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='商品分类表';

-- ----------------------------
-- Records of pig_category
-- ----------------------------
INSERT INTO `pig_category` VALUES ('1', '0', '土特产', '1546424719', '1546424719');

-- ----------------------------
-- Table structure for pig_collect
-- ----------------------------
DROP TABLE IF EXISTS `pig_collect`;
CREATE TABLE `pig_collect` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL COMMENT '用户id',
  `activity_id` int(11) unsigned NOT NULL COMMENT '商品id',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `pig_collect_ibfk_2` (`activity_id`),
  CONSTRAINT `pig_collect_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `pig_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pig_collect_ibfk_2` FOREIGN KEY (`activity_id`) REFERENCES `pig_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收藏表';

-- ----------------------------
-- Records of pig_collect
-- ----------------------------

-- ----------------------------
-- Table structure for pig_comment
-- ----------------------------
DROP TABLE IF EXISTS `pig_comment`;
CREATE TABLE `pig_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0' COMMENT '订单id',
  `goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品id',
  `user_id` int(11) NOT NULL,
  `crdate` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '评价',
  `level` tinyint(1) NOT NULL DEFAULT '5' COMMENT '客房评分',
  `reply` varchar(255) NOT NULL DEFAULT '' COMMENT '回复',
  `service_level` tinyint(1) NOT NULL DEFAULT '5' COMMENT '服务评分',
  `product_level` tinyint(1) NOT NULL DEFAULT '5' COMMENT '商品评分',
  `postage_level` tinyint(1) NOT NULL DEFAULT '5' COMMENT '物流评分',
  `is_hidden_name` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否匿名 0否1是',
  `is_has_attach` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否有图 0否1是',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `pig_comment_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `pig_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='评价表';

-- ----------------------------
-- Records of pig_comment
-- ----------------------------
INSERT INTO `pig_comment` VALUES ('1', '1', '11', '3', '1547433523', '1547433523', '666', '5', '真的还不错', '5', '5', '5', '0', '0');

-- ----------------------------
-- Table structure for pig_commission
-- ----------------------------
DROP TABLE IF EXISTS `pig_commission`;
CREATE TABLE `pig_commission` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '关联用户id',
  `group_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '团购id',
  `money` decimal(20,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '佣金金额',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `crdate` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='佣金表';

-- ----------------------------
-- Records of pig_commission
-- ----------------------------

-- ----------------------------
-- Table structure for pig_config
-- ----------------------------
DROP TABLE IF EXISTS `pig_config`;
CREATE TABLE `pig_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` tinytext COLLATE utf8_unicode_ci NOT NULL COMMENT '变量名',
  `value` text COLLATE utf8_unicode_ci NOT NULL COMMENT '值',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='配置';

-- ----------------------------
-- Records of pig_config
-- ----------------------------
INSERT INTO `pig_config` VALUES ('1', 'sitename', '');
INSERT INTO `pig_config` VALUES ('2', 'copyright', '');
INSERT INTO `pig_config` VALUES ('3', 'is_del', '0');
INSERT INTO `pig_config` VALUES ('4', 'money', '30');
INSERT INTO `pig_config` VALUES ('5', 'expired', '3');
INSERT INTO `pig_config` VALUES ('6', 'order_prefix', 'xiaoyao-');
INSERT INTO `pig_config` VALUES ('7', 'appid', '');
INSERT INTO `pig_config` VALUES ('8', 'appsecret', '');
INSERT INTO `pig_config` VALUES ('9', 'mchid', '');
INSERT INTO `pig_config` VALUES ('10', 'key', '');
INSERT INTO `pig_config` VALUES ('11', 'cookie_prefix', 'xiaoyao-');
INSERT INTO `pig_config` VALUES ('12', 'email_forgot_be', '0');
INSERT INTO `pig_config` VALUES ('13', 'email_forgot_fe', '0');
INSERT INTO `pig_config` VALUES ('14', 'email_register', '0');
INSERT INTO `pig_config` VALUES ('15', 'email_message', '0');
INSERT INTO `pig_config` VALUES ('16', 'is_dispatch', '0');
INSERT INTO `pig_config` VALUES ('17', 'mod_rewrite', '0');
INSERT INTO `pig_config` VALUES ('18', 'is_notice', '0');
INSERT INTO `pig_config` VALUES ('19', 'comment_condition', '0');
INSERT INTO `pig_config` VALUES ('20', 'is_duoshuo', '0');
INSERT INTO `pig_config` VALUES ('21', 'enable_alipay', '0');
INSERT INTO `pig_config` VALUES ('22', 'enable_alipay_quick', '0');
INSERT INTO `pig_config` VALUES ('23', 'enable_wx', '0');
INSERT INTO `pig_config` VALUES ('24', 'delete_adv', '1');
INSERT INTO `pig_config` VALUES ('25', 'adv', '5c3ead6d442c7.jpg');

-- ----------------------------
-- Table structure for pig_coupon
-- ----------------------------
DROP TABLE IF EXISTS `pig_coupon`;
CREATE TABLE `pig_coupon` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除 0否1是',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '优惠券标题',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '优惠券类型 1满减 2立减 3.新客大礼包',
  `full_money` decimal(20,0) NOT NULL DEFAULT '0' COMMENT '适用金额',
  `money` decimal(20,0) NOT NULL DEFAULT '0' COMMENT '面值',
  `goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '关联商品(适用立减类型优惠券)',
  `expired` int(11) NOT NULL DEFAULT '1' COMMENT '有效期(从领取时间开始计算)',
  `crdate` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `tstamp` int(11) NOT NULL DEFAULT '0' COMMENT '编辑时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='优惠券表';

-- ----------------------------
-- Records of pig_coupon
-- ----------------------------
INSERT INTO `pig_coupon` VALUES ('1', '0', '喜迎春节', '1', '199', '25', '0', '3', '1546567723', '1546567723');
INSERT INTO `pig_coupon` VALUES ('2', '0', '新人有利', '3', '0', '10', '0', '2', '1547258512', '1547258512');
INSERT INTO `pig_coupon` VALUES ('3', '0', '1', '3', '0', '15', '0', '1', '1547260008', '1547260008');
INSERT INTO `pig_coupon` VALUES ('4', '0', '发钱啦', '3', '0', '25', '0', '5', '1547260128', '1547260128');

-- ----------------------------
-- Table structure for pig_email
-- ----------------------------
DROP TABLE IF EXISTS `pig_email`;
CREATE TABLE `pig_email` (
  `key` varchar(225) COLLATE utf8_unicode_ci NOT NULL COMMENT 'KEY',
  `subject` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '标题',
  `content` text COLLATE utf8_unicode_ci COMMENT '邮件内容',
  `bcc` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '暗送'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='找回密码';

-- ----------------------------
-- Records of pig_email
-- ----------------------------

-- ----------------------------
-- Table structure for pig_goods
-- ----------------------------
DROP TABLE IF EXISTS `pig_goods`;
CREATE TABLE `pig_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hidden` tinyint(1) DEFAULT '0' COMMENT '发布 0是1否',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除 0否1是',
  `category_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '关联分类',
  `name` varchar(225) NOT NULL DEFAULT '' COMMENT '商品名称',
  `remark` text COMMENT '描述(多个用,隔开)',
  `type` tinyint(1) DEFAULT '0' COMMENT '规格类型 0单规格 1多规格',
  `origin_area` varchar(255) NOT NULL DEFAULT '' COMMENT '产地',
  `postage_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '运费',
  `suggest_start_price` decimal(20,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '参团起始价',
  `suggest_end_price` decimal(20,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '参团结束价',
  `image` varchar(255) NOT NULL DEFAULT '' COMMENT '主图',
  `links` varchar(255) DEFAULT '' COMMENT '视频链接',
  `content` mediumtext COMMENT '商品详情',
  `crdate` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `tstamp` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `name` (`name`) USING BTREE COMMENT '商品名',
  KEY `category_id` (`category_id`),
  CONSTRAINT `pig_goods_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `pig_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='商品表';

-- ----------------------------
-- Records of pig_goods
-- ----------------------------
INSERT INTO `pig_goods` VALUES ('6', '0', '0', '1', '褚橙', null, '1', '上海', '10.00', '45.00', '65.00', '5c2de5788708e.jpg', 'https://ww.baidu.com', '<p><img src=\"http://teambuy.zero.com/Uploads/rte/image/20190103/1546506731381917.png\" title=\"1546506731381917.png\" alt=\"3NAIPF6OjZ2kJmKroUdt2dE1uuXs2Z2r3u.png\"/></p>', '1546506735', '1546764388');
INSERT INTO `pig_goods` VALUES ('11', '0', '0', '1', '重庆土鸡(母鸡5斤重)', '健康,绿色', '0', '重庆', '10.00', '80.00', '100.00', '5c36b04f7f41e.jpg', 'https://ww.baidu.com', '<p>好<img src=\"http://teambuy.zero.com/Uploads/rte/image/20190112/1547273436913636.jpg\" title=\"1547273436913636.jpg\" alt=\"5c36af508b0be.jpg\"/></p>', '1547087951', '1547273459');

-- ----------------------------
-- Table structure for pig_goods_info
-- ----------------------------
DROP TABLE IF EXISTS `pig_goods_info`;
CREATE TABLE `pig_goods_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除 0否1是',
  `goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '关联商品id',
  `sku` varchar(255) NOT NULL DEFAULT '' COMMENT '商品编码',
  `assistant` varchar(255) NOT NULL DEFAULT '' COMMENT '商品规格(多个规格值用,隔开)',
  `sale_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '零售价',
  `cost_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '成本价',
  `distribution_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '分销价',
  `stock` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '库存',
  `crdate` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `tstamp` int(11) NOT NULL DEFAULT '0' COMMENT '编辑时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='商品详情表';

-- ----------------------------
-- Records of pig_goods_info
-- ----------------------------
INSERT INTO `pig_goods_info` VALUES ('1', '1', '6', 'kl10086', '精品大果5斤装', '55.00', '40.00', '0.00', '100', '1546506735', '1546764388');
INSERT INTO `pig_goods_info` VALUES ('2', '1', '6', 'jp1008525654', '精品小果6斤装', '60.00', '38.00', '56.00', '60', '1546506735', '1546511736');
INSERT INTO `pig_goods_info` VALUES ('3', '0', '6', 'dg100820', '精品大果5斤装', '120.00', '65.00', '99.00', '100', '1546764388', '1546764388');
INSERT INTO `pig_goods_info` VALUES ('4', '0', '6', 'dg1005203', '精品大果10斤装', '210.00', '110.00', '188.00', '50', '1546764388', '1546764388');
INSERT INTO `pig_goods_info` VALUES ('5', '0', '11', 'tj100236521', '', '105.00', '70.00', '90.00', '100', '1547087951', '1547273459');

-- ----------------------------
-- Table structure for pig_group
-- ----------------------------
DROP TABLE IF EXISTS `pig_group`;
CREATE TABLE `pig_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '拼团类型 1.普通团 2.神秘团 3.自定义团',
  `limit` int(11) NOT NULL DEFAULT '1' COMMENT '限购件数',
  `num` int(11) NOT NULL DEFAULT '2' COMMENT '拼团人数',
  `auto_group` tinyint(1) NOT NULL DEFAULT '0' COMMENT '自动拼团 0否1是 (针对普通团)',
  `group_num` varchar(225) NOT NULL DEFAULT '' COMMENT '团编号',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id(团长)',
  `goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品id',
  `goods_name` varchar(255) NOT NULL DEFAULT '' COMMENT '商品名称',
  `complete_time` int(11) NOT NULL DEFAULT '0' COMMENT '满团时间',
  `crdate` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `expired` int(11) NOT NULL DEFAULT '0' COMMENT '过期时间',
  `remark` varchar(225) DEFAULT '' COMMENT '团备注',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '团状态 0未开启 1已开启(团长开团为支付未开启状态 开团支付未开启状态)',
  `start` int(11) NOT NULL DEFAULT '0' COMMENT '开团时间(团长支付订单时间)',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `group_num` (`group_num`) USING BTREE,
  KEY `crdate` (`crdate`) USING BTREE,
  KEY `expired` (`expired`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='开团表';

-- ----------------------------
-- Records of pig_group
-- ----------------------------
INSERT INTO `pig_group` VALUES ('1', '3', '1', '2', '2', '1', '201901081235451', '1', '6', '褚橙', '1546941301', '1546940101', '1548761128', '拼团成功,系统拼团', '1', '1548757528');

-- ----------------------------
-- Table structure for pig_image
-- ----------------------------
DROP TABLE IF EXISTS `pig_image`;
CREATE TABLE `pig_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) DEFAULT '0',
  `category_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属分类id',
  `image` varchar(255) NOT NULL DEFAULT '' COMMENT '图片',
  `remark` varchar(255) DEFAULT '' COMMENT '图片描述',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `pig_image_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `pig_image_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='图库表';

-- ----------------------------
-- Records of pig_image
-- ----------------------------
INSERT INTO `pig_image` VALUES ('3', '0', '1', '5c3e940193776.jpg', '', '1547604993', '1547604993');

-- ----------------------------
-- Table structure for pig_image_category
-- ----------------------------
DROP TABLE IF EXISTS `pig_image_category`;
CREATE TABLE `pig_image_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '分类名称',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='图片分类表';

-- ----------------------------
-- Records of pig_image_category
-- ----------------------------
INSERT INTO `pig_image_category` VALUES ('1', '商品图', '0', '1547604976', '1547604976');

-- ----------------------------
-- Table structure for pig_log
-- ----------------------------
DROP TABLE IF EXISTS `pig_log`;
CREATE TABLE `pig_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text CHARACTER SET utf8 NOT NULL,
  `crdate` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='回调日志\r\n';

-- ----------------------------
-- Records of pig_log
-- ----------------------------

-- ----------------------------
-- Table structure for pig_member
-- ----------------------------
DROP TABLE IF EXISTS `pig_member`;
CREATE TABLE `pig_member` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tstamp` int(11) DEFAULT '0' COMMENT '修改时间',
  `crdate` int(11) DEFAULT '0' COMMENT '创建时间',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '删除',
  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '昵称',
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '手机号码',
  `parent_id` int(11) DEFAULT '0' COMMENT '推荐人id',
  `openid` varchar(255) NOT NULL DEFAULT '',
  `wxlogo` varchar(255) DEFAULT '' COMMENT '微信头像',
  `gender` varchar(255) DEFAULT '' COMMENT '性别',
  `area` varchar(255) DEFAULT '' COMMENT '地区',
  `is_colonel` tinyint(1) DEFAULT '0' COMMENT '是否团长 0否1是',
  PRIMARY KEY (`id`),
  KEY `nickname` (`nickname`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='小程序端用户';

-- ----------------------------
-- Records of pig_member
-- ----------------------------
INSERT INTO `pig_member` VALUES ('1', '1547262768', '1545181625', '0', '阿魂', '18812345678', '0', '', 'https://wx.qlogo.cn/mmopen/vi_32/g70JffkvicdHK32mHiaQYf1vcbskWauP4wZrJysmXsHKZBxlD3iavM0x3pjFbZmKDLS0KCQsQTFHE8NW2jl9sLkHg/132', '男', '湖北武汉', '1');

-- ----------------------------
-- Table structure for pig_menu
-- ----------------------------
DROP TABLE IF EXISTS `pig_menu`;
CREATE TABLE `pig_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tstamp` int(11) DEFAULT '0' COMMENT '修改时间',
  `crdate` int(11) DEFAULT '0' COMMENT '创建时间',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '删除',
  `hidden` tinyint(1) DEFAULT '0' COMMENT '隐藏',
  `sorting` int(11) DEFAULT '0' COMMENT '排序',
  `parent_id` int(11) DEFAULT '0' COMMENT '父级',
  `app` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '应用名称',
  `title` varchar(225) COLLATE utf8_unicode_ci NOT NULL COMMENT '菜单标题',
  `icon` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '图标',
  `alias` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '别名',
  `type` tinyint(1) DEFAULT '0' COMMENT '链接类型',
  `model` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模块',
  `action` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '方法',
  `param` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '参数',
  `url` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '链接地址',
  `target` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '打开方式',
  `content` text COLLATE utf8_unicode_ci COMMENT '页面内容',
  `remark` text COLLATE utf8_unicode_ci COMMENT '备注',
  `position` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `active` tinyint(1) DEFAULT '0' COMMENT '是否为活动状态',
  PRIMARY KEY (`id`),
  KEY `title` (`title`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='菜单';

-- ----------------------------
-- Records of pig_menu
-- ----------------------------
INSERT INTO `pig_menu` VALUES ('1', '1546409356', '1546409356', '0', '0', '1', '0', 'Admin', '基础设置', 'icon-cogs', '', '3', '', '', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('2', '1546409507', '1546409507', '0', '0', '2', '1', 'Admin', '管理中心', 'icon-home', '', '1', 'index', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('3', '1546409626', '1546409626', '0', '0', '3', '1', 'Admin', '站点配置', 'icon-globe', '', '1', 'index', 'config', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('4', '1546409761', '1546409761', '0', '0', '11', '0', 'Admin', '商品管理', 'icon-dashboard', '', '3', '', '', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('5', '1546409803', '1546409803', '0', '0', '12', '4', 'Admin', '商品分类', 'icon-compass', '', '1', 'category', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('6', '1546409841', '1546409841', '0', '1', '13', '5', 'Admin', '添加', '', '', '1', 'category', 'save', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('7', '1546424696', '1546424696', '0', '1', '14', '5', 'Admin', '编辑', '', '', '1', 'category', 'edit', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('8', '1546409886', '1546409886', '0', '1', '15', '5', 'Admin', '删除', '', '', '1', 'category', 'del', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('9', '1546409970', '1546409970', '0', '0', '16', '4', 'Admin', '商品列表', 'icon-compass', '', '1', 'goods', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('10', '1546415291', '1546415291', '0', '1', '17', '9', 'Admin', '添加', '', '', '1', 'goods', 'save', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('11', '1546415308', '1546415308', '0', '1', '18', '9', 'Admin', '编辑', '', '', '1', 'goods', 'edit', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('12', '1546415335', '1546415335', '0', '1', '19', '9', 'Admin', '删除', '', '', '1', 'goods', 'del', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('13', '1546486152', '1546486152', '0', '0', '20', '0', 'Admin', '营销管理', 'icon-archive', '', '3', '', '', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('14', '1546486186', '1546486186', '0', '0', '21', '13', 'Admin', '优惠券', 'icon-compass', '', '1', 'coupon', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('15', '1546566020', '1546566020', '0', '1', '22', '14', 'Admin', '添加', '', '', '1', 'coupon', 'save', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('16', '1546566042', '1546566042', '0', '1', '23', '14', 'Admin', '编辑', '', '', '1', 'coupon', 'edit', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('17', '1546566058', '1546566058', '0', '1', '24', '14', 'Admin', '删除', '', '', '1', 'coupon', 'del', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('18', '1546570306', '1546570306', '0', '0', '26', '0', 'Admin', '用户管理', 'icon-group', '', '3', '', '', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('19', '1546570337', '1546570337', '0', '0', '27', '18', 'Admin', '用户列表', 'icon-compass', '', '1', 'member', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('20', '1546586365', '1546586365', '0', '0', '30', '0', 'Admin', '提现管理', 'icon-money', '', '3', '', '', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('21', '1546586414', '1546586414', '0', '0', '31', '20', 'Admin', '提现列表', 'icon-compass', '', '1', 'cash', 'index', '', '', '_self', '', '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('22', '1546593479', '1546593479', '0', '1', '32', '21', 'Admin', '同意提现', '', '', '1', 'cash', 'agree', '', '', '_self', '<hr/><p><br/></p>', '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('23', '1546593501', '1546593501', '0', '1', '33', '21', 'Admin', '拒绝提现', '', '', '1', 'cash', 'refuse', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('24', '1546593611', '1546593611', '0', '0', '34', '0', 'Admin', '订单管理', 'icon-comments-alt', '', '3', '', '', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('25', '1546593702', '1546593702', '0', '1', '36', '26', 'Admin', '发货', '', '', '1', 'order', 'send', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('26', '1546593688', '1546593688', '0', '0', '35', '24', 'Admin', '订单列表', 'icon-compass', '', '1', 'order', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('27', '1546593795', '1546593795', '0', '1', '37', '26', 'Admin', '同意退款', '', '', '1', 'order', 'agree', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('28', '1546593780', '1546593780', '0', '1', '38', '26', 'Admin', '拒绝退款', '', '', '1', 'order', 'refuse', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('29', '1546596033', '1546596033', '0', '0', '40', '0', 'Admin', '财务管理', 'icon-usd', '', '3', '', '', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('30', '1546738254', '1546738254', '0', '0', '41', '29', 'Admin', '财务列表', 'icon-compass', '', '1', 'finance', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('31', '1546738283', '1546738283', '0', '1', '42', '29', 'Admin', '数据导出', '', '', '1', 'finance', 'export', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('32', '1546738320', '1546738320', '0', '1', '39', '26', 'Admin', '数据导出', '', '', '1', 'order', 'export', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('33', '1546744849', '1546744849', '0', '0', '4', '1', 'Admin', '管理员', 'icon-renren', '', '1', 'Beusers', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('34', '1546745565', '1546745565', '0', '1', '5', '33', 'Admin', '添加用户', '', '', '1', 'Beusers', 'user_save', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('35', '1546745597', '1546745597', '0', '1', '6', '33', 'Admin', '编辑用户', '', '', '1', 'Beusers', 'user_edit', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('36', '1546745633', '1546745633', '0', '1', '7', '33', 'Admin', '删除用户', '', '', '1', 'Beusers', 'user_del', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('37', '1546745667', '1546745667', '0', '1', '8', '33', 'Admin', '添加用户组', '', '', '1', 'Beusers', 'group_save', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('38', '1546745693', '1546745693', '0', '1', '9', '33', 'Admin', '编辑用户组', '', '', '1', 'Beusers', 'group_edit', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('39', '1546745716', '1546745716', '0', '1', '10', '33', 'Admin', '删除用户组', '', '', '1', 'Beusers', 'group_del', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('40', '1546758863', '1546758863', '0', '0', '43', '0', 'Admin', '拼团管理', 'icon-th', '', '3', '', '', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('41', '1547204578', '1547204578', '0', '0', '44', '40', 'Admin', '官方拼团', 'icon-compass', '', '1', 'Activity', 'index', 'category=1', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('42', '1547204442', '1547204442', '0', '1', '45', '41', 'Admin', '添加', '', '', '1', 'activity', 'save', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('43', '1547279298', '1547279298', '0', '0', '49', '0', 'Admin', '限时抢购管理', 'icon-gbp', '', '3', '', '', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('44', '1547279310', '1547279310', '0', '0', '50', '43', 'Admin', '时间段设置', 'icon-compass', '', '1', 'seckilltime', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('45', '1547279328', '1547279328', '0', '0', '51', '43', 'Admin', '限时抢购列表', 'icon-compass', '', '1', 'seckill', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('46', '1547099966', '1547099966', '0', '1', '52', '45', 'Admin', '添加', '', '', '1', 'seckill', 'save', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('47', '1547099992', '1547099992', '0', '1', '53', '45', 'Admin', '删除', '', '', '1', 'seckill', 'del', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('48', '1547204480', '1547204480', '0', '1', '46', '41', 'Admin', '详情', '', '', '1', 'activity', 'edit', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('49', '1547204636', '1547204636', '0', '0', '48', '40', 'Admin', '代理拼团', 'icon-compass', '', '1', 'activity', 'index', 'category=2', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('50', '1547257483', '1547257483', '0', '0', '25', '13', 'Admin', '新人礼包', 'icon-compass', '', '1', 'coupon', 'index', 'type=3', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('51', '1547262926', '1547262926', '0', '1', '28', '19', 'Admin', '数据导出', '', '', '1', 'member', 'export', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('52', '1547262961', '1547262961', '0', '1', '29', '19', 'Admin', '设置团长', '', '', '1', 'member', 'setcolonel', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('53', '1547281139', '1547281139', '0', '1', '54', '45', 'Admin', '编辑', '', '', '1', 'seckill', 'edit', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('54', '1547429969', '1547429969', '0', '1', '55', '0', 'Admin', '商品评价', '', '', '1', 'comment', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('55', '1547434182', '1547434182', '0', '1', '56', '54', 'Admin', '回复', '', '', '1', 'comment', 'reply', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('56', '1547549885', '1547549885', '0', '1', '47', '41', 'Admin', '开启/关闭', '', '', '1', 'activity', 'editStatus', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('57', '1547603612', '1547603612', '0', '0', '57', '0', 'Admin', '图库管理', 'icon-dropbox', '', '3', 'Image', '', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('58', '1547603637', '1547603637', '0', '0', '62', '57', 'Admin', '图库列表', 'icon-compass', '', '1', 'image', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('59', '1547603658', '1547603658', '0', '1', '63', '58', 'Admin', '添加', '', '', '1', 'image', 'save', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('60', '1547603679', '1547603679', '0', '1', '64', '58', 'Admin', '编辑', '', '', '1', 'image', 'edit', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('61', '1547603701', '1547603701', '0', '1', '65', '58', 'Admin', '删除', '', '', '1', 'image', 'del', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('62', '1547604874', '1547604874', '0', '0', '58', '57', 'Admin', '图库分类', 'icon-compass', '', '1', 'imageCategory', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('63', '1547604906', '1547604906', '0', '1', '59', '62', 'Admin', '添加', '', '', '1', 'imageCategory', 'save', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('64', '1547604925', '1547604925', '0', '1', '60', '62', 'Admin', '编辑', '', '', '1', 'imageCategory', 'edit', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('65', '1547604950', '1547604950', '0', '1', '61', '62', 'Admin', '删除', '', '', '1', 'imageCategory', 'del', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('66', '1547607305', '1547607305', '0', '0', '66', '0', 'Admin', '广告管理', 'icon-btc', '', '3', '', '', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('67', '1547607328', '1547607328', '0', '0', '67', '66', 'Admin', '广告列表', '', '', '1', 'banner', 'index', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('68', '1547607350', '1547607350', '0', '1', '68', '67', 'Admin', '添加', '', '', '1', 'banner', 'save', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('69', '1547607368', '1547607368', '0', '1', '69', '67', 'Admin', '编辑', '', '', '1', 'banner', 'edit', '', '', '_self', null, '', null, null, '', '', '', '0');
INSERT INTO `pig_menu` VALUES ('70', '1547607388', '1547607388', '0', '1', '70', '67', 'Admin', '删除', '', '', '1', 'banner', 'del', '', '', '_self', null, '', null, null, '', '', '', '1');

-- ----------------------------
-- Table structure for pig_order
-- ----------------------------
DROP TABLE IF EXISTS `pig_order`;
CREATE TABLE `pig_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '关联团购id',
  `seckill_id` int(11) NOT NULL DEFAULT '0' COMMENT '秒杀id',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '删除',
  `order_num` varchar(255) NOT NULL DEFAULT '' COMMENT '订单号',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` varchar(255) DEFAULT '' COMMENT '收货人',
  `phone` varchar(11) DEFAULT '' COMMENT '联系电话',
  `address` varchar(255) NOT NULL DEFAULT '' COMMENT '地址',
  `total_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '总价',
  `pay_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '支付金额',
  `message` text COMMENT '买家留言',
  `is_collect_bills` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否凑单 0否1是 (针对超级团长开团,凑单为虚拟购买,不需要支付,不凑单为真实购买需支付)',
  `is_pay` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否支付  0否1是',
  `pay_time` int(11) DEFAULT '0' COMMENT '支付时间',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态 1.待付款 2.待发货 3.待收货 4.待评价 5.已完成 6.已取消7.已失效(已过期) ->针对参团未支付,且过了可支付时间的订单 8，退款申请中 9.退款中 10.拒绝退款 11.退款完成',
  `wx_num` varchar(255) DEFAULT '' COMMENT '微信单号',
  `formId` varchar(255) DEFAULT '' COMMENT '发送小程序模板消息的formId',
  `remark` text COMMENT '备注(工程师或后台人员备注)',
  `goods_id` int(11) NOT NULL,
  `goods_name` varchar(255) NOT NULL DEFAULT '' COMMENT '商品名称',
  `goods_info_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品详细id',
  `assistant` varchar(255) DEFAULT '' COMMENT '商品规格',
  `sku` varchar(225) NOT NULL DEFAULT '' COMMENT '商品编码',
  `deal_money` decimal(20,0) NOT NULL DEFAULT '0' COMMENT '团购价',
  `cost_money` decimal(20,0) NOT NULL DEFAULT '0' COMMENT '成本价',
  `distribution_money` decimal(20,0) NOT NULL DEFAULT '0' COMMENT '分销价',
  `num` int(11) NOT NULL DEFAULT '1' COMMENT '购买数量',
  `user_coupon_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户优惠券id',
  `coupon_money` decimal(20,0) DEFAULT '0' COMMENT '优惠券面值',
  `postage_money` decimal(20,0) NOT NULL DEFAULT '0' COMMENT '快递费',
  `postage_name` varchar(255) DEFAULT '' COMMENT '快递名称',
  `postage_code` varchar(255) DEFAULT '' COMMENT '快递拼音',
  `postage_num` varchar(225) DEFAULT '' COMMENT '快递编号',
  `crdate` int(11) DEFAULT '0' COMMENT '创建时间',
  `tstamp` int(11) DEFAULT '0' COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  KEY `order_num` (`order_num`) USING BTREE COMMENT '订单编号',
  KEY `name` (`name`) USING BTREE COMMENT '收件人',
  KEY `phone` (`phone`) USING BTREE COMMENT '联系电话',
  KEY `address` (`address`) USING BTREE COMMENT '收货地址',
  KEY `pay_time` (`pay_time`) USING BTREE COMMENT '付款时间',
  KEY `sku` (`sku`) USING BTREE COMMENT '商品编码',
  KEY `goods_name` (`deal_money`) USING BTREE COMMENT '商品名称',
  KEY `postage_name` (`postage_name`) USING BTREE COMMENT '快递名称',
  KEY `crdate` (`crdate`) USING BTREE COMMENT '创建时间'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='订单';

-- ----------------------------
-- Records of pig_order
-- ----------------------------
INSERT INTO `pig_order` VALUES ('1', '1', '0', '0', '2015181815515', '1', 'all in', '15972053215', '湖北武汉', '60.00', '60.00', null, '0', '0', '1548757528', '2', '7112237246', '', null, '11', '重庆土鸡(5斤装)', '5', '', 'tj100236521', '55', '70', '0', '1', '0', '0', '0', '苏宁快递', 'suning', '0052202739270101', '1547087951', '1548757528');
INSERT INTO `pig_order` VALUES ('2', '1', '0', '0', '2019181815515', '1', 'hello it\'s me!', '15972053215', '湖北咸宁', '80.00', '80.00', null, '0', '0', '1547089951', '3', 'wx151515151512565', '', null, '6', '褚橙', '3', '精品大果5斤装', 'dg100820', '70', '65', '0', '1', '1', '5', '10', '圆通快递', 'yutong', '20215151022', '1547088951', '1547265582');

-- ----------------------------
-- Table structure for pig_order_info
-- ----------------------------
DROP TABLE IF EXISTS `pig_order_info`;
CREATE TABLE `pig_order_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) DEFAULT '0' COMMENT '删除',
  `order_id` int(11) NOT NULL DEFAULT '0' COMMENT '订单id',
  `category_id` int(11) NOT NULL DEFAULT '0' COMMENT '分类名称',
  `failure_id` int(11) NOT NULL DEFAULT '0' COMMENT '故障id',
  `name` varchar(25) NOT NULL DEFAULT '' COMMENT '名称',
  `price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='订单详情';

-- ----------------------------
-- Records of pig_order_info
-- ----------------------------
INSERT INTO `pig_order_info` VALUES ('1', '0', '1', '1', '1', '屏幕故障', '0.00');
INSERT INTO `pig_order_info` VALUES ('2', '0', '1', '1', '1', '屏幕故障', '0.00');

-- ----------------------------
-- Table structure for pig_plan
-- ----------------------------
DROP TABLE IF EXISTS `pig_plan`;
CREATE TABLE `pig_plan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `activity_id` int(11) NOT NULL DEFAULT '0' COMMENT '活动id',
  `goods_info_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品详情id',
  `group_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '团购价',
  `leader_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '团长价(针对神秘团)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='价格方案表';

-- ----------------------------
-- Records of pig_plan
-- ----------------------------
INSERT INTO `pig_plan` VALUES ('1', '1', '0', '0', '1.00', '0.00');
INSERT INTO `pig_plan` VALUES ('2', '0', '0', '0', '1.00', '0.00');
INSERT INTO `pig_plan` VALUES ('3', '0', '0', '0', '1.00', '0.00');
INSERT INTO `pig_plan` VALUES ('4', '0', '1', '3', '88.00', '0.00');
INSERT INTO `pig_plan` VALUES ('5', '0', '1', '4', '144.00', '0.00');
INSERT INTO `pig_plan` VALUES ('6', '0', '2', '3', '88.00', '78.00');
INSERT INTO `pig_plan` VALUES ('7', '0', '2', '4', '166.00', '140.00');
INSERT INTO `pig_plan` VALUES ('8', '0', '3', '3', '88.00', '0.00');
INSERT INTO `pig_plan` VALUES ('9', '0', '3', '4', '166.00', '0.00');
INSERT INTO `pig_plan` VALUES ('10', '0', '4', '3', '110.00', '0.00');
INSERT INTO `pig_plan` VALUES ('11', '0', '4', '4', '198.00', '0.00');
INSERT INTO `pig_plan` VALUES ('12', '0', '6', '5', '96.00', '0.00');
INSERT INTO `pig_plan` VALUES ('13', '0', '7', '5', '88.00', '0.00');
INSERT INTO `pig_plan` VALUES ('14', '0', '8', '5', '88.00', '80.00');
INSERT INTO `pig_plan` VALUES ('15', '0', '9', '5', '68.00', '0.00');

-- ----------------------------
-- Table structure for pig_queue
-- ----------------------------
DROP TABLE IF EXISTS `pig_queue`;
CREATE TABLE `pig_queue` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL COMMENT '内容',
  `crdate` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pig_queue
-- ----------------------------
INSERT INTO `pig_queue` VALUES ('1', '消息', '2019-01-08 14:30:18');
INSERT INTO `pig_queue` VALUES ('2', '消息', '2019-01-08 14:35:41');
INSERT INTO `pig_queue` VALUES ('3', '消息', '2019-01-08 14:36:03');

-- ----------------------------
-- Table structure for pig_seckill
-- ----------------------------
DROP TABLE IF EXISTS `pig_seckill`;
CREATE TABLE `pig_seckill` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除 0否1是',
  `time_id` int(11) unsigned NOT NULL COMMENT '关联秒杀时间',
  `activity_id` int(11) unsigned NOT NULL COMMENT '关联神秘团id',
  `crdate` int(11) NOT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='秒杀时间表';

-- ----------------------------
-- Records of pig_seckill
-- ----------------------------
INSERT INTO `pig_seckill` VALUES ('1', '0', '1', '2', '1547280652', '1547280652');

-- ----------------------------
-- Table structure for pig_seckill_time
-- ----------------------------
DROP TABLE IF EXISTS `pig_seckill_time`;
CREATE TABLE `pig_seckill_time` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `time` varchar(20) NOT NULL COMMENT '开始时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='限时秒杀时间';

-- ----------------------------
-- Records of pig_seckill_time
-- ----------------------------
INSERT INTO `pig_seckill_time` VALUES ('1', '10:00');
INSERT INTO `pig_seckill_time` VALUES ('2', '14:00');

-- ----------------------------
-- Table structure for pig_user_address
-- ----------------------------
DROP TABLE IF EXISTS `pig_user_address`;
CREATE TABLE `pig_user_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `crdate` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `tstamp` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `cruser_id` int(11) DEFAULT '0' COMMENT '创建人',
  `deleted` int(11) DEFAULT '0' COMMENT '删除 0否1是',
  `is_default` tinyint(1) DEFAULT '0' COMMENT '是否默认 0否1是',
  `address` varchar(255) DEFAULT '' COMMENT '地址',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '收货电话',
  `gender` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别 0男1女',
  `area` varchar(255) DEFAULT '' COMMENT '地址详细',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '收件人',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户收获地址';

-- ----------------------------
-- Records of pig_user_address
-- ----------------------------

-- ----------------------------
-- Table structure for pig_user_coupon
-- ----------------------------
DROP TABLE IF EXISTS `pig_user_coupon`;
CREATE TABLE `pig_user_coupon` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `coupon_id` int(11) NOT NULL DEFAULT '0' COMMENT '关联优惠券id(新人券为0)',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '优惠券名称',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '优惠券类型 0.新人券 1满减券 2.立减券',
  `full_money` decimal(20,0) unsigned NOT NULL DEFAULT '0' COMMENT '适用金额(仅对满减类型券)',
  `money` decimal(20,0) NOT NULL,
  `goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '关联商品(针对立减类型券)',
  `used_time` int(11) DEFAULT '0' COMMENT '使用时间',
  `crdate` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `expired` int(11) NOT NULL DEFAULT '0' COMMENT '过期时间',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `used_time` (`used_time`) USING BTREE,
  KEY `expired` (`expired`) USING BTREE,
  CONSTRAINT `pig_user_coupon_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `pig_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户优惠券表';

-- ----------------------------
-- Records of pig_user_coupon
-- ----------------------------
