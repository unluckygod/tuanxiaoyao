//app.js
const event = require('/utils/event.js');
const common = require('/utils/common.js');
App({
    event: event,
    common: common,
    onLaunch: function () {
        var that = this;
        // 登录
        wx.login({
            success: res => {
                var data = {
                    code: res.code
                }
                that.common.apiRequest('api/member/getOpenid', data, function (res) {
                    if (res.code == 1) {
                        that.globalData.openid = res.data.openid;
                        wx.setStorageSync('session_key', res.data.session_key);
                        /*获取用户信息 */
                        that.common.apiRequest('api/member/getUserInfo', { openid: res.data.openid }, function (result) {
                            if (result.data) {
                                that.globalData.userInfo = result.data;
                                
                            }
                            // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                            // 所以此处加入 callback 以防止这种情况
                            if (that.userInfoReadyCallback) {
                                console.log('*******');
                                var param = result.data?result.data:null;
                                that.userInfoReadyCallback(param);
                            }
                        })
                    }else{
                        wx.showToast({
                            title: 'appid与appsecret不匹配',
                            icon: 'none',
                            duration:2000
                        })
                    }
                })
            }
        })
        //强制更新
        // 获取小程序更新机制兼容
        if (wx.canIUse('getUpdateManager')) {
            const updateManager = wx.getUpdateManager()
            updateManager.onCheckForUpdate(function (res) {
                // 请求完新版本信息的回调
                if (res.hasUpdate) {
                    updateManager.onUpdateReady(function () {
                        wx.showModal({
                            title: '更新提示',
                            content: '新版本已经准备好，是否重启应用？',
                            success: function (res) {
                                if (res.confirm) {
                                    // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                                    updateManager.applyUpdate()
                                }
                            }
                        })
                    })
                    updateManager.onUpdateFailed(function () {
                        // 新的版本下载失败
                        wx.showModal({
                            title: '已经有新版本了哟~',
                            content: '新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~',
                        })
                    })
                }
            })
        } else {
            // 如果希望用户在最新版本的客户端上体验您的小程序，可以这样子提示
            wx.showModal({
                title: '提示',
                content: '当前微信版本过低，无法自动更新小程序，请升级到最新微信版本后重试。'
            })
        }
    },
    globalData: {
        userInfo: null,
        openid: null,
        wxInfo: null
    }
})