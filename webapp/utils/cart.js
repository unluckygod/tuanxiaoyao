var expired_time = 7200;

/**
 * 获取当前时间,精确到秒
 */
function getTimestamps() {
    return Date.parse(new Date()) * 0.001;
}
/**
 * 获取购物车所有数据
 * @param  string   shop_id 所属门店 
 */
function getCartGoods(shop_id){
    var cache = wx.getStorageSync(shop_id + 'cartGoods');
    var time = wx.getStorageSync(shop_id + 'cartGoods_expired');
    var currentTime = getTimestamps();
    if (cache == null) {
        return [];
    }
    if (currentTime >= time) {
        storeCartGoods(null);
        return [];
    }
    return cache;
    //return wx.getStorageSync(shop_id + 'cartGoods') ? wx.getStorageSync(shop_id + 'cartGoods'):[];
}

/**
 * 保存购物车数据
 * @param  string   shop_id 所属门店
 */
function storeCartGoods(shop_id,data){
    wx.setStorageSync(shop_id + 'cartGoods', data);
    var time = getTimestamps() + expired_time;
    wx.setStorageSync(shop_id + 'cartGoods_expired', time);
    //return wx.setStorageSync(shop_id + 'cartGoods',data);
}

/**
 * 添加购物车
 * @param  string   shop_id 所属门店
 * @param  object   goods
 * @return bool     
 */
function addCartGoods(shop_id,goods){
    var cartGoods = getCartGoods(shop_id);
    if(cartGoods.length){
        var result = filter(cartGoods,'id',goods.id);
        if(result.length){
            if(result.length){
                ++result[0].num;
                for(var i in cartGoods){
                    if(cartGoods[i].id == result[0].id)
                        cartGoods[i] = result[0];
                }
            }else{
                cartGoods.push(goods);
            }    
        }else{
            cartGoods.push(goods); 
        }
    }else{
        cartGoods.push(goods);
    }
    return storeCartGoods(shop_id,cartGoods);
}

/**
 * 清空购物车
 * @param  string   shop_id 所属门店
 */
function deleteCartGoods(shop_id){
    return storeCartGoods(shop_id,null);
}

/**
 * 删除购物车商品
 * @param  string   shop_id 所属门店
 */
function removeCartGoods(shop_id,goods){
    var cartGoods = getCartGoods(shop_id);
    if (cartGoods) {
        result = filter(cartGoods, 'id', goods.id,0);
        return storeCartGoods(shop_id,result);
    } else {
        return false;
    }
}

/**
 * 购物车商品加
 * @param  string   shop_id 所属门店
 * @param int  index
 */
function addNumCartGoods(shop_id,index){
    var cartGoods = getCartGoods(shop_id);
    if (cartGoods) {
        try{
            ++cartGoods[index].num;
            return storeCartGoods(shop_id,cartGoods);
        }catch(e){
            return false;
        }
    } else {
        return false;
    }
}

/**
 * 购物车商品减
 * @param  string   shop_id 所属门店
 * @param int  index
 */
function minusNumCartGoods(shop_id,index) {
    var cartGoods = getCartGoods(shop_id);
    if (cartGoods) {
        try {
            if(--cartGoods[index].num <= 0){
                cartGoods.splice(index,1);
            }
            return storeCartGoods(shop_id,cartGoods);
        } catch (e) {
            return false;
        }
    } else {
        return false;
    }
}

/**
 * 购物车数量统计
 * @param  string   shop_id 所属门店
 * @param string  field --num数量  price 价格
 * @param float 
 */
function statistics(shop_id,field = 'num'){
    let count = 0;
    var cartGoods = getCartGoods(shop_id);
    if(cartGoods){
        for(var i in cartGoods){
            switch(field){
                case 'num':
                    count += parseFloat(cartGoods[i][field]);
                break;
                default:
                    count += parseFloat(cartGoods[i][field]) * parseFloat(cartGoods[i]['num']);
                break;
            }   
        }
    }
    return count;
}

/**
 * 筛选是否有同类菜品
 * @param  array  data
 * @param  string key
 * @param  string val
 * @param  int    rule    规则 0不相等  1相等
 * @return mixed
 */
function filter(data, key = 'id', val,rule = 1){
    return data.filter(function(res){
        if(rule)
            return res[key] == val;
        else
            return res[key] != val;
    });
}

/**
 * 传递方法(封装的方法需在调用时先引入,再传递方法之后才能调用 前一参数为传递方法名,后一参数为实际方法名)
 */
module.exports = {
    getCartGoods: getCartGoods,
    addCartGoods: addCartGoods,
    deleteCartGoods: deleteCartGoods,
    removeCartGoods: removeCartGoods,
    addNumCartGoods: addNumCartGoods,
    minusNumCartGoods: minusNumCartGoods,
    statistics: statistics,
    filter: filter
}