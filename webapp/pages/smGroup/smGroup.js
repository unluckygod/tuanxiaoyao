// pages/store/store.js
const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        activities: [],
        network: false,
        loading: false,
        page: 1,
        loadingMore: true
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onShow: function (options) {
        var that = this;
        that.setData({
            activities: [],
            network: false,
            loading: false,
            page: 1,
            loadingMore: true
        });
        that.getActivities();
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        var that = this;
        that.setData({
            page: 1,
            activities: [],
        });
        that.getActivities();
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function (e) {
        var that = this;
        /*判断是否有更多数据 */
        if (!that.data.loadingMore) {
            return;
        }
        /*判断是否加载完成*/
        if (that.data.loading) {
            return;
        }
        that.getActivities();
    },

    /**
     * 获取团详细
     */
    getActivities: function () {
        var that = this;
        var activities = that.data.activities;
        var page = that.data.page;
        var data = {
            type: 2,
            page: page
        }
        that.setData({
            loading: true
        });
        app.common.apiRequest('api/activity/index', data, function (res) {
            that.setData({
                network: true
            });
            activities = activities.concat(res.data);
            var loadingMore = true;
            if (res.data.length < 10) {
                loadingMore = false;
            }
            that.setData({
                activities: activities,
                loading: false,
                page: ++page,
                loadingMore: loadingMore
            });
        });
    },
})