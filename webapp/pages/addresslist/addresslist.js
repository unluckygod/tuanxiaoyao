// pages/addresslist/addresslist.js
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        network:false,
        index:0,
        addressList:[]
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        var that = this;
        that.setData({
            network:false
        });
        var user_id = app.globalData.userInfo.id;
        //加载地址列表
        app.common.apiRequest('api/address/index', { user_id: user_id }, function (res) {
            that.setData({
                addressList: res.data,
                index:0,
                network:true
            })
        });
    },
    select:function(e){
        var page = getCurrentPages();
        //判断上个页面是否为确定订单页面
        if(page[page.length - 2].route == "pages/submitorder/submitorder"){
            var that = this;
            app.event.emit('addressChange', that.data.addressList[e.currentTarget.dataset.index]);
            wx.navigateBack({});
        }
    },
    /*修改地址 */
    editAddess:function(e){
        wx.navigateTo({
            url: "../newaddress/newaddress?id=" + e.currentTarget.dataset.id,
        })
    },
    /*删除地址 */
    removeAddess:function(e){
        var that = this;
        wx.showModal({
            title: '提示',
            content: '确定删除？',
            complete:function(res){
                if(res.confirm){
                    var param = {
                        user_id: app.globalData.userInfo.id,
                        address_id: e.currentTarget.dataset.id,
                        action: 'del'
                    };
                    var index = e.currentTarget.dataset.index;
                    app.common.apiRequest('api/address/edit', param, function (res) {
                        if(res.code == 1){
                            var addressList = that.data.addressList;
                            addressList.splice(index,1);
                            that.setData({
                                addressList: addressList
                            });
                        }else{
                            wx.showToast({
                                title:'删除失败',
                                icon:'none'
                            })
                        }
                    }) 
                }
            }
        })
    },
    /*设置默认地址 */
    setDefault:function(e){
        var that=this;
        var data = {
            user_id: app.globalData.userInfo.id,
            address_id: e.currentTarget.dataset.id,
            action:'default'
        }
        var currentindex = e.currentTarget.dataset.index;
        var addressList = that.data.addressList
        app.common.apiRequest('api/address/edit', data, function (res) {  
            if (res.code != 1) {
                wx.showToast({
                    title: res.msg,
                    icon:'none'
                })
            } else {
                addressList[that.data.index].is_default = 0;
                addressList[currentindex].is_default = 1;
                that.setData({
                    addressList: addressList,
                    index: currentindex,
                });
            }
        });
    },
})