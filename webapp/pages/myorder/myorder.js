const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        navtab: ['全部', '待付款', '待发货', '待收货', '待评价'],
        activeIndex: 0,
        sliderOffset: 0,
        sliderLeft: 0,
        page: [1, 1, 1, 1, 1],
        loading: false,
        loadingMore: [true, true, true, true, true],
        list: [[], [], [], [], []],
        network: false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        var activeIndex = that.data.activeIndex;
        if (options.status) {
            activeIndex = options.status;
            var sliderOffset = activeIndex * 72;
            that.setData({
                activeIndex: options.status,
                sliderOffset: sliderOffset
            });
        }
        /*加载数据 */
        that.getOrders(activeIndex);
    },
    
    /**
     * tab导航切换
     */
    tabClick: function (e) {
        var that = this;
        if (that.data.page[e.currentTarget.id] == 1) {
            /*加载数据 */
            that.getOrders(e.currentTarget.id);
        }
        that.setData({
            sliderOffset: e.currentTarget.offsetLeft,
            activeIndex: e.currentTarget.id
        });
    },
    /**
     * 获取订单
     */
    getOrders: function (status) {
        var that = this;
        var list = that.data.list;
        var loadingMore = that.data.loadingMore;
        var page = that.data.page;
        var data = {
            user_id: app.globalData.userInfo.id,
            status: status,
            page: page[status],
            show_num: 5
        };
        that.setData({
            loading: true
        });
        app.common.apiRequest('api/order/index', data, function (res) {
            that.setData({
                network: true
            });
            list[status] = list[status].concat(res.data);
            ++page[status];
            if (res.data.length < 5) {
                loadingMore[status] = false;
            }
            that.setData({
                list: list,
                loading: false,
                page: page,
                loadingMore: loadingMore
            });
        })
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        var that = this;
        that.setData({
            activeIndex: 0,
            sliderOffset: 0,
            sliderLeft: 0,
            page: [1, 1, 1, 1, 1],
            loading: false,
            loadingMore: [true, true, true, true, true],
            list: [[], [], [], [], []],
        });
        /*加载数据 */
        that.getOrders(0);
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function (e) {
        var that = this;
        var index = that.data.activeIndex;
        /*判断是否有更多数据 */
        if (!that.data.loadingMore[index]) {
            return;
        }
        /*判断是否加载完成*/
        if (that.data.loading) {
            return;
        }
        /*加载数据 */
        that.getOrders(index);
    },

    /**
     * 支付
     */
    pay: function (e) {
        var that = this;
        var data = {
            order_num: e.currentTarget.dataset.num
        }
        app.common.apiRequest('api/WX/prePay', data, function (result) {
            if (result.code != '1') {
                wx.showToast({
                    title: result.msg,
                    icon:'none',
                    complete:function(){
                        that.setData({
                            loading: false,
                        });
                    }
                })
                
            }else{
                /*唤起微信支付 */
                wx.requestPayment({
                    timeStamp: result.data.timeStamp,
                    nonceStr: result.data.nonceStr,
                    package: result.data.package,
                    signType: result.data.signType,
                    paySign: result.data.paySign,
                    complete: function (rs) {
                        that.setData({
                            loading: false
                        });
                        if (rs.errMsg == "requestPayment:fail") {
                            wx.showToast({
                                title: rs.err_desc,
                                icon: 'none'
                            });
                        } else if (rs.errMsg == "requestPayment:fail cancel") {

                        } else {
                            //支付成功刷新页面
                            that.setData({
                                page: [1, 1, 1, 1, 1],
                                loading: false,
                                loadingMore: [true, true, true, true, true],
                                list: [[], [], [], [], []],
                            });
                            /*加载数据 */
                            that.getOrders(that.data.activeIndex);
                        }
                    }
                })
            }
        })
    },
    /**
     * 取消订单
     */
    cancel: function (e) {
        var that = this;
        wx.showModal({
            title: '提示',
            content: '确认取消订单?',
            complete:function(res){
                if (res.confirm){
                    that.setData({
                        loading: true
                    });
                    var data = {
                        order_id: e.currentTarget.id
                    }
                    app.common.apiRequest('api/order/cancelOrder', data, function (result) {
                        if (result.code != '1') {
                            that.setData({
                                loading: false,
                            });
                            wx.showToast({
                                title: result.msg,
                                icon: 'none'
                            });
                            return;
                        }
                        wx.showToast({
                            title:'提交成功',
                            icon:'none',
                            duration:1500,
                            success:function(){
                                setTimeout(function(){
                                    //取消刷新页面
                                    that.setData({
                                        page: [1, 1, 1, 1, 1],
                                        loading: false,
                                        loadingMore: [true, true, true, true, true],
                                        list: [[], [], [], [], []],
                                    });
                                    /*加载数据 */
                                    that.getOrders(that.data.activeIndex);
                                },1500);
                            }
                        })
                    })
                }
            }
        })    
    },
})