// pages/editaddress/editaddress.js
var app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        addInfo:null,
        loading:false,
        area:[],
        network:false
    },

    /**
     * 监听页面加载
     */
    onLoad:function(e){
        var that = this;
        if(e.id !== undefined){
            var data = {
                address_id: e.id
            };
            app.common.apiRequest('api/address/info', data, function (res) {
                that.setData({
                    network:true,
                    addInfo:res.data,
                    area: res.data.area.split(',')
                });
            });
            wx.setNavigationBarTitle({
                title: '修改地址'
            })
        }else{
            wx.setNavigationBarTitle({
                title:'新增地址'
            });
            that.setData({
                network: true,
            });
        }
        that.setData({
            user_id: app.globalData.userInfo.id,
            id: e.id?e.id:0
        });
            
    },
    bindRegionChange:function(e){
        var that = this;
        var area = e.detail.value;
        that.setData({
            area: area
        });
    },

    /**
     * 提交
     */
    formSubmit: function (e) {
        var that = this;
        that.setData({
            loading: true
        });
        /*表单数据验证 */
        var result = that.validate(e);
        if (!result.status) {
            wx.showToast({
                title: result.msg,
                icon: 'none'
            });
            that.setData({
                loading:false
            });
            return;
        }
        var data = e.detail.value;
        data.area = data.area.join('');
        app.common.apiRequest('api/address/save',data,function(res){
            if(res.code == 1){
                wx.showToast({
                    title:'保存成功',
                    icon:'none',
                    complete:function(){
                        setTimeout(function(){
                            that.setData({
                                loading: false
                            });
                            app.event.emit('addressChange', e.detail.value);
                            wx.navigateBack({});
                        },1500)
                    }
                })
            }else[
                wx.showToast({
                    title: res.msg,
                    icon: 'none',
                    duration:2500,
                    complete:function(){
                        that.setData({
                            loading: false
                        });
                    }
                })
            ]
        });
    },

    /**
     * 验证数据
     */
    validate: function (e) {
        var that = this;
        var data = e.detail.value;
        var result = {
            status: true,
            msg: ''
        };
        var param = {};
        //数据验证
        var temp = {
            name: {
                key: data.name,
                reg: [/\S/],
                msg: ['请填写联系人'],
            },
            phone: {
                key: data.phone,
                reg: [/\S/, /^1[345789]\d{9}$/],
                msg: ['请填写手机号', '手机号格式错误'],
            },
            area: {
                key: data.area,
                reg: [/\S/],
                msg: ['请选择地址'],
            },
            address: {
                key: data.address,
                reg: [/\S/],
                msg: ['请填写详细地址'],
            },
        }
        param = Object.assign(param, temp);
        for (var i in param) {
            for (var j in param[i].reg) {
                if (!param[i].reg[j].test(param[i].key)) {
                    result.status = false;
                    result.msg = param[i].msg[j];
                    return result;
                }
            }
        }
        return result;
    },
})