// pages/mycoupons/mycoupons.js
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        navtab: ['全部', '未使用', '已使用', '已失效'],
        navIndex: 0,
        page: [1, 1, 1, 1],
        loading: false,
        loadingMore: [true, true, true, true],
        list: [[], [], [], []],
        network: false,
        scrollTop:0
    },
    //一级导航
    navtab: function (e) {
        var that = this;
        if (that.data.page[e.currentTarget.id] == 1) {
            /*加载数据 */
            that.getCoupons(e.currentTarget.id);
        }
        that.setData({
            sliderOffset: e.currentTarget.offsetLeft,
            navIndex: e.currentTarget.id
        });
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        /*加载数据 */
        this.getCoupons(0);
    },

    //监听页面滚动事件
    onPageScroll: function (e) {
        var that = this;
        that.setData({
            scrollTop: e.scrollTop
        });
    },
    //返回顶部
    gotop: function () {
        if (wx.pageScrollTo) {
            wx.pageScrollTo({
                scrollTop: 0
            });
        } else {
            wx.showModal({
                title: '提示',
                content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
            })
        }
    },

    /**
     * 获取订单
     */
    getCoupons: function (status) {
        var that = this;
        var list = that.data.list;
        var loadingMore = that.data.loadingMore;
        var page = that.data.page;
        var data = {
            user_id: app.globalData.userInfo.id,
            status: status,
            page: page[status],
            show_num: 8
        };
        that.setData({
            loading: true
        });
        app.common.apiRequest('api/coupon/list', data, function (res) {
            that.setData({
                network: true
            });
            list[status] = list[status].concat(res.data);
            ++page[status];
            if (res.data.length < 8) {
                loadingMore[status] = false;
            }
            that.setData({
                list: list,
                loading: false,
                page: page,
                loadingMore: loadingMore
            });
        })
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        var that = this;
        that.setData({
            navIndex: 0,
            sliderOffset: 0,
            sliderLeft: 0,
            page: [1, 1, 1, 1],
            loading: false,
            loadingMore: [true, true, true, true],
            list: [[], [], [], []],
        });
        /*加载数据 */
        that.getCoupons(0);
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function (e) {
        var that = this;
        var index = that.data.navIndex;
        /*判断是否有更多数据 */
        if (!that.data.loadingMore[index]) {
            return;
        }
        /*判断是否加载完成*/
        if (that.data.loading) {
            return;
        }
        /*加载数据 */
        that.getCoupons(index);
    },
})