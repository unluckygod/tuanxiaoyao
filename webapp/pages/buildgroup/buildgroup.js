var util = require('../../utils/choseTime.js')
var cellId;
var date = new Date();
var years = [];
var months = [];
var days = [];
var hours = [];
var minutes = [];
var seconds = [];

var nowYear;
var nowMonth;
var nowDay;
var nowHour;
var nowMinute;
var nowSecond;

var t = 0;
var show = false;
var moveY = 200;

for (let i = new Date().getFullYear(); i < 2117; i++) {
    years.push(i)
}

for (let i = 1; i <= 12; i++) {
    i = zeroPadding(i);
    months.push(i)
}

for (let i = 1; i <= 31; i++) {
    i = zeroPadding(i);
    days.push(i)
}

for (let i = 0; i <= 23; i++) {
    i = zeroPadding(i);
    hours.push(i)
}
for (let i = 0; i <= 59; i++) {
    i = zeroPadding(i);
    minutes.push(i)
}
for (let i = 0; i <= 59; i++) {
    i = zeroPadding(i);
    seconds.push(i)
}

const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        years: years,
        months: months,
        days: days,
        hours: hours,
        minutes: minutes,
        seconds: seconds,
        goods:null,
        select:false,
        kefu:false,
        is_support_coupon:1,
        auto_group: 1,
        self_buy: 1,
        loading:false,
        info_index: 0,
        num: 1,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function () {
        var that = this;
        this.setData({
            userInfo: app.globalData.userInfo
        });

        //开启监听选择商品事件
        app.event.on('choseGoods',that,function(res){
            that.setData({
                goods:res,
                self_buy:1,
                num:1,
                info_index:0
            });
        })
        var that = this;
        //获取时间
        getNowDate(new Date());
        // 初始化
        days = getDays(nowYear, nowMonth);
        that.setData({
            year: nowYear,
            month: zeroPadding(nowMonth),
            days: days,
            day: zeroPadding(nowDay),
            hour: zeroPadding(nowHour),
            minute: zeroPadding(nowMinute),
            second: zeroPadding(nowSecond),
            value: [nowYear - nowYear, nowMonth - 1, nowDay - 1, nowHour, nowMinute, nowSecond]
        })
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload:function(){
        var that = this;
        app.event.remove('choseGoods',that);
    },

    /**
     * 切换凑团
     */
    switchGroup:function(e){
        var bool = e.detail.value;
        var auto_group = bool ? 1 : 0;
        this.setData({
            auto_group: auto_group
        });
    },


    /**
     * 切换使用优惠券状态
     */
    switchCoupon: function (e) {
        var bool = e.detail.value;
        var is_support_coupon = bool?1:0;
        this.setData({
            is_support_coupon: is_support_coupon
        });
    },

    /**
     * 切换自购
     */
    switchBuy: function (e) {
        var bool = e.detail.value;
        var self_buy = bool ? 1 : 0;
        this.setData({
            self_buy: self_buy
        });
    },


    formSubmit: function (e) {
        var that = this, data;
        /*表单数据验证 */
        var result = that.validate(e);
        if (!result.status) {
            wx.showToast({
                title: result.msg,
                icon: 'none'
            });
            return;
        }
        data = e.detail.value;
        that.setData({
            loading: true
        });
        app.common.apiRequest('api/activity/createActivity',data,function(res){
            var msg = '创建成功';
            if(res.code != 1){
                msg = res.msg;
            }
            wx.showToast({
                title:msg,
                icon:'none',
                duration:1500,
                success:function(){
                    that.setData({
                        loading:false
                    });
                    if(res.code == 1){
                        setTimeout(function(){
                            wx.redirectTo({
                                url: '../mybuiltgroup/mybuiltgroup',
                            })
                        },1500)
                        
                    }
                }
            })
        })
    },
    /**
     * 验证数据
     */
    validate: function (e) {
        var that = this;
        var data = e.detail.value;
        var result = {
            status: true,
            msg: ''
        };
        var param = {};
        //数据验证
        var temp = {
            num: {
                key: data.num,
                reg: [/\S/,/^[1-9]+d*$/],
                msg: ['请填写拼团人数','请填写正确的拼团人数'],
            },
            start: {
                key: data.start,
                reg: [/\S/],
                msg: ['请填写选择活动开始时间'],
            },
        }
        param = Object.assign(param, temp);
        for (var i in param) {
            for (var j in param[i].reg) {
                if (!param[i].reg[j].test(param[i].key)) {
                    result.status = false;
                    result.msg = param[i].msg[j];
                    return result;
                }
            }
        }
        return result;
    },
    onReady: function () {
        this.animation = wx.createAnimation({
            transformOrigin: "50% 50%",
            duration: 0,
            timingFunction: "ease",
            delay: 0
        }
        )
        this.animation.translateY(200 + 'vh').step();
        this.setData({
            animation: this.animation.export(),
            show: show
        })
    },
    // 滑动事件
    bindChange: function (e) {
        const val = e.detail.value

        // 获取天数
        days = getDays(val[0] + 2017, val[1] + 1);

        this.setData({
            days: days,
            year: this.data.years[val[0]],
            month: this.data.months[val[1]],
            day: this.data.days[val[2]],
            hour: this.data.hours[val[3]],
            minute: this.data.minutes[val[4]],
            second: this.data.seconds[val[5]]
        })
    },
    //移动按钮点击事件
    translate: function (e) {
        this.setData({
            kefu:true
        });
        if (t == 0) {
            moveY = 0;
            show = false;
            t = 1;
        } else {
            moveY = 200;
            show = true;
            t = 0;
        }
        animationEvents(this, moveY, show);

    },
    //隐藏弹窗浮层
    hiddenFloatView() {
        this.setData({
            select:true,
            kefu:false
        });
        moveY = 200;
        show = true;
        t = 0;
        animationEvents(this, moveY, show);

    },
})

//动画事件
function animationEvents(that, moveY, show) {
    console.log("moveY:" + moveY + "\nshow:" + show);
    that.animation = wx.createAnimation({
        transformOrigin: "50% 50%",
        duration: 400,
        timingFunction: "ease",
        delay: 0
    }
    )
    that.animation.translateY(moveY + 'vh').step()

    that.setData({
        animation: that.animation.export(),
        show: show
    })

}


//输入年、月计算当月天数
var getDays = function (year, month) {
    // month 取自然值，从 1-12 而不是从 0 开始
    var dayCount = new Date(year, month, 0).getDate();
    // 如果 month 按 javascript 的定义从 0 开始的话就是
    // return new Date(year, month + 1, 0).getDate()

    var tempDays = [];
    for (let i = 1; i <= dayCount; i++) {
        i = zeroPadding(i);
        tempDays.push(i)
    }
    return tempDays;
}

// 自动补零
function zeroPadding(i) {
    return ('0' + i).slice(-2);
    // return (Array(2).join(0) + i).slice(-2);
}

//获取当前年月日时分秒
function getNowDate(date) {
    nowYear = date.getFullYear();
    nowMonth = date.getMonth() + 1;
    nowDay = date.getDate();
    nowHour = date.getHours();
    nowMinute = date.getMinutes();
    nowSecond = date.getSeconds();
}

// 反向传值
function nav_back(that) {
    var rewriteTime = that.data.year + "-" + that.data.month + "-" + that.data.day + " " + that.data.hour + ":" + that.data.minute + ":" + that.data.second;

    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1];   //当前页面
    var prevPage = pages[pages.length - 2];  //上一个页面

    //直接调用上一个页面的setData()方法，把数据存到上一个页面中去
    if (cellId == "21") {
        prevPage.setData({
            starTime: rewriteTime
        })
    } else if (cellId == "22") {
        prevPage.setData({
            endTime: rewriteTime
        })
    }
    console.log(prevPage.data);
    var a = ''
}