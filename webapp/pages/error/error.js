// pages/error/error.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    image: '../../images/network-failed.png',
    title: '网页无法打开...',
    tip: "",
    button: '返回首页'
  },


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
    
    },
    goback: function () {
        wx.reLaunch({
            url: '../index/index'
        })
    },
})