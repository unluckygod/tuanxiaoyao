// pages/mygroupdetail/mygroupdetail.js
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        network:false,
        assistant:true,
        select: 0,
        num: 1,
        obj:null
    },

    /**
     * 生命周期函数--监听页面加载
     */
    // onLoad: function (options) {
    //     this.getInfo(options.group_id);
    // },
    
    onShow:function(){
		var options = this.options;
		this.getInfo(options.group_id);
        this.setData({
            userInfo: app.globalData.userInfo
        });
    },

    getInfo:function(group_id){
        var that = this;
        var data = {
            group_id: group_id
        };
        app.common.apiRequest('api/group/info',data,function(res){
            var arr = [];
            for(let i = 0;i < res.data.info.num; i++){
                arr.push(i+1);
            }
            that.setData({
                network:true,
                data:res.data,
                arr: arr
            });
            var info_timer = setInterval(function () {
                that.countDown();
            }, 1000);
            var other_timer = setInterval(function () {
                that.countDown();
            }, 1000);
        })
    },
    //点击开团-弹出规格
    buy: function (e) {
        this.setData({
            assistant: false,
            group_id:e.currentTarget.id
        });
    },

    //关闭规格
    closed: function () {
        this.setData({
            assistant: true
        });
    },

    //点击确定
    makesure: function () {
        //判断是否为多规格
        var flag = true;
        if (this.data.data.plans.length != 1) {
            if (this.data.select == null) {
                flag = false;
                wx.showToast({
                    title: '请选择规格',
                    icon: 'none'
                });
            }else{
                that.setData({
                    select:0
                });
            }
        }
        if (flag) {
            //判断用户是否登录
            if (!this.data.userInfo) {
                wx.navigateTo({
                    url: '../unAuth/unAuth',
                })
                return;
            }
            //关闭弹框
            this.setData({
                assistant: true
            });
            //整理确定订单页面参数
            var param = {
                image: this.data.data.goods.image,
                name: this.data.data.goods.name,
                goods_id: this.data.data.goods.id,
                goods_info_id: this.data.data.plans[this.data.select].goods_info_id,
                assistant: this.data.data.plans[this.data.select].assistant,
                group_price: this.data.data.plans[this.data.select].group_price,
                leader_price: this.data.data.plans[this.data.select].leader_price,
                num: this.data.num,
                activity_id: this.data.data.activity.id,
                group_id:this.data.group_id,
                type: this.data.data.activity.type,
                is_support_coupon: this.data.data.activity.is_support_coupon,
                cut_money: this.data.data.activity.cut_money,
                postage_price: this.data.data.goods.postage_price
            };
            param = JSON.stringify(param);
            wx.navigateTo({
                url: '../submitorder/submitorder?param=' + param,
            })
        }
    },

    //选择规格
    select: function (e) {
        this.setData({
            select: e.currentTarget.id
        });
        var num = this.data.num;
        //判断购买数量是否超出当前选择的规格库存
        //获取该活动是否设置限购数量
        var limit = this.data.data.activity.limit;
        if (limit == 0) {
            if (num > this.data.data.plans[this.data.select].stock) {
                this.setData({
                    num: this.data.data.plans[this.data.select].stock
                });
            }
        } else {
            if (num > limit) {
                this.setData({
                    num: limit
                });
            }
        }
    },
    //购买数量加
    numJiaTap: function () {
        var num = this.data.num;
        //获取该活动是否设置限购数量
        var limit = this.data.data.activity.limit;
        if (limit == 0) {
            //多规格只判断是否选中的规格是否超过库存,若没有选中规格则不做判断
            if (this.data.data.plans.length == 1) {
                if ((num + 1) > this.data.data.plans[0].stock) {
                    wx.showToast({
                        title: '库存不足',
                        icon: 'none'
                    });
                } else {
                    ++num;
                }
            } else {
                if (this.data.select != null) {
                    if ((num + 1) > this.data.data.plans[this.data.select].stock) {
                        wx.showToast({
                            title: '库存不足',
                            icon: 'none'
                        });
                    }
                } else {
                    ++num;;
                }
            }
        } else {
            if (num + 1 > limit) {
                wx.showToast({
                    title: '每个用户限购' + limit + '件',
                    icon: 'none'
                });
            } else {
                ++num;
            }
        }
        this.setData({
            num: num
        });
    },
    //购买数量减
    numJianTap: function () {
        var num = this.data.num;
        if (num == 1) {
            wx.showToast({
                title: '不能再减了',
                icon: 'none'
            });
        } else {
            --num;
        }
        this.setData({
            num: num
        });
    },
    timeFormat(param) { //小于10的格式化函数
        return param < 10 ? '0' + param : param;
    },
    countDown() { //倒计时函数
        // 获取当前时间，同时得到活动结束时间数组
        let newTime = new Date().getTime();
        var data = this.data.data;
        let expired = data.info.expired;
        // 对结束时间进行处理渲染到页面
        let arr = expired.split(/[- :]/);
        let nndate = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
        nndate = Date.parse(nndate);
        let endTime = nndate;
        let obj = null;
        // 如果活动未结束，对时间进行处理
        if (endTime - newTime > 0) {
            let time = (endTime - newTime) / 1000;
            // 获取天、时、分、秒
            let day = parseInt(time / (60 * 60 * 24));
            let hou = parseInt(time % (60 * 60 * 24) / 3600);
            let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
            let sec = parseInt(time % (60 * 60 * 24) % 3600 % 60);
            obj = {
                day: this.timeFormat(day),
                hou: this.timeFormat(hou),
                min: this.timeFormat(min),
                sec: this.timeFormat(sec)
            }
        } else { //活动已结束，全部设置为'00'
            obj = {
                day: '00',
                hou: '00',
                min: '00',
                sec: '00'
            }
            //关闭循环执行函数
            clearInterval(info_timer);
            //不显示倒计时
            data.info.status = 2;
            this.setData({
                data: data
            });
        }
        // 渲染，然后每隔一秒执行一次倒计时函数
        this.setData({
            obj: obj
        })
        setTimeout(this.countDown, 1000);
    },

    countOtherDown() { //倒计时函数
        // 获取当前时间，同时得到活动结束时间数组
        let newTime = new Date().getTime();
        let endTimeList = this.data.data.other;
        let countDownArr = [];
        // 对结束时间进行处理渲染到页面
        endTimeList.forEach((o, index) => {
            //计算时间戳方式二，（兼容ios和android）
            let arr = o.expired.split(/[- :]/);
            let nndate = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
            nndate = Date.parse(nndate)
            let endTime = nndate;
            let obj = null;
            // 如果活动未结束，对时间进行处理
            if (endTime - newTime > 0) {
                let time = (endTime - newTime) / 1000;
                // 获取天、时、分、秒
                let day = parseInt(time / (60 * 60 * 24));
                let hou = parseInt(time % (60 * 60 * 24) / 3600);
                let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
                let sec = parseInt(time % (60 * 60 * 24) % 3600 % 60);
                obj = {
                    day: this.timeFormat(day),
                    hou: this.timeFormat(hou),
                    min: this.timeFormat(min),
                    sec: this.timeFormat(sec)
                }
            } else { //活动已结束，全部设置为'00'
                obj = {
                    day: '00',
                    hou: '00',
                    min: '00',
                    sec: '00'
                }
                //剔除掉已过期的团
                var data = this.data.data;
                data.other.splice(index, 1);
                if(data.other.length == 0){
                    //关闭循环执行函数
                    clearInterval(other_timer);
                }
                this.setData({
                    data: data
                });
            }
            countDownArr.push(obj);
        })
        // 渲染，然后每隔一秒执行一次倒计时函数
        this.setData({
            countDownList: countDownArr
        });
        setTimeout(this.countOtherDown, 1000);
    },
})