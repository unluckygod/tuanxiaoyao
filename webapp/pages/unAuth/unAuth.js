// pages/mine/mine.js
const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        hidden: true,
        time: '发送验证码',
        showTime: false,
        loading: false,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        that.setData({
            userInfo: app.globalData.userInfo,
            openid: app.globalData.openid
        });
    },

    formSubmit: function (e) {
        var that = this;
        that.setData({
            loading: true
        });
        /*表单数据验证 */
        var result = that.validate(e);
        if (!result.status) {
            wx.showToast({
                title: result.msg,
                icon: 'none',
                complete: function (res) {
                    that.setData({
                        loading: false
                    });
                }
            });
            return;
        }
        /*登录 */
        app.common.apiRequest('api/member/addUser', e.detail.value, function (res) {
            if (res.code != '1') {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
                that.setData({
                    loading: false,
                });
            } else {
                wx.showToast({
                    title: '登录成功',
                    icon: 'none',
                    complete: function () {
                        setTimeout(function(){
                            app.globalData.userInfo = res.data;
                            wx.navigateBack({})
                        },1500);
                    }
                })
            }
        });

    },

    /**
     * 验证数据
     */
    validate: function (e) {
        var that = this;
        var data = e.detail.value;
        var result = {
            status: true,
            msg: ''
        };
        var param = {};
        //数据验证
        var temp = {
            mobile: {
                key: data.mobile,
                reg: [/\S/, /^1[345789]\d{9}$/],
                msg: ['请填写手机号', '手机号格式错误'],
            },
            code: {
                key: data.code,
                reg: [/\S/, /\d{6}/],
                msg: ['请填写验证码', '验证码格式错误'],
            }

        }
        param = Object.assign(param, temp);
        for (var i in param) {
            for (var j in param[i].reg) {
                if (!param[i].reg[j].test(param[i].key)) {
                    result.status = false;
                    result.msg = param[i].msg[j];
                    return result;
                }
            }
        }
        return result;
    },

    //授权回调
    getAuth: function (e) {
        var that = this;
        wx.getSetting({
            complete: function (res) {
                if (res.authSetting['scope.userInfo'] == true) {
                    that.setData({
                        wxInfo:e.detail.userInfo,
                        hidden: false
                    });
                    app.globalData.wxInfo = e.detail.userInfo;
                }
            }
        });
    },

    //关闭弹框
    closeTips: function () {
        var that = this;
        that.setData({
            phone: '',
            code: '',
            time: '获取验证码',
            hidden: true
        });
    },

    /*动态获取输入的手机号 */
    getPhone: function (e) {
        var that = this;
        var phone = e.detail.value;
        that.setData({
            phone: phone,
        });
    },
    /*动态获取输入的验证码 */
    getCode: function (e) {
        var that = this;
        var code = e.detail.value;
        that.setData({
            code: code,
        });
    },
    /*发送短信验证码 */
    sendCode: function () {
        var that = this;
        var phone = that.data.phone;
        app.common.apiRequest('api/sms/sendSMSCode', { phone: phone }, function (res) {
            if (res.code = '1') {
                var time = 120;
                that.setData({
                    showTime: true
                });
                that.resetTime(time);
            } else {
                wx.showToast({
                    title: '发送失败',
                    icon: 'none'
                });
            }
        });
    },
    /*60秒倒计时*/
    resetTime: function (time) {
        var that = this;
        if (time > 1) {
            time--;
            var content = time + '(S)';
            that.setData({
                time: content,
            });
            setTimeout(function () { that.resetTime(time) }, 1000);
        } else {
            that.setData({
                time: '重新发送',
                showTime: false
            });
        }
    },
    //绑定手机号回调
    getPhoneNumber: function (e) {
        var that = this;
        if (e.detail.errMsg.indexOf("getPhoneNumber:ok") >= 0) {
            var wxInfo = that.data.wxInfo;
            var data = {
                encryptedData: e.detail.encryptedData,
                iv: e.detail.iv,
                openid:app.globalData.openid,
                nickname:wxInfo.nickName,
                wxlogo: wxInfo.avatarUrl,
                gender: wxInfo.gender == 1 ? '男' : '女',
                area: (wxInfo.province + wxInfo.city)
            }
            //检测登录是否过期
            wx.checkSession({
                complete: function (e) {
                    if (e.errMsg.indexOf('checkSession:ok') >= 0) {
                        data.sessionKey = wx.getStorageSync('session_key');
                    } else {
                        app.common.login()
                            .then(function (res) {
                                data.sessionKey = res.data.session_key;
                            });
                    }
                    app.common.apiRequest('api/member/editPhone', data, function (res) {
                        if (res.code == 1) {
                            app.globalData.userInfo = res.data;
                            wx.showToast({
                                title: '绑定成功',
                                icon: 'none',
                                success:function(){
                                    setTimeout(function(){
                                        wx.navigateBack({});
                                    },1500)
                                }
                            })
                        } else {
                            wx.showToast({
                                title:'绑定失败',
                                icon:'none',
                            })
                        }
                    })
                }
            })

        }
    },
})