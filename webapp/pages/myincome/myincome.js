// pages/store/store.js
const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        cash:0,
        valid:0,
        list: [],
        network: false,
        loading: false,
        page: 1,
        loadingMore: true
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        var that = this;
        that.setData({
            cash: 0,
            valid: 0,
            list: [],
            network: false,
            loading: false,
            page: 1,
            loadingMore: true
        });
        that.getCommission();
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        var that = this;
        that.setData({
            cash: 0,
            valid: 0,
            list: [],
            network: false,
            loading: false,
            page: 1,
            loadingMore: true
        });
        that.getCommission();
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function (e) {
        var that = this;
        /*判断是否有更多数据 */
        if (!that.data.loadingMore) {
            return;
        }
        /*判断是否加载完成*/
        if (that.data.loading) {
            return;
        }
        that.getCommission();
    },

    /**
     * 获取数据
     */
    getCommission: function () {
        var that = this;
        var data = {
            user_id: app.globalData.userInfo.id,
        }
        app.common.apiRequest('api/Commission/index', data, function (res) {
            that.setData({
                network: true
            });
            var loadingMore = true;
            if (res.data.list.length < 2) {
                loadingMore = false;
            }
            that.setData({
                cash: res.data.cash,
                valid: res.data.valid,
                list: res.data.list,
                loading: false,
                network: true,
                page: 2,
                loadingMore: loadingMore
            });
        });
    },

    /**
     * 加载更多
     */
    getMore: function () {
        var that = this;
        var data = {
            user_id: app.globalData.userInfo.id,
            page: that.data.page,
            show_num:2
        }
        var list = that.data.list;
        that.setData({
            loading:true
        });
        app.common.apiRequest('api/Commission/list', data, function (res) {
            var loadingMore = true;
            if (res.data.length < 2) {
                loadingMore = false;
            }
            list = list.concat(res.data);
            that.setData({
                list: list,
                loading: false,
                page: ++data.page,
                loadingMore: loadingMore
            });
        });
    },
})