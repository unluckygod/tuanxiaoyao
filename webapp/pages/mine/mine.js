// pages/mine/mine.js
const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
		num:[0,0,0,0],
        network:false,
        hidden: true,
        time: '发送验证码',
        showTime: false,
        loading: false,
        order:[
            {
                status:1,
                image:"../../images/mine-order-icon01.png",
                name:'待付款'
            },
            {
                status: 2,
                image: "../../images/mine-order-icon02.png",
                name: '待发货'
            },
            {
                status: 3,
                image: "../../images/mine-order-icon03.png",
                name: '待收货'
            },
            {
                status: 4,
                image: "../../images/mine-order-icon04.png",
                name: '待评论'
            }
        ],
        tools:[
            {
                url:"../mygroup/mygroup",
                image:"../../images/mine-icon01.png",
                name:"我的拼团"
            },
            {
                url: "../mycoupons/mycoupons",
                image: "../../images/mine-icon02.png",
                name: "优惠券"
            },
            {
                url: "../mycollection/mycollection",
                image: "../../images/mine-icon03.png",
                name: "我的收藏"
            },
            {
                url: "../mycomments/mycomments",
                image: "../../images/mine-icon04.png",
                name: "我的评论"
            },
            {
                url: "../addresslist/addresslist",
                image: "../../images/mine-icon05.png",
                name: "地址管理"
            }
        ],
        superLeaders:[
            {
                url: "../buildgroup/buildgroup",
                image: "../../images/tuan-icon01.png",
                name: "建团"
            },
            {
                url: "../mybuiltgroup/mybuiltgroup",
                image: "../../images/tuan-icon02.png",
                name: "我建的团"
            },
            {
                url: "../myincome/myincome",
                image: "../../images/tuan-icon03.png",
                name: "我的收益"
            }
        ]
        
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        
    },

	//更新资料
	refresh:function(){
		var that = this;
		var userInfo = that.data.userInfo;
		var data = {
			openid: userInfo.openid
		}
		app.common.apiRequest('api/member/getUserInfo',data,function(res){
			app.globalData.userInfo = res.data;
			that.setData({
				userInfo:res.data
			});
			wx.showToast({
				title:'资料更新成功',
				icon:'none',
			});
		});
	},
    onShow:function(){
        var that = this;
        that.setData({
            userInfo: app.globalData.userInfo,
            wxInfo: app.globalData.wxInfo,
            openid: app.globalData.openid,
            network: false,
			num:[0,0,0,0]
        });
		if(app.globalData.userInfo){
			that.getOrderCount();
		}else{
			that.setData({
				network:true
			});
		}
    },
	//获取用户对应订单状态下的数量
	getOrderCount:function(){
		var that = this;
		var data = {
			user_id: app.globalData.userInfo.id
		};
		app.common.apiRequest('api/order/statisticsOrder',data,function(res){
			that.setData({
				num:res.data,
				network:true
			});
		})
	},

    formSubmit: function (e) {
        var that = this;
        that.setData({
            loading: true
        });
        /*表单数据验证 */
        var result = that.validate(e);
        if (!result.status) {
            wx.showToast({
                title: result.msg,
                icon: 'none',
                complete: function (res) {
                    that.setData({
                        loading: false
                    });
                }
            });
            return;
        }
        /*登录 */
        app.common.apiRequest('api/member/addUser', e.detail.value, function (res) {
            if (res.code != '1') {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
                that.setData({
                    loading: false,
                });
            } else {
                wx.showToast({
                    title: '登录成功',
                    icon: 'none',
                    complete: function () {
                        that.setData({
                            userInfo: res.data,
                            loading: false,
                            hidden: true
                        });
                        app.globalData.userInfo = res.data;
                    }
                })
            }
        });

    },

    /**
     * 验证数据
     */
    validate: function (e) {
        var that = this;
        var data = e.detail.value;
        var result = {
            status: true,
            msg: ''
        };
        var param = {};
        //数据验证
        var temp = {
            mobile: {
                key: data.mobile,
                reg: [/\S/, /^1[345789]\d{9}$/],
                msg: ['请填写手机号', '手机号格式错误'],
            },
            code: {
                key: data.code,
                reg: [/\S/, /\d{6}/],
                msg: ['请填写验证码', '验证码格式错误'],
            }

        }
        param = Object.assign(param, temp);
        for (var i in param) {
            for (var j in param[i].reg) {
                if (!param[i].reg[j].test(param[i].key)) {
                    result.status = false;
                    result.msg = param[i].msg[j];
                    return result;
                }
            }
        }
        return result;
    },

    //授权回调
    getAuth: function (e) {
        console.log(e);
        var that = this;
        wx.getSetting({
            complete: function (res) {
                if (res.authSetting['scope.userInfo'] == true) {
                    that.setData({
                        wxInfo: e.detail.userInfo,
                        hidden: false
                    });
                    app.globalData.wxInfo = e.detail.userInfo;
                }
            }
        });
    },

    //关闭弹框
    closeTips: function () {
        var that = this;
        that.setData({
            phone: '',
            code: '',
            time: '获取验证码',
            hidden: true
        });
    },

    /*动态获取输入的手机号 */
    getPhone: function (e) {
        var that = this;
        var phone = e.detail.value;
        that.setData({
            phone: phone,
        });
    },
    /*动态获取输入的验证码 */
    getCode: function (e) {
        var that = this;
        var code = e.detail.value;
        that.setData({
            code: code,
        });
    },
    /*发送短信验证码 */
    sendCode: function () {
        var that = this;
        var phone = that.data.phone;
        app.common.apiRequest('api/sms/sendSMSCode', { phone: phone }, function (res) {
            if (res.code = '1') {
                var time = 120;
                that.setData({
                    showTime: true
                });
                that.resetTime(time);
            } else {
                wx.showToast({
                    title: '发送失败',
                    icon: 'none'
                });
            }
        });
    },
    /*60秒倒计时*/
    resetTime: function (time) {
        var that = this;
        if (time > 1) {
            time--;
            var content = time + '(S)';
            that.setData({
                time: content,
            });
            setTimeout(function () { that.resetTime(time) }, 1000);
        } else {
            that.setData({
                time: '重新发送',
                showTime: false
            });
        }
    },

    //全部订单
    myorder:function(){
        if(this.checkLogin()){
            wx.navigateTo({
                url: '../myorder/myorder',
            })
        }else{
            wx.navigateTo({
                url: '../unAuth/unAuth',
            })
            // wx.showModal({
            //     title: '提示',
            //     content: '请登录后再操作',
            //     showCancel:false
            // })
        }
    },

    //订单跳转
    navigateToOrder: function (e) {
        var order = this.data.order;
        var index = e.currentTarget.id;
        if (this.checkLogin()) {
            wx.navigateTo({
                url: '../myorder/myorder?status=' + order[index].status,
            })
        } else {
            wx.navigateTo({
                url: '../unAuth/unAuth',
            })
            // wx.showModal({
            //     title: '提示',
            //     content: '请登录后再操作',
            //     showCancel: false
            // })
        }
    },

    //工具栏跳转
    navigateToTool: function (e) {
        var tools = this.data.tools;
        var index = e.currentTarget.id;
        if (this.checkLogin()) {
            wx.navigateTo({
                url: tools[index].url,
            })
        } else {
            wx.navigateTo({
                url: '../unAuth/unAuth',
            })
            // wx.showModal({
            //     title: '提示',
            //     content: '请登录后再操作',
            //     showCancel: false
            // })
        }
    },


    //验证登录
    checkLogin:function(){
        return app.globalData.userInfo;
    }
})