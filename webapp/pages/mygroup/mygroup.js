// pages/mygroup/mygroup.js
const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        navtab: ['我发起的拼团', '我参与的拼团'],
        navIndex: 0,
        page: [1, 1],
        loading: false,
        loadingMore: [true, true],
        list: [[], []],
        network: false
    },
    //一级导航
    navtab: function (e) {
        var that = this;
        if (that.data.page[e.currentTarget.id] == 1) {
            /*加载数据 */
            that.getGroups(e.currentTarget.id);
        }
        that.setData({
            sliderOffset: e.currentTarget.offsetLeft,
            navIndex: e.currentTarget.id
        });
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        /*加载数据 */
        this.getGroups(0);
    },

    /**
     * 获取订单
     */
    getGroups: function (status) {
        var that = this;
        var list = that.data.list;
        var loadingMore = that.data.loadingMore;
        var page = that.data.page;
        var data = {
            user_id: app.globalData.userInfo.id,
            type: status,
            page: page[status],
            show_num: 8
        };
        that.setData({
            loading: true
        });
        app.common.apiRequest('api/group/list', data, function (res) {
            that.setData({
                network: true
            });
            list[status] = list[status].concat(res.data);
            ++page[status];
            if (res.data.length < 8) {
                loadingMore[status] = false;
            }
            that.setData({
                list: list,
                loading: false,
                page: page,
                loadingMore: loadingMore
            });
        })
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        var that = this;
        that.setData({
            navIndex: 0,
            sliderOffset: 0,
            sliderLeft: 0,
            page: [1, 1],
            loading: false,
            loadingMore: [true, true],
            list: [[], []],
        });
        /*加载数据 */
        that.getGroups(0);
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function (e) {
        var that = this;
        var index = that.data.navIndex;
        /*判断是否有更多数据 */
        if (!that.data.loadingMore[index]) {
            return;
        }
        /*判断是否加载完成*/
        if (that.data.loading) {
            return;
        }
        /*加载数据 */
        that.getGroups(index);
    },
})