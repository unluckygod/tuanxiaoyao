// pages/mybuiltgroup/mybuiltgroup.js
const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        navtab: ['全部', '进行中', '已完成'],
        activeIndex: 0,
        sliderOffset: 0,
        sliderLeft: 0,
        page: [1, 1, 1],
        loading: false,
        loadingMore: [true, true, true],
        list: [[], [], []],
        network: false
    },
    /**
     * tab导航切换
     */
    tabClick: function (e) {
        var that = this;
        if (that.data.page[e.currentTarget.id] == 1) {
            /*加载数据 */
            that.getOrders(e.currentTarget.id);
        }
        that.setData({
            sliderOffset: e.currentTarget.offsetLeft,
            activeIndex: e.currentTarget.id
        });
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function () {
        var that = this;
        var activeIndex = that.data.activeIndex;
        /*加载数据 */
        that.getOrders(activeIndex);
    },
    /**
     * 获取建团列表
     */
    getOrders: function (status) {
        var that = this;
        var list = that.data.list;
        var loadingMore = that.data.loadingMore;
        var page = that.data.page;
        var num = that.data.num;
        var data = {
            user_id: app.globalData.userInfo.id,
            status: status,
            page: page[status],
            show_num: 5
        };
        that.setData({
            loading: true
        });
        app.common.apiRequest('api/group/colonelList', data, function (res) {
            that.setData({
                network: true
            });
            var result = res.data;
            for (var i in result){
                result[i].arr = [];
                for(var k=0;k<res.data[i].num;k++){
                    result[i].arr.push(1);
                }
            }
            list[status] = list[status].concat(result);
            //显示开团信息则对时间进行处理
            that.handleTime(list);
            ++page[status];
            if (res.data.length < 5) {
                loadingMore[status] = false;
            }
            that.setData({
                list: list,
                loading: false,
                page: page,
                num:num,
                loadingMore: loadingMore
            });
        })
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        var that = this;
        that.setData({
            activeIndex: 0,
            sliderOffset: 0,
            sliderLeft: 0,
            page: [1, 1, 1],
            loading: false,
            loadingMore: [true, true, true],
            list: [[], [], []],
            num:[[],[],[]]
        });
        /*加载数据 */
        that.getOrders(0);
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function (e) {
        var that = this;
        var index = that.data.activeIndex;
        /*判断是否有更多数据 */
        if (!that.data.loadingMore[index]) {
            return;
        }
        /*判断是否加载完成*/
        if (that.data.loading) {
            return;
        }
        /*加载数据 */
        that.getOrders(index);
    },

    //时间处理
    handleTime(end_times) {
        let endTimeList = [];
        let actStatus = [];
        // 将活动的结束时间参数提成一个单独的数组，方便操作,活动状态都设置进行中状态
        end_times.forEach((first,index) => {
            endTimeList[index] = [];
            actStatus[index] = [];
            first.forEach((sub,subindex) => {
                endTimeList[index].push(sub.expired);
                actStatus[index].push(true);
            })
        })
        this.setData({
            actEndTimeList: endTimeList,
            actStatus: actStatus
        });
        // 执行倒计时函数
        this.countDown();
    },
    timeFormat(param) { //小于10的格式化函数
        return param < 10 ? '0' + param : param;
    },
    countDown() { //倒计时函数
        // 获取当前时间，同时得到活动结束时间数组
        let newTime = new Date().getTime();
        let endTimeList = this.data.actEndTimeList;
        let countDownArr = [];
        let actStatus = this.data.actStatus;
        // 对结束时间进行处理渲染到页面
        endTimeList.forEach((o, index) => {
            countDownArr[index] = [];
            o.forEach((sub) =>{
                //计算时间戳方式二，（兼容ios和android）
                let arr = sub.split(/[- :]/);
                let nndate = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                nndate = Date.parse(nndate)
                let endTime = nndate;
                let obj = null;
                // 如果活动未结束，对时间进行处理
                if (endTime - newTime > 0) {
                    let time = (endTime - newTime) / 1000;
                    // 获取天、时、分、秒
                    let day = parseInt(time / (60 * 60 * 24));
                    let hou = parseInt(time % (60 * 60 * 24) / 3600);
                    let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
                    let sec = parseInt(time % (60 * 60 * 24) % 3600 % 60);
                    obj = {
                        day: this.timeFormat(day),
                        hou: this.timeFormat(hou),
                        min: this.timeFormat(min),
                        sec: this.timeFormat(sec)
                    }
                } else { //活动已结束，全部设置为'00'
                    obj = {
                        day: '00',
                        hou: '00',
                        min: '00',
                        sec: '00'
                    }
                }
                countDownArr[index].push(obj);
            });    
        })
        // 渲染，然后每隔一秒执行一次倒计时函数
        this.setData({
            countDownList: countDownArr,
            actStatus: actStatus
        })
        setTimeout(this.countDown, 1000);
    },
})