// pages/store/store.js
const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
	data: {
		collects: [],
		network: false
	},

    /**
     * 生命周期函数--监听页面加载
     */
	onLoad: function (options) {
		var that = this;
		that.getLogistical(options.order_id);
	},

    /**
     * 获取物流信息
     */
	getLogistical: function (order_id) {
		var that = this;
		var data = {
			order_id: order_id
		}
		app.common.apiRequest('api/index/logistical', data, function (res) {
			that.setData({
				collects: res.data,
				network: true
			});
		});
	},
})