//index.js
//获取应用实例
const app = getApp()

Page({
    data: {
        imgUrls: [
        '../../uploadimg/banner.jpg',
        '../../uploadimg/banner.jpg',
        '../../uploadimg/banner.jpg'
        ],
        network:false,
        scrollTop:0,
        coupon:null,
        show:true,
        loading:false,
        loadingMore:true,
        page:1,
        tab:['综合','价格','全部'],
        tabindex:0,
        default:0,
        timeindex:0
    },
    
    onLoad: function () {
        var that = this;
        app.common.apiRequest('api/index/index','',function(res){
            that.setData({
                network:true,
                list:  res.data,
                activities: res.data.normal_group,
                loadingMore: res.data.normal_group == 5?true:false,
                timeindex: res.data.secret_index,
				swiper_index: res.data.secret_index,
            });
            //判断当前哪个时间段正在进行限时购
            if(res.data.secret_group.length){
                setInterval(function () {
                    that.time_range();
                }, 1000);
            }
        });
        //检测是否为新用户
        app.userInfoReadyCallback = res => {
            that.getNewGift();
        };
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        
    },
	//跳转至神秘团详情页
	navigate:function(e){
		var activity_id = e.currentTarget.id;
		wx.navigateTo({
			url: '../productdetail/productdetail?id=' + activity_id,
		});
	},

    //tab切换时间
    tab:function(e){
        this.setData({
            timeindex:e.currentTarget.id,
            swiper_index: e.currentTarget.id
        });
    },
    change:function(e){
        this.setData({
            timeindex: e.detail.current,
            swiper_index: e.detail.current
        });
    },

    //获取新人礼数据
    getNewGift:function(){
        var that = this;
        var data = {
            user_id: app.globalData.userInfo?app.globalData.userInfo.id:0
        }
        app.common.apiRequest('api/coupon/index',data,function(e){
            that.setData({
                coupon:e.data
            });
        })
    },
    //监听页面滚动事件
    onPageScroll:function(e){
        var that = this;
        that.setData({
            scrollTop: e.scrollTop
        });
    },
    //返回顶部
    gotop:function(){
        if (wx.pageScrollTo) {
            wx.pageScrollTo({
                scrollTop: 0
            });
        } else {
            wx.showModal({
                title: '提示',
                content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
            })
        }
    },

    /**
     * 获取拼团数据
     */
    getActivity: function () {
        var that = this;
        var activities = that.data.activities;
        that.setData({
            loading: true
        });
        var index = that.data.tabindex;
        var sort = 0;
        if(index == 1){
            sort = that.data.default;
        }else if(index == 2){
            sort = 3;
        }
        var page = that.data.page;
        var data = {
            page: page,
            show_num: 5,
            type: 1,
            sort:sort
        };
        app.common.apiRequest('api/activity/index', data, function (res) {
            ++page;
            activities = activities.concat(res.data);
            var loadingMore = true;
            if (res.data.length < 5) {
                loadingMore = false;
            }
            that.setData({
                activities: activities,
                page: page,
                loadingMore: loadingMore,
                loading: false
            });
        })
    },

    //显示红包弹框
    showCoupon:function(){
        this.setData({
            show:true
        });
    },

    //关闭红包弹框
    closed:function(){
        this.setData({
            show:false
        });
    },

    //领取红包
    recevie:function(){
        var that = this;
        //判断是否登录
        if(app.globalData.userInfo){
            var data = {
                user_id: app.globalData.userInfo.id
            }
            app.common.apiRequest('api/coupon/recevie',data,function(res){
                if(res.code == 1){
                    var coupon = that.data.coupon;
                    coupon['total'] = 0;
                    that.setData({
                        show:false,
                        coupon:coupon
                    });
                    wx.showToast({
                        title: '领取成功',
                        icon: 'none'
                    });
                }else{
                    wx.showToast({
                        title: '领取失败',
                        icon: 'none'
                    });
                }
            })
        }else{
            that.setData({
                show: false
            });
            wx.showModal({
                title: '提示',
                content: '您尚未登录，请先登录',
                cancelText: '先看看',
                confirmText:'去登录',
                complete:function(e){
                    if(e.confirm){
                        wx.navigateTo({
                            url: '../unAuth/unAuth',
                        })
                        // wx.switchTab({
                        //     url: '../mine/mine'
                        // })
                    }
                }
            })           
        }
    },
    //切换团选项
    tap:function(e){
        var that = this;
        var index = e.currentTarget.id;
        var tabindex = that.data.tabindex;
        var price = 0;
        if(index == 1){
            var price = that.data.default;
            if (price == 0){
                price = 1;
            }else if(price == 1){
                price = 2;
            }else{
                price = 1;
            }
            that.setData({
                loadingMore:true,
                tabindex: index,
                default: price,
                activities: [],
                page: 1,
            });
            that.getActivity();
        }else{
            if(tabindex != index){
                that.setData({
                    loadingMore: true,
                    tabindex: index,
                    activities:[],
                    page:1,
                    default:0,
                });
                that.getActivity();
            }
        }
    },
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        /*加载数据 */
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function (e) {
        var that = this;
        var index = that.data.tabindex;
        /*判断是否有更多数据 */
        if (!that.data.loadingMore) {
            return;
        }
        /*判断是否加载完成*/
        if (that.data.loading) {
            return;
        }
        /*加载数据 */
        that.getShops();
    },

    //判断当前哪个活动时间段正在进行限时购的活动
    time_range: function () {
        var that = this;
        var secret_group = that.data.list.secret_group;
        var i = secret_group.length;
        //status 活动状态 0已过期 1进行中 2即将开始
        var status = [];
        //设置活动状态全部已过期
        for(var k in secret_group){
            status.push(0);
        }
        //循环限时购的时间与当前时间比较 大于当前时间 设置为即将开始 小于当前时间设置进行中(跳出循环) 
        for(i;i>=1;i--){
            var secret_time = secret_group[i-1].time.split(":");
            var secret = new Date();
            var now = new Date();  
            secret.setHours(secret_time[0]);
            secret.setMinutes(secret_time[1]); 
            now.setHours(now.getHours());
            now.setMinutes(now.getMinutes());
            if (now.getTime() >= secret.getTime()) {
                status[i-1] = 1;
                break;
            } else if (now.getTime() < secret.getTime()){
                status[i-1] = 2;
            }
        }
        that.setData({
            status:status
        });    
    },
 
})
