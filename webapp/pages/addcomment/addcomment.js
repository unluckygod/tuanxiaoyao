const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        tempFilePaths: [],
        file:[],
        loading: false,
        start:[
            [1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1]
        ],
        level:[0,0,0,]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        console.log(JSON.stringify(this.data.image));
        this.setData({
            options: options,
            userInfo: app.globalData.userInfo
        });
    },
    score: function (e) {
        var that = this;
        var index = e.currentTarget.dataset.index;
        var parent = e.currentTarget.dataset.parent;
        var level = that.data.level;
        level[parent] = index;
        that.setData({
            level: level
        });
    },
    //图片上传本地  
    uploadImg: function (e) {
        var that = this;
        var tempFilePaths = that.data.tempFilePaths;
        var len = tempFilePaths.length;
        if (len >= 3) {
            wx.showToast({
                title:'最多上传3张图片',
                icon:'none',
                duration:2500
            })
            return false;
        }
        wx.chooseImage({
            success: function (res) {
                if((len + res.tempFilePaths.length) > 3){
                    wx.showToast({
                        title: '最多上传3张图片',
                        icon: 'none',
                        duration: 2500
                    })
                    return false;
                }
                var file = that.data.tempFilePaths;
                for (var i = 0; i < res.tempFilePaths.length; i++) {
                    file.push(res.tempFilePaths[i]);
                }
                // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片  
                that.setData({
                    tempFilePaths: file
                })
            }
        })
    },

    /**
     * 图片删除  
     */
    del: function (e) {
        var that = this;
        var index = e.currentTarget.dataset.index;
        var tempFilePaths = that.data.tempFilePaths;
        tempFilePaths.splice(index, 1);
        that.setData({
            tempFilePaths: tempFilePaths
        });
    },
    formSubmit: function (e) {
        var that = this, data;
        /*表单数据验证 */
        var result = that.validate(e);
        if (!result.status) {
            wx.showToast({
                title: result.msg,
                icon: 'none'
            });
            return;
        }
        data = e.detail.value;
        that.setData({
            loading: true
        });
        wx.showLoading({
            title: '数据处理中……',
        })
        var tempFilePaths = that.data.tempFilePaths;
        if (tempFilePaths.length) {
            data.is_has_attach = 1;
            that.uploadimgs(tempFilePaths,0,data);
        }else{
            data.is_has_attach = 0;
            that.comment(data);
        }
    },
    //多张图片上传  
    uploadimgs: function (files, i,data) {
        var that = this,
            i = i ? i : 0;
        var baseUrl = app.common.baseUrl;
        var file = that.data.file;
        wx.uploadFile({
            url: baseUrl + 'api/comment/uploadFiles',
            filePath: files[i],
            name: 'image',
            complete: (res) => {
                var res = JSON.parse(res.data);
                if (res.code == 1) {
                    file.push(res.data);
                    i++;
                    if (i == files.length) { //当图片传完时，停止调用
                        var obj = {};  
                        for(var n in file){
                            obj[n] = file[n];
                        }
                        data.file = JSON.stringify(obj);
                        //data.file = obj;
                        console.log(data);
                        that.comment(data);
                    } else {//若图片还没有传完，则继续调用函数
                        that.setData({
                            file:file
                        });
                        that.uploadimgs(files, i, data);
                    }
                } else {
                    wx.hideLoading();
                    wx.showToast({
                        title: '图片上传失败',
                        icon: 'none',
                        duration: 2500
                    })
                    that.setData({
                        loading: false
                    });
                }
            }
        });
    },
    comment:function(data){
        var that = this;
        app.common.apiRequest('api/comment/comment', data, function (res) {
            if (res.code == 1) {
                wx.hideLoading();
                wx.showToast({
                    title: '评价成功',
                    icon: 'none',
                    duration: 2500
                })
                setTimeout(function () {
                    wx.navigateBack({});
                }, 2500);
            } else {
                wx.hideLoading();
                wx.showToast({
                    title: res.msg,
                    icon: 'none',
                    duration: 2500
                })
                setTimeout(function () {
                    that.setData({
                        loading: false
                    });
                }, 2500);
            }
        })
    },
    /**
     * 验证数据
     */
    validate: function (e) {
        var that = this;
        var data = e.detail.value;
        var result = {
            status: true,
            msg: ''
        };
        var param = {};
        //数据验证
        var temp = {
            content: {
                key: data.content,
                reg: [/\S/],
                msg: ['请填写评价'],
            }
        }
        param = Object.assign(param, temp);
        for (var i in param) {
            for (var j in param[i].reg) {
                if (!param[i].reg[j].test(param[i].key)) {
                    result.status = false;
                    result.msg = param[i].msg[j];
                    return result;
                }
            }
        }
        return result;
    },
})