// pages/withdraw/withdraw.js
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        loading: false,
        money: '',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        that.setData({
            visiable_commission: options.visiable_commission
        });
    },
    //输入金额
    money: function (e) {
        var that = this;
        var visiable_commission = parseFloat(that.data.visiable_commission);
        var money = parseFloat(e.detail.value);
        if (money > visiable_commission) {
            wx.showToast({
                title: '提现金额已超出可用佣金',
                icon: 'none'
            })
            return ;
        }
        that.setData({
            money: money
        });
    },

    all:function(){
        var visiable_commission = parseFloat(this.data.visiable_commission);
        this.setData({
            money: visiable_commission
        });
    },

    //数据验证
    validate: function (e) {
        var that = this;
        var data = e.detail.value;
        var result = {
            status: true,
            msg: ''
        };
        //数据验证
        if (!/\S/.test(data.money)) {
            result.status = false;
            result.msg = '请输入提现金额';
            return result;
        }
        if (!/^[1-9]\d*$/.test(data.money)) {
            result.status = false;
            result.msg = '请输入正确的提现金额';
            return result;
        }
        var visiable_commission = parseFloat(that.data.visiable_commission);
        if (parseFloat(data.money) > visiable_commission) {
            result.status = false;
            result.msg = '提现金额超出可提现佣金';
            return result;
        }
        return result;
    },
    formSubmit: function (e) {
        var that = this;
        that.setData({
            loading:true
        });
        var result = that.validate(e);
        if (!result.status) {
            that.setData({
                loading: false
            }); 
            wx.showToast({
                title: result.msg,
                icon:'none',
            })
            return false;
        }
        var data = e.detail.value;
        data.user_id = app.globalData.userInfo.id;
        app.data.common.apiRequest('api/cash/index', data, function (res) {
            
            if (res.code != 1) {
                that.setData({
                    loading: false
                });
                wx.showToast({
                    title:res.msg,
                    icon:'none'
                })
            } else {
                wx.showToast({
                    title: '提交成功',
                    icon:'none',
                    success:function(){
                        setTimeout(function () {
                            that.setData({
                                network: false
                            });
                            wx.navigateBack({});
                        }, 1500);
                    }
                })
                
            }
        });
    }
})