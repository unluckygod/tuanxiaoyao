// pages/store/store.js
const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        network: false,
        loading:false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        that.getOrder(options.id);
    },

    /**
     * 获取商品数据
     */
    getOrder: function (order_id) {
        var that = this;
        var data = {
            order_id: order_id
        }
        app.common.apiRequest('api/order/orderInfo', data, function (res) {
            var arr = [];
            for(let i=0;i<res.data.group_num;i++){
                arr.push(1);
            }
            that.setData({
                network: true,
                order: res.data,
                arr:arr
            });
        });
    },

    /**
     * 拨打电话
     */
    call: function () {
        var that = this;
        var phone = that.data.order.mobile;
        if (phone == '') {
            that.showMsg('商家暂未开放联系方式');
            setTimeout(function () {
                that.hideMsg();
                return false;
            }, 2500)
        }
        wx.makePhoneCall({
            phoneNumber: phone,
        });
    },

    /**
     * 支付
     */
    pay: function (e) {
        var that = this;
        that.setData({
            loading:true
        });
        var data = {
            order_num: e.currentTarget.dataset.num
        }
        app.common.apiRequest('api/WX/prePay', data, function (result) {
            if (result.code != '1') {
                that.setData({
                    loading: false,
                });
                wx.showToast({
                    title: result.msg,
                    icon: 'none'
                });
                return;
            }
            /*唤起微信支付 */
            wx.requestPayment({
                timeStamp: result.data.timeStamp,
                nonceStr: result.data.nonceStr,
                package: result.data.package,
                signType: result.data.signType,
                paySign: result.data.paySign,
                complete: function (rs) {
                    that.setData({
                        loading: false
                    });
                    if (rs.errMsg == "requestPayment:fail") {
                        wx.showToast({
                            title: rs.err_desc,
                            icon: 'none'
                        });
                    } else if (rs.errMsg == "requestPayment:fail cancel") {

                    } else {
                        //支付成功刷新页面
                        that.refresh();
                    }
                }
            })
        })
    },

    /**
     * 取消订单
     */
    cancel: function () {
        var that = this;
        wx.showModal({
            title: '提示',
            content: '确认取消订单?',
            complete: function (res) {
                that.setData({
                    loading: true
                });
                var data = {
                    order_id: that.data.order.id
                }
                app.common.apiRequest('api/order/cancelOrder', data, function (result) {
                    if (result.code != '1') {
                        that.setData({
                            loading: false,
                        });
                        wx.showToast({
                            title: result.msg,
                            icon:'none'
                        });
                        return;
                    }
                    wx.showToast({
                        title: '提交成功',
                        icon: 'none',
                        duration: 1500,
                        success: function () {
                            setTimeout(function () {
                                //刷新页面
                                that.refresh();
                            }, 1500);
                        }
                    })
                    
                })
            }
        });
    },

    /**
     * 确认收货
     */
    makesure: function (e) {
        var that = this;
        that.setData({
            loading: true
        });
        var data = {
            order_id: that.data.order.id
        }
        app.common.apiRequest('api/order/makesure', data, function (result) {
            if (result.code != '1') {
                that.setData({
                    loading: false,
                });
                wx.showToast({
                    title: result.msg,
                    icon: 'none'
                });
                return;
            }
            //支付成功刷新页面
            that.refresh();
        })
    },

    refresh:function(){
        var that = this;
        var order_id = that.data.order.id;
        that.setData({
            network:false
        });
        that.getOrder(order_id);
    }
})