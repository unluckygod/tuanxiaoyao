// pages/store/store.js
const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        goods: [],
        network: false,
        loading: false,
        page: 1,
        loadingMore: true,
        select:null
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        that.getGoods();
    },

    chose:function(e){
        var that = this;
        var goods = that.data.goods;
        var index = e.currentTarget.id;
        app.event.emit('choseGoods',goods[index]);
        that.setData({
            select:index
        });
        setTimeout(function(){
            wx.navigateBack({})
        },500);
        
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        var that = this;
        that.setData({
            network: false,
            loadingMore:true,
            page: 1,
            goods: [],
        });
        that.getGoods();
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function (e) {
        var that = this;
        /*判断是否有更多数据 */
        if (!that.data.loadingMore) {
            return;
        }
        /*判断是否加载完成*/
        if (that.data.loading) {
            return;
        }
        that.getGoods();
    },

    /**
     * 获取商品
     */
    getGoods: function () {
        var that = this;
        var goods = that.data.goods;
        var page = that.data.page;
        var data = {
            page: page
        }
        that.setData({
            loading: true
        });
        app.common.apiRequest('api/goods/index', data, function (res) {
            that.setData({
                network: true
            });
            goods = goods.concat(res.data);
            var loadingMore = true;
            if (res.data.length < 10) {
                loadingMore = false;
            }
            that.setData({
                goods: goods,
                loading: false,
                page: ++page,
                loadingMore: loadingMore
            });
        });
    },
})