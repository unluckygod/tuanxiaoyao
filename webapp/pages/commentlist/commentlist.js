// pages/store/store.js
const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        comments: [[],[]],
        network: false,
        loading: false,
        page: [1,1],
        loadingMore: [true,true],
        tab:['全部','有图'],
        tabindex:0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        that.setData({
            goods_id: options.goods_id
        });
        that.getComments();
    },

    //切换
    tab:function(e){
        this.setData({
            tabindex: e.currentTarget.id
        });
        var page = this.data.page;
        if(page[e.currentTarget.id] == 1){
            this.getComments();
        }
        
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        var that = this;
        that.setData({
            network: false,
            page: [1,1],
            comments: [[],[]],
            loadingMore:[true,true],
            tabindex:0
        });
        that.getComments();
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function (e) {
        var that = this;
        /*判断是否有更多数据 */
        if (!that.data.loadingMore) {
            return;
        }
        /*判断是否加载完成*/
        if (that.data.loading) {
            return;
        }
        that.getComments();
    },

    /**
     * 获取评价
     */
    getComments: function () {
        var that = this;
        var comments = that.data.comments;
        var tabindex=  that.data.tabindex;
        var page = that.data.page;
        var data = {
            goods_id: that.data.goods_id,
            page: page[tabindex],
            has_attach:tabindex
        }
        that.setData({
            loading: true
        });
        app.common.apiRequest('api/comment/index', data, function (res) {
            that.setData({
                network: true
            });
            comments[tabindex] = comments[tabindex].concat(res.data);
            var loadingMore = that.data.loadingMore;
            if (res.data.length < 10) {
                loadingMore[tabindex] = false;
            }
            ++page[tabindex];
            that.setData({
                comments: comments,
                loading: false,
                page: page,
                loadingMore: loadingMore
            });
        });
    },
})