// pages/store/store.js
const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        comments: [],
        network: false,
        loading: false,
        page: 1,
        loadingMore: true
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        that.getComments();
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        var that = this;
        that.setData({
            network:false,
            page: 1,
            comments: [],
        });
        that.getComments();
        wx.stopPullDownRefresh();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function (e) {
        var that = this;
        /*判断是否有更多数据 */
        if (!that.data.loadingMore) {
            return;
        }
        /*判断是否加载完成*/
        if (that.data.loading) {
            return;
        }
        that.getComments();
    },

    /**
     * 获取评价
     */
    getComments: function () {
        var that = this;
        var comments = that.data.comments;
        var page = that.data.page;
        var data = {
            user_id: app.globalData.userInfo.id,
            page: page
        }
        that.setData({
            loading: true
        });
        app.common.apiRequest('api/comment/list', data, function (res) {
            that.setData({
                network: true
            });
            comments = comments.concat(res.data);
            var loadingMore = true;
            if (res.data.length < 10) {
                loadingMore = false;
            }
            that.setData({
                comments: comments,
                loading: false,
                page: ++page,
                loadingMore: loadingMore
            });
        });
    },
})